import { Controller } from "stimulus"
import { ApiAdmin } from "@js/api/api"
import Utils from "@js/admin/utils/utils"
import FL from "@js/admin/utils/Loading"


export default class extends Controller {
    
    /**
     * @param {Event} e 
     */
    adminModify = async (e) => {
        e.preventDefault()
        const form = e.target
        const datas = new FormData(form)
        FL.set()
        try {
            const { data: { message } } = await ApiAdmin.put(form.getAttribute('action'), datas)
            Utils.notifySuccess(message)
            datas.forEach(v => this.element.querySelector('[name="' + v.toString() + '"]').value = '')
        } catch (_) { }
        FL.unset()
    }

}
