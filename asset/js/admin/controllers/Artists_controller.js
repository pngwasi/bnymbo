import { Controller } from "stimulus"
import Utils from "../utils/utils"
import { ApiAdmin } from "../../api/api"
import FL from "../utils/Loading"


export default class extends Controller {
    static targets = ["name", "output"]

    initialize() {
        Utils.dropify()
    }

    connect() { }

    /**
     * 
     * @param {Event} e 
     */
    newArtist = async (e) => {
        e.preventDefault()
        const form = e.target
        const datas = new FormData(form)
        FL.set()
        try {
            const { data } = await ApiAdmin.post(form.getAttribute('action'), datas)
            Utils.swup.loadPage({
                url: data.redirect
            })
            $('.modal').modal('hide')
        } catch (_) { }
        FL.unset()
    }

}
