import { Controller } from "stimulus"

export default class extends Controller {
    urls = {
        getClients: this.data.get('getClients'),
        searchClient: this.data.get('searchClient'),
        clientDetails: this.data.get('clientDetails'),
        clientFollowing: this.data.get('clientFollowing'),
        clientFollowers: this.data.get('clientFollowers'),
        clientLikes: this.data.get('clientLikes'),
        clientPurchase: this.data.get('clientPurchase'),
        clientToArtiste: this.data.get('clientToArtiste')
    }

    async connect() {
        const { init } = await import('./client/react/Client.jsx')
        this.react = init(this.element, document.querySelector('html').lang, this.urls)
    }

    disconnect() {
        this.react && this.react()
    }

}
