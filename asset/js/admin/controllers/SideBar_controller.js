import { Controller } from "stimulus"

export default class extends Controller {

    initialize() {
        this.elements = Array.from(this.element.querySelectorAll('.nav .nav-item .nav-link'))
    }

    connect() {
        const elements = this.elements
        const path = window.location.pathname
        elements.forEach($element => {
            if ($element.getAttribute('href').indexOf(path) > -1) {
                $element.parentNode.classList.add('active')
            }
            $element.addEventListener('click', () => {
                if ($element.parentNode.classList.contains('active')) {
                    return
                }
                const active = this.element.querySelector('.active')
                active.classList.remove('active')
                $element.parentNode.classList.add('active')
            })
        });
    }

}
