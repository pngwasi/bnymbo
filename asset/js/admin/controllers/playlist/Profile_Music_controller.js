import { Controller } from "stimulus"
import FL from "@js/admin/utils/Loading"
import { ApiAdmin } from "@js/api/api"
import { Audio } from "@js/admin/utils/Audio"
import { musicsHtmlWithBtnAction } from "@js/admin/utils/Html"


export default class extends Controller {
    static targets = ['musicsList', 'musicsListModal']
    urls = {
        musics: this.data.get('musics'),
        artistMusics: this.data.get('artistMusics'),
        addToPlaylist: this.data.get('addToPlaylist')
    }

    initialize() {
        this.musicsDatas = []
    }

    connect() {
        this.getMusicCardList()
    }

    disconnect() { }

    /**
     * @param {array} musics 
     * @param {string} action 
     * @param {HTMLElement} musicsList 
    */
    musicsListCardHtml(musics, action, musicsList, delBtn = true, col_lg = 'col-lg-3') {
        const views = musics.map(music => {
            return (`<div ${music.id ? `data-id="${music.id}"` : ''} class="${col_lg} col-md-4 col-6">
                ${musicsHtmlWithBtnAction(music, true, delBtn, false)}
            </div>`)
        }).join('\n')
        let html = document.createRange().createContextualFragment(views)
        musicsList[action](html)
    }


    appendToMusicList(data) {
        this.musicsListCardHtml(data, 'appendChild', this.musicsList)
        Audio.actionPlayMusic(this.musicsList, data)
        Audio.actionRemoveMusic(this.musicsList, data, () => { })
    }

    async getMusicCardList() {
        FL.prSet(this.element)
        try {
            const { data } = await ApiAdmin.get(this.urls.musics)
            this.appendToMusicList(data)
        } catch (_) { }
        FL.prUnset(this.element)
        this.getArtistMusicList()
    }

    /**
     * @param { number } e 
     */
    async addToPlaylist(id) {
        if (isNaN(+id)) return
        if (!confirm('?')) return
        FL.set()
        try {
            const { data } = await ApiAdmin.post(encodeURI(`${this.urls.addToPlaylist}?music_id=${id}`), {})
            this.appendToMusicList(data)
        } catch (_) { }
        FL.unset()
    }
    /**
     * @param { Object } data 
     */
    initAddToPlaylist() {
        /**
         * @param { HTMLElement } el 
         */
        const handleAction = (el) => {
            const k = el.querySelector('.uppy-Dashboard-Item-previewInnerWrap')
            k.style.cursor = 'pointer'
            k.addEventListener('click', this.addToPlaylist.bind(this, ...[el.dataset.id]))
        }
        Array.from(this.musicsListModal.children)
            .forEach(el => el.hasAttribute('data-id') && handleAction(el));
    }

    /**
     * @param { Object } data 
     */
    appendItemToModal(data) {
        this.removeMusicChild(this.musicsListModal)
        this.musicsListCardHtml(data, 'appendChild', this.musicsListModal, false, 'col-lg-4')
        Audio.actionPlayMusic(this.musicsListModal, data)
        this.initAddToPlaylist()
    }

    async getArtistMusicList() {
        FL.prSet(this.musicsListModal)
        try {
            const { data: { data: { data } } } = await ApiAdmin.get(this.urls.artistMusics)
            this.musicsDatas = [...data]
            this.appendItemToModal(data)
        } catch (_) { }
        FL.prUnset(this.musicsListModal)
    }

    /**
     * @param { Event } e
     */
    searchMusics = async (e) => {
        e.preventDefault()
        const form = new FormData(e.target)
        const title = String(form.get('title')).trim();
        const parent = this.musicsListModal
        if (!title.length) {
            this.appendItemToModal(this.musicsDatas)
            return;
        }
        const url = e.target.getAttribute('action')
        FL.prSet(parent)
        try {
            const { data } = await ApiAdmin.post(url, form)
            this.appendItemToModal(data)
        } catch (_) { }
        FL.prUnset(parent)
    }

    /**
     * @param { HTMLElement } parent 
     */
    removeMusicChild(parent) {
        Array.from(parent.children)
            .forEach(element => element.parentNode.removeChild(element))
    }

    /**
     * @returns { HTMLElement }
     */
    get musicsList() {
        return this.musicsListTarget
    }
    /**
     * @returns { HTMLElement }
     */
    get musicsListModal() {
        return this.musicsListModalTarget
    }
}