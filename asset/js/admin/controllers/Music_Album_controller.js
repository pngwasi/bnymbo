import { Controller } from "stimulus"
import { ApiAdmin } from "@js/api/api"
import FL from "@js/admin/utils/Loading"


export default class extends Controller {
    static targets = ["price"]


    connect() {}

    /**
     * @param {Event} e 
     */
    generalPrice = async (e) => {
        e.preventDefault()
        const form = e.target
        const datas = new FormData(form)
        FL.set()
        try {
            const { data } = await ApiAdmin.post(form.getAttribute('action'), datas)
            this.price.textContent = data.price || 0
            $('.modal').modal('hide')
        } catch (_) { }
        FL.unset()
    }

    /**
     * @returns {HTMLElement}
    */
    get price() {
        return this.priceTarget
    }

}
