import { Controller } from "stimulus"
import FL from "@js/admin/utils/Loading"
import { ApiAdmin } from "@js/api/api"
import Utils from "@js/admin/utils/utils"
import SlimSelect from 'slim-select'
import { initUppy } from "@js/admin/utils/Uppy"
import { Audio } from "@js/admin/utils/Audio"
import { musicsHtmlWithBtnAction, htmlAlbum, htmlPlaylist, HtmlAudio } from "@js/admin/utils/Html"


export default class extends Controller {
    static targets = [
        'albumTab',
        'playlistTab',
        'musicTab',
        'albumsView',
        'uppyInit',
        'slimSelectGenres',
        'slimSelectCategory',
        'audioList',
        'playlistsView',
        'musicsList'
    ]
    $user = {}
    $albums = []

    /**
     * @param {string} property 
     */
    initEvent(property) {
        window[property]('tab-' + this.playlistTab.id, this.playlist)
        window[property]('tab-' + this.albumTab.id, this.album)
        window[property]('tab-' + this.musicTab.id, this.music)
    }

    initialize = () => {
        Utils.dropify()
        $('.datetimepicker').datetimepicker({
            format: 'YYYY-MM-DD',
            defaultDate: false,
            useStrict: true
        })
        this.local = document.querySelector('html').getAttribute('lang')
        this.ui = document.querySelector('#artist-id').getAttribute('data-ui')
        this.genreElement = null
        this.categoryElement = null
    }

    connect() {
        this.initEvent('addEventListener')
    }

    disconnect() {
        this.initEvent('removeEventListener')
        const destoryed = $('.datetimepicker').data("DateTimePicker")
        if (destoryed) destoryed.destroy()
        this.genresSelect && this.genresSelect.destroy()
        this.categorySelect && this.categorySelect.destroy()
    }

    album = async (_) => {
        FL.prSet(this.albumTab)
        try {
            const { data } = await ApiAdmin.get(this.albumTab.dataset.urlAlbums)
            this.$albums = [...data]
            this.htmlAlbum(data, 'appendChild')
        } catch (_) { }
        FL.prUnset(this.albumTab)
    }

    /**
     * @param {array} albums 
     * @param { string } action
     */
    htmlAlbum(albums, action) {
        const views = albums.map(album =>
            `<div class="col-md-4 col-lg-2 col-6" >${htmlAlbum(album)}</div>`).join('\n')
        let html = document.createRange().createContextualFragment(views)
        this.albumsView[action](html)
    }

    /**
     * @param {Event} e
     */
    newAlbum = async (e) => {
        e.preventDefault()
        const form = e.target
        const datas = new FormData(form)
        FL.set()
        try {
            const { data } = await ApiAdmin.post(this.albumTab.dataset.urlCreateAlbum, datas)
            this.$albums = [...data, ...this.$albums]
            this.htmlAlbum(data, 'prepend');
            $('.modal').modal('hide');
            datas.forEach((v, k) => form.querySelector('[name=' + k + ']').value = '')
        } catch (_) { }
        FL.unset()
    }

    /**
     * @param { Event } e
     */
    newPlaylist = async (e) => {
        e.preventDefault()
        const form = e.target
        const datas = new FormData(form)
        FL.set()
        try {
            const { data } = await ApiAdmin.post(this.playlistTab.dataset.urlNewPlaylist, datas)
            this.htmlPlaylist(data, 'prepend');
            $('.modal').modal('hide');
            datas.forEach((v, k) => form.querySelector('[name=' + k + ']').value = '')
        } catch (_) { }
        FL.unset()
    }

    async slimInitSelect() {
        const ev = new Event('input', { bubbles: true });
        ev.simulated = true;
        this.genresSelect = new SlimSelect({
            select: this.slimSelectGenres,
            placeholder: (this.local === 'en' ? 'Genres Music' : 'Genres Musique')
        })

        this.categorySelect = new SlimSelect({
            select: this.slimSelectCategory,
            showSearch: false,
            placeholder: (this.local === 'en' ? 'Music category' : 'Catégorie Musique')
        })
        this.slimSelectGenres.addEventListener('change', ({ target: { value } }) => {
            if (this.genreElement && value) {
                this.genreElement.value = value
                this.genreElement.dispatchEvent(ev)
            }
        })
        this.slimSelectCategory.addEventListener('change', ({ target: { value } }) => {
            if (this.categoryElement && value) {
                this.categoryElement.value = value
                this.categoryElement.dispatchEvent(ev)
            }
        })
    }

    /**
     * @param {array} musics 
     * @param {string} action 
     */
    htmlMusics(musics, action) {
        const views = musics.map(music => HtmlAudio(music)).join('\n')
        let html = document.createRange().createContextualFragment(views)
        this.audioList[action](html)
    }

    /**
     * @param {array} musics 
     * @param {string} action 
    */
    musicsListCardHtml(musics, action) {
        const views = musics.map(music => {
            return (`<div class="col-md-4 col-lg-3 col-6" >${musicsHtmlWithBtnAction(music)}</div>`)
        }).join('\n')
        let html = document.createRange().createContextualFragment(views)
        this.musicsList[action](html)
        return html
    }

    bindToMusicsList(data, remove = true) {
        if (remove) this.removeMusicChild()
        this.musicsListCardHtml(data, 'appendChild')
        Audio.actionPlayMusic(this.musicsList, data)
        Audio.actionRemoveMusic(this.musicsList, data, () => { })
    }

    async getMusicCardList() {
        if (this.musicListData && this.musicListData.to >= this.musicListData.total) {
            return;
        }
        FL.prSet(this.musicsList)
        try {
            const { data } = await ApiAdmin.get(this.musicListLink)
            if (data && data.to < data.total) {
                this.mustScroll = true
            }
            this.musicListData = data
            this.musicListLink = data.next_page_url
            this.datasMusics = [...this.datasMusics, ...data.data.data]
            this.bindToMusicsList(data.data.data, false)
        } catch (_) { }
        FL.prUnset(this.musicsList)
    }

    getMusicsByLazyScroll() {
        if (this.musicListData && this.musicListData.to >= this.musicListData.total) {
            return;
        }
        this.musicsList.addEventListener("scroll", (e) => {
            const red = ((this.musicsList.scrollHeight - this.musicsList.scrollTop) <= (this.musicsList.clientHeight + 10)
                && this.mustScroll === true)
            if (red) {
                this.mustScroll = false
                this.getMusicCardList()
            }
        });
    }

    async getMusicList() {
        FL.prSet(this.audioList)
        try {
            const { data } = await ApiAdmin.get(this.audioList.dataset.urlGetMusics)
            this.htmlMusics(data, 'appendChild')
        } catch (_) { }
        FL.prUnset(this.audioList)
        this.getMusicCardList()
        this.getMusicsByLazyScroll()
    }

    removeMusicChild() {
        Array.from(this.musicsList.children)
            .forEach(element => element.parentNode.removeChild(element))
    }

    /**
     * @param { Event } e
     */
    onSearchMusic = e => {
        const { value } = e.target
        if (value.trim().length > 1) {
            this.submitSearch = true;
            this.mustScroll = false;
            return;
        } else if (value.trim().length <= 1) {
            this.bindToMusicsList(this.datasMusics)
        }
        if (this.musicListData && this.musicListData.to < this.musicListData.total) {
            this.mustScroll = true
        }
        this.submitSearch = false;
    }

    /**
     * @param { Event } e
     */
    searchMusics = async (e) => {
        e.preventDefault()
        if (!this.submitSearch) return;
        const url = e.target.getAttribute('action')
        const form = new FormData(e.target)
        FL.prSet(this.musicsList)
        try {
            const { data } = await ApiAdmin.post(url, form)
            this.bindToMusicsList(data)
        } catch (_) { }
        FL.prUnset(this.musicsList)
    }

    music = async () => {
        this.musicListLink = this.musicsList.dataset.urlGetMusics
        this.musicListData = null
        this.mustScroll = false;
        this.datasMusics = []
        //---------
        this.slimInitSelect()
        this.getMusicList()
        const genres = this.genresSelect.data.data.map(data => data.value)
        const categorys = this.categorySelect.data.data.map(data => data.value)

        const uppy = initUppy({
            locale: this.local,
            target: this.uppyInit,
            endpoint: this.musicTab.dataset.urlMusicPost,
            genres: genres,
            categorys: categorys
        })

        uppy.on('dashboard:file-edit-start', () => {
            const initInput = () => {
                this.categoryElement = this.uppyInit.querySelector('input[placeholder="Category"]')
                this.genreElement = this.uppyInit.querySelector('input[placeholder="Genres"]')
            }
            setTimeout(initInput, 1000);
        })
        uppy.on('dashboard:file-edit-complete', () => {
            this.genresSelect.set('')
            this.categorySelect.set('')
            this.categoryElement = null
            this.genreElement = null
        })
        uppy.on('upload-success', (file, response) => {
            this.htmlMusics(response.body, 'prepend')
        })
    }

    /**
     * 
     * @param {array} playlists 
     * @param {string} action 
     */
    htmlPlaylist(playlists, action) {
        const views = playlists.map(playlist =>
            `<div class="col-md-4 col-lg-2 col-6">${htmlPlaylist(playlist)}</div></div>`).join('\n')
        let html = document.createRange().createContextualFragment(views)
        this.playlistsView[action](html)
    }

    playlist = async () => {
        FL.prSet(this.playlistTab)
        try {
            const { data } = await ApiAdmin.get(this.playlistTab.dataset.urlPlaylists)
            this.htmlPlaylist(data, 'appendChild')
        } catch (_) { }
        FL.prUnset(this.playlistTab)
    }



    /**
     * @returns {HTMLElement}
    */
    get musicsList() {
        return this.musicsListTarget
    }
    /**
     * @returns {HTMLElement}
    */
    get playlistsView() {
        return this.playlistsViewTarget
    }
    /**
     * @returns {HTMLElement}
    */
    get audioList() {
        return this.audioListTarget
    }
    /**
     * @returns {HTMLElement}
    */
    get slimSelectCategory() {
        return this.slimSelectCategoryTarget
    }

    /**
     * @returns {HTMLElement}
    */
    get slimSelectGenres() {
        return this.slimSelectGenresTarget
    }

    /**
     * @returns {HTMLElement}
     */
    get uppyInit() {
        return this.uppyInitTarget
    }

    /**
     * @returns {HTMLElement}
     */
    get albumsView() {
        return this.albumsViewTarget
    }
    /**
     * @returns {HTMLElement}
     */
    get albumTab() {
        return this.albumTabTarget
    }

    /**
     * @returns {HTMLElement}
     */
    get playlistTab() {
        return this.playlistTabTarget
    }

    /**
     * @returns {HTMLElement}
    */
    get musicTab() {
        return this.musicTabTarget
    }

}
