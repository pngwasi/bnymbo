import { Controller } from "stimulus"
import { ApiAdmin } from "@js/api/api"
import FL from "@js/admin/utils/Loading"


export default class extends Controller {
    dispached = []

    /**
     * @param {string} property 
     */
    initEvent(property) {
        this.tabs.forEach(tab => tab[property]('click', e => {
            e.preventDefault()
            this.dispacheEventF(tab)
        }))
    }

    initialize() {
        this.dispached = []
        this.tabs = Array.from(this.element.querySelectorAll('.tabs--dash a.nav-link'))
        this.actives = Array.from(this.element.querySelectorAll('.tabs--dash a.nav-link.active'))
    }

    connect() {
        this.initEvent('addEventListener')
        window.setTimeout(() => {
            this.actives.forEach(tab => this.dispacheEventF(tab))
        }, 500)
    }

    disconnect() {
        this.initEvent('removeEventListener')
    }

    /**
     * 
     * @param { HTMLElement } tab
     */
    dispacheEventF = (tab) => {
        const id = tab.getAttribute('href').replace('#', '')
        const event = `tab-${id}`
        if (!this.dispached.includes(event)) {
            window.dispatchEvent(new CustomEvent(event))
            this.dispached.push(event)
        }
    }

    notifySuccess(message) {
        window.$.notify({
            message: message
        }, {
            type: "success",
            z_index: 11055,
            placement: {
                from: "bottom",
                align: "right"
            },
        })
    }

    /**
     * @param { Event } e
     */
    generatePassword = async (e) => {
        e.preventDefault()
        FL.set()
        try {
            const { data: { message } } = await ApiAdmin.put(e.target.getAttribute('action'))
            this.notifySuccess(message)
        } catch (_) { }
        FL.unset()
    }

    /**
     * @param { Event } e
     */
    changeSecondary = async e => {
        e.preventDefault()
        const form = e.target
        const datas = new FormData(form)
        FL.set()
        try {
            const { data: { message } } = await ApiAdmin.put(form.getAttribute('action'), datas)
            this.notifySuccess(message)
        } catch (_) { }
        FL.unset()
    }

    /**
     * @param { Event } e
     */
    changePrimary = async e => {
        e.preventDefault()
        const form = e.target
        const datas = new FormData(form)
        FL.set()
        try {
            const { data: { message } } = await ApiAdmin.put(form.getAttribute('action'), datas)
            this.notifySuccess(message)
        } catch (_) { }
        FL.unset()
    }
}
