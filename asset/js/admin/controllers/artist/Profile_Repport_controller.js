import { Controller } from "stimulus"
import { ApiAdmin } from "../../../api/api"
import FL from "../../utils/Loading"


export default class extends Controller {
    static targets = ['activityTab', 'followersTab', 'printTab', 'activityTable', 'followersTable']

    /**
     * @param {string} property 
     */
    initEvent(property) {
        window[property]('tab-' + this.activityTab.id, this.activity)
        window[property]('tab-' + this.followersTab.id, this.followers)
        window[property]('tab-' + this.printTab.id, this.prints)
    }

    initialize() {
        this.local = document.querySelector('html').getAttribute('lang')
        this.ui = document.querySelector('#artist-id').getAttribute('data-ui')
    }

    connect() {
        this.initEvent('addEventListener')
    }

    disconnect() {
        this.initEvent('removeEventListener')
    }

    activity = async () => {
        FL.prSet(this.activityTab)
        try {
            const { data } = await ApiAdmin.get(this.activityTab.dataset.urlActivities)
            const database = $(this.activityTable).DataTable({
                data: data
            });
        } catch (_) { }
        FL.prUnset(this.activityTab)
    }

    followers = async () => {
        FL.prSet(this.followersTab)
        try {
            const { data } = await ApiAdmin.get(this.followersTab.dataset.urlFollowers)
            const database = $(this.followersTable).DataTable({
                data: data
            });
        } catch (_) { }
        FL.prUnset(this.followersTab)
    }

    prints = () => {
        console.log('prints');
    }

    /**
     * @returns {HTMLElement}
    */
    get followersTable() {
        return this.followersTableTarget
    }

    /**
     * @returns {HTMLElement}
    */
    get activityTable() {
        return this.activityTableTarget
    }
    /**
     * @returns {HTMLElement}
    */
    get activityTab() {
        return this.activityTabTarget
    }

    /**
     * @returns {HTMLElement}
    */
    get followersTab() {
        return this.followersTabTarget
    }

    /**
     * @returns {HTMLElement}
    */
    get printTab() {
        return this.printTabTarget
    }
}
