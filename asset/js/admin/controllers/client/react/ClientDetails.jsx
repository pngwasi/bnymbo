import React, { Fragment, useCallback, useEffect, useState, useContext, createContext, useMemo, useRef } from 'react'
import { ClientContext } from './context/ClientContext';
import { Tabs, Loader, CardStat } from './Components';
import { ApiAdmin } from '@/js/api/api';
import { useTranslation } from "react-i18next";
import { Paginate } from './Components';

const H = {
    link: null,
    response: { data: { data: [] } },
    client: {}
}
const TabsValue = {
    likes: { ...H },
    following: { ...H },
    followers: { ...H },
    purchase: { ...H },
}

const TabsContext = createContext({
    ...TabsValue,
    memorizeDatas: () => null
})

const useTabsFetch = (urlKey, contextKey) => {
    const { client, URL } = useContext(ClientContext)
    const tc = useContext(TabsContext)
    const [url, seturl] = useState(null)
    const [loader, setLoader] = useState(false)
    const datas = tc[contextKey].response.data.data
    const contextClient = tc[contextKey].client
    useEffect(() => {
        if (contextClient !== client) {
            tc[contextKey].client = client
            tc[contextKey].link = null
            seturl(null)
            tc.memorizeDatas(tc)
        }
    })
    useEffect(() => {
        if ((!url && tc[contextKey].link) || (contextClient !== client)) return
        (async () => {
            setLoader(true)
            const link = (url || URL[urlKey]) + `${url ? '&' : '?'}${client.query}`;
            try {
                const { data: response } = await ApiAdmin.get(link)
                tc[contextKey].link = link
                tc[contextKey].response = response
                tc.memorizeDatas(tc)
            } catch (error) {
                console.log(error)
            }
            setLoader(false)
        })()
    }, [url, contextClient])
    const handlePaginationLinkChanged = useCallback((link) => {
        link && seturl(link)
    });

    return { handlePaginationLinkChanged, datas, pagination: tc[contextKey].response, loader }
}

const LR = ({ className, children, loader }) => {
    return <div style={{ position: 'relative' }} className={className}>
        {children}
        {loader && <Loader />}
    </div>
}

const Following = () => {
    const { handlePaginationLinkChanged, datas, pagination, loader } = useTabsFetch('clientFollowing', 'following')
    const { t } = useTranslation();

    return <LR loader={loader}>
        <Paginate pagination={pagination} onChange={handlePaginationLinkChanged} />
        <table className="table">
            <thead>
                <tr>
                    <th>{t("Image")}</th>
                    <th>{t("Name")}</th>
                    <th>{t("Email")}</th>
                    <th>{t("Recorded On")}</th>
                </tr>
            </thead>
            <tbody>
                {datas.map(client => (
                    <tr key={client.id}>
                        <td>
                            <div className="img-container">
                                <img className="img-cover-full" src={client.image} alt={client.name} rel="nofollow" />
                            </div>
                        </td>
                        <td>
                            <a href={client.route}>{client.name}</a>
                        </td>
                        <td>{client.email}</td>
                        <td>{client.created_at}</td>
                    </tr>
                ))}
            </tbody>
        </table>
    </LR>
}

const Followers = () => {
    const { handlePaginationLinkChanged, datas, pagination, loader } = useTabsFetch('clientFollowers', 'followers')
    const { t } = useTranslation();

    return <LR loader={loader}>
        <Paginate pagination={pagination} onChange={handlePaginationLinkChanged} />
        <table className="table">
            <thead>
                <tr>
                    <th>{t("Image")}</th>
                    <th>{t("Name")}</th>
                    <th>{t("Email")}</th>
                    <th>{t("Recorded On")}</th>
                </tr>
            </thead>
            <tbody>
                {datas.map(client => (
                    <tr key={client.id}>
                        <td>
                            <div className="img-container">
                                <img className="img-cover-full" src={client.image} alt={client.name} rel="nofollow" />
                            </div>
                        </td>
                        <td>{client.name}</td>
                        <td>{client.email}</td>
                        <td>{client.created_at}</td>
                    </tr>
                ))}
            </tbody>
        </table>
    </LR>
}

const Likes = () => {
    const { loader, pagination, handlePaginationLinkChanged, datas } = useTabsFetch('clientLikes', 'likes')
    const { t } = useTranslation();
    return <LR loader={loader}>
        <Paginate pagination={pagination} onChange={handlePaginationLinkChanged} />
        <table className="table">
            <thead>
                <tr>
                    <th>{t("Image")}</th>
                    <th>{t("Title")}</th>
                    <th>{t("Type")}</th>
                    <th>{t("Recorded On")}</th>
                </tr>
            </thead>
            <tbody>
                {datas.map(like => (
                    <tr key={like.id}>
                        <td>
                            {like.image ? (
                                <div className="img-container">
                                    <img className="img-cover-full" src={like.image} alt={like.name} rel="nofollow" />
                                </div>
                            ) : (<b>P</b>)}
                        </td>
                        <td>
                            <a href={like.route}>{like.name}</a>
                        </td>
                        <td>{like.morthable}</td>
                        <td>{like.created_at}</td>
                    </tr>
                ))}
            </tbody>
        </table>
    </LR>
}

const Purchase = () => {
    const { loader, pagination, handlePaginationLinkChanged, datas } = useTabsFetch('clientPurchase', 'purchase')
    const { t } = useTranslation();
    return <LR loader={loader}>
        <Paginate pagination={pagination} onChange={handlePaginationLinkChanged} />
        <table className="table">
            <thead>
                <tr>
                    <th>{t("Product Name")}</th>
                    <th>{t("Product Type")}</th>
                    <th>{t("Product Image")}</th>
                    <th>{t("Product Price")}</th>
                    <th>{t("Service")}</th>
                    <th>{t("Amount")}</th>
                    <th>{t("Discounts")}</th>
                    <th>{t("Total")}</th>
                    <th>{t("Recorded On")}</th>
                </tr>
            </thead>
            <tbody>
                {datas.map(order => (
                    <tr key={order.id}>
                        <td>
                            <a href={order.product.route}>{order.product.name}</a>
                        </td>
                        <td>{order.product.morthable}</td>
                        <td>
                            {order.product.image ? (
                                <div className="img-container">
                                    <img className="img-cover-full" src={order.product.image} alt={order.product.name} rel="nofollow" />
                                </div>
                            ) : (<b>P</b>)}
                        </td>
                        <td>${order.product.price}</td>
                        <td>{order.service_name}</td>
                        <td>${order.total_paid}</td>
                        <td>${order.discounts}</td>
                        <td>${order.total}</td>
                        <td>{order.created_at}</td>
                    </tr>
                ))}
            </tbody>
        </table>
    </LR>
}

const Profile = ({ profile }) => {
    const { t } = useTranslation();
    return <LR>
        <div className="table-responsive">
            <table className="table">
                <tbody>
                    <tr>
                        <td>{t("Name")}: </td>
                        <td><b>{profile.name}</b></td>
                    </tr>
                    <tr>
                        <td>{t("Email")}: </td>
                        <td><b>{profile.email}</b></td>
                    </tr>
                    <tr>
                        <td>{t("First Name")}: </td>
                        <td><b>{profile.firstname}</b></td>
                    </tr>
                    <tr>
                        <td>{t("Last Name")}: </td>
                        <td><b>{profile.lastname}</b></td>
                    </tr>
                    <tr>
                        <td>{t("Recorded On")}: </td>
                        <td><b>{profile.created_at}</b></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </LR>
}

export const ClientDetails = () => {
    const [loader, setLoader] = useState(false)
    const { client, URL } = useContext(ClientContext)
    const [profile, setprofile] = useState({})
    const [details, setdetails] = useState({
        likes: 0,
        following: 0,
        downloads: 0,
        purchase: 0
    })
    const { t } = useTranslation();
    const [tabsDatas, settabsDatas] = useState(TabsValue)
    useEffect(() => {
        (async () => {
            setLoader(true)
            try {
                const { data: { details, profile } } = await ApiAdmin.get(`${URL.clientDetails}?${client.query}`)
                setdetails(details)
                setprofile(profile)
            } catch (error) {
                console.log(error)
            }
            setLoader(false)
        })()
    }, [client])

    const memorizeDatas = useCallback((data) => {
        settabsDatas(data)
    })

    const valueContext = useMemo(() => ({
        ...tabsDatas,
        memorizeDatas
    }), [tabsDatas, memorizeDatas])

    return <LR className="pb-2" loader={loader}>
        <div className="row">
            <div className="col-lg-3 col-md-6 col-sm-6">
                <CardStat icon="favorite" label={t("Likes")} color="card-header-warning" title={details.likes} />
            </div>
            <div className="col-lg-3 col-md-6 col-sm-6">
                <CardStat icon="supervised_user_circle" color="card-header-rose" label={t("Following")} title={details.following} />
            </div>
            <div className="col-lg-3 col-md-6 col-sm-6">
                <CardStat icon="archive" label={t("Downloading")} color="card-header-success" title={details.downloads} />
            </div>
            <div className="col-lg-3 col-md-6 col-sm-6">
                <CardStat icon="store" label={t("Purchase")} color="card-header-info" title={`$${details.purchase}`} />
            </div>
        </div>
        <TabsContext.Provider value={valueContext}>
            <Tabs components={[
                {
                    label: t('Profile'),
                    component: <Profile profile={profile} />
                },
                {
                    label: t('Following'),
                    component: <Following />
                },
                (client.artiste ? {
                    label: t('Followers'),
                    component: <Followers />
                } : null),
                {
                    label: t('Likes'),
                    component: <Likes />
                },
                {
                    label: t('Purchase'),
                    component: <Purchase />
                }
            ].filter(e => e !== null)} />
        </TabsContext.Provider>
    </LR>
}