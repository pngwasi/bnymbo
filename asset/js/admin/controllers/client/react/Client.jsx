import React, { Fragment, useCallback, useRef, useEffect, useState, useMemo } from 'react'
import { render, unmountComponentAtNode } from 'react-dom'
import i18n from "i18next";
import { useTranslation, initReactI18next } from "react-i18next";
import { ApiAdmin } from '@/js/api/api';
import { ClientContext } from './context/ClientContext';
import { ClientDetails } from './ClientDetails'
import { Loader, Paginate } from './Components'
import { ClientToArtiste } from './ClientToArtist';

let URL = {}

const Card = ({ children, header }) => {
    return <div className="card">
        <div className="card-header">{header}</div>
        <div className="card-body">{children}</div>
    </div>
}

const CLSearch = ({ onSubmit }) => {
    const { t } = useTranslation();

    return <form method="get" onSubmit={onSubmit}>
        <div className="input-group no-border">
            <input type="text" name="q" className="form-control" placeholder={t('Search') + "..."} />
            <button type="submit" className="btn btn-white btn-round btn-just-icon">
                <i className="material-icons">search</i>
            </button>
        </div>
    </form>
}

const ClientList = ({ onClientSelect, newClient }) => {
    const parent = useRef(null)
    const [loader, setLoader] = useState(false)
    const [url, setUrl] = useState(URL.getClients)
    const [clients, setClients] = useState([])
    const [pagination, setPagination] = useState({})
    const [reloadFetch, setreloadFetch] = useState(0)
    const { t } = useTranslation();

    useEffect(() => {
        (async () => {
            setLoader(true)
            try {
                const { data: response } = await ApiAdmin.get(url)
                const { data: { data } } = response
                setClients(data)
                setPagination(response)
            } catch (_) { }
            setLoader(false)
        })()
    }, [url, reloadFetch])

    useEffect(() => {
        if (!newClient) return
        const uClient = clients.map(cl => {
            return (
                cl.artiste !== newClient.artiste
            ) ? newClient : cl
        })
        setClients(uClient)
    }, [newClient])

    const handleSearch = useCallback(async (e) => {
        e.preventDefault()
        const query = new URLSearchParams(new FormData(e.target))
        const q = query.get('q').trim()
        if (!q.length) {
            if (URL.getClients !== url) {
                setUrl(URL.getClients)
            } else {
                setreloadFetch(r => r + 1)
            }
            return;
        }
        setLoader(true)
        try {
            const { data: response } = await ApiAdmin.get(URL.searchClient + '?' + query.toString())
            const { data: { data } } = response
            setClients(data)
            setPagination(response)
        } catch (error) {
            console.error(error)
        }
        setLoader(false)
    });

    const handlePaginationLinkChanged = useCallback((link) => {
        link && setUrl(link)
    });

    const imageErrorLoad = function (error) {
        if (error.target) {
            error.target.src = '/images/avatars/avatar.jpg'
        }
    }
    return <>
        <div className="mb-1">
            <CLSearch onSubmit={handleSearch} />
        </div>
        <Paginate pagination={pagination} onChange={handlePaginationLinkChanged} />
        <hr />
        <div ref={parent}>
            <table className="table table-hover">
                <thead>
                    <tr>
                        <th>{t('Image')}</th>
                        <th>{t('Name')}</th>
                    </tr>
                </thead>
                <tbody>
                    {clients.map(client => (
                        <tr key={client.id} onClick={() => onClientSelect(client)}>
                            <td>
                                <div className="img-container">
                                    <img className="img-cover-full" onError={imageErrorLoad} src={client.image} alt={client.name} rel="nofollow" />
                                </div>
                            </td>
                            <td>{client.name}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
            {loader && <Loader parent={parent.current} />}
        </div>
    </>
}

const ClientHeader = ({ client, updateClient }) => {
    const modal = useRef(null)
    const imageErrorLoad = function (error) {
        if (error.target) {
            error.target.src = '/images/avatars/avatar.jpg'
        }
    }
    const { t } = useTranslation();
    return <>
        <div className="justify-content-between d-flex">
            <table>
                <thead>
                    <tr>
                        <th>
                            <div className="img-container">
                                <img className="img-cover-full" onError={imageErrorLoad} src={client.image} alt={client.name} rel="nofollow" />
                            </div>
                        </th>
                        <th>
                            <div className="mx-3">
                                <span>{client.name}</span><br />
                                <span className="text-primary">{client.artiste ? 'client, artiste' : 'client'}</span>
                            </div>
                        </th>
                    </tr>
                </thead>
            </table>
            <table>
                <thead>
                    <tr>
                        <th>
                            {!client.artiste ? (
                                <button
                                    type="button"
                                    onClick={() => modal.current && $(modal.current).modal('show')}
                                    className="btn btn-primary btn-sm">{t("Client to Artiste")}</button>
                            ) : <a href={client.route} className="btn btn-primary btn-sm">Profile</a>}
                        </th>
                    </tr>
                </thead>
            </table>
        </div>
        {!client.artiste && <ClientToArtiste URL={URL} updateClient={updateClient} ref={modal} client={client} />}
    </>
}

const Client = () => {
    const [client, setClient] = useState(null)
    const [newClient, setNewClient] = useState(null)
    const handleSelectedClient = useCallback((clt) => {
        const nc = (client && client.id === clt.id && client.artiste !== clt.artiste);
        if (!client || (client && client.id !== clt.id) || nc) {
            setClient({ ...clt, query: `client=${clt.id}` })
        }
        if (nc) setNewClient(clt)
    }, [client])
    const contextValue = useMemo(() => ({ client, URL }), [client, URL])
    const { t } = useTranslation();

    return <div className="row">
        <div className="col-5">
            <Card>
                <ClientList onClientSelect={handleSelectedClient} newClient={newClient} />
            </Card>
        </div>
        <div className="col-7">
            <Card
                header={client ?
                    <ClientHeader updateClient={handleSelectedClient} client={client} /> :
                    <b>{t('No Client Details')}</b>}>
                <ClientContext.Provider value={contextValue}>
                    {client && <ClientDetails />}
                </ClientContext.Provider>
            </Card>
        </div>
    </div>
}
/**
 * @param { HTMLElement } element 
 */
export const init = (element, locale, url = {}) => {
    i18n
        .use(initReactI18next)
        .init({
            resources: {
                fr: {
                    translation: {
                        "Image": 'Image',
                        "Name": "Nom",
                        "Search": "Chercher",
                        "Email": 'Email',
                        "Recorded On": 'Enregistré le',
                        "Title": 'Titre',
                        "Product Name": 'Nom du produit',
                        "Product Type": 'type de produit',
                        "Product Image": 'Image du produit',
                        "Product Price": 'Prix du produit',
                        "Service": 'Service',
                        "Amount": 'Montant',
                        "Discounts": 'Réductions',
                        "Total": 'Total',
                        "First Name": 'Prénom',
                        "Last Name": 'Nom',
                        "Following": 'Abonnément',
                        "Followers": 'Abonnés',
                        "Likes": 'J\'aimes',
                        "Purchase": 'Achat',
                        "Save as Artiste": 'Enregistrer en tant qu\'artiste',
                        'No Client Details': 'Aucun détail du client',
                        "Downloading": 'Télécharge...',
                        "Client to Artiste": 'Client à artiste'
                    }
                }
            },
            lng: locale,
            interpolation: {
                escapeValue: false
            }
        });
    URL = url
    render(<Client url={url} />, element)
    return () => unmountComponentAtNode(element)
}