import { createContext } from 'react'

export const ClientContext = createContext({
    client: {},
    URL: {}
})