import React, { forwardRef, useEffect, useCallback } from 'react'
import { createPortal } from 'react-dom'
import Utils from '@/js/admin/utils/utils'
import { ApiAdmin } from '@/js/api/api'
import FL from '@/js/admin/utils/Loading'
import { useTranslation } from "react-i18next";

export const ClientToArtiste = forwardRef(({ title = 'Client To Artiste', client = {}, updateClient, URL }, ref) => {
    const { t } = useTranslation();

    useEffect(() => {
        Utils.dropify()
    }, [])
    const handleSubmite = useCallback(async (e) => {
        e.preventDefault()
        FL.set()
        const form = new FormData(e.target)
        try {
            const { data } = await ApiAdmin.post(`${URL.clientToArtiste}?${client.query}`, form)
            $(ref.current).modal('hide')
            updateClient(data)
        } catch (error) {
            console.log(error)
        }
        FL.unset()
    }, [client])
    return createPortal((
        <div className={`modal`} ref={ref} tabIndex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
            <div className="modal-dialog modal-lg">
                <div className="modal-content">
                    <div className="modal-header">
                        <h4 className="modal-title" id="myLargeModalLabel">{title}</h4>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <form onSubmit={handleSubmite} encType="multipart/form-data" method="POST" autoComplete="off">
                        <div className="modal-body">
                            <div className="row mt-3 justify-content-center">
                                <input type="file" className="dropify" accept="image/*" data-allowed-file-extensions="jpg jepg png gif" name="image" data-default-file="/images/simples/Headshot-Placeholder-1.png" data-max-file-size="5M" />
                                <div className="col-md-8 col-sm-12">
                                    <div className="table-responsive">
                                        <table className="table">
                                            <tbody>
                                                <tr>
                                                    <td>{t("Name")}: </td>
                                                    <td><b>{client.name}</b></td>
                                                </tr>
                                                <tr>
                                                    <td>{t("Email")}: </td>
                                                    <td><b>{client.email}</b></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="modal-footer">
                            <button type="submit" className="btn btn-primary">
                                {t("Save as Artiste")}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    ), document.body)
})