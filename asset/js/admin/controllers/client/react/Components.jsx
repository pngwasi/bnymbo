import React, { useState, useEffect } from 'react'

export const Loader = ({ parent }) => {
    if (parent) parent.style.position = 'relative';
    return <div className="loading-ntf-abs">
        <div className='uil-ring-css-abs'>
            <spinning-dots style={{ width: "40px", color: "#27a3eb" }} />
        </div>
    </div>
}

export const Tabs = ({ components = [] }) => {
    const [index, setIndex] = useState(0)
    useEffect(() => {
        if (!components[index]) {
            setIndex(components.length - 1)
        }
    })
    return <>
        <div className="tabs--dash">
            <div className="nav-tabs-navigation">
                <div className="nav-tabs-wrapper">
                    <ul className="nav nav-tabs pl-0 ml-0" data-tabs="tabs">
                        {components.map((tab, i) => {
                            return <li className="nav-item" style={{ cursor: 'pointer' }} key={tab.label}>
                                <a className={`nav-link ${i === index ? 'active show' : ''}`} onClick={() => setIndex(i)} data-toggle="tab">
                                    {tab.icon && <i className="material-icons">{tab.icon}</i>}  {tab.label}
                                </a>
                            </li>
                        })}
                    </ul>
                </div>
            </div>
            <hr />
        </div>
        <div className="tab-content">
            {(!components[index] ? components[components.length - 1] : components[index]).component}
        </div>
    </>
}

export const Paginate = ({ pagination, onChange }) => {
    const prev = pagination.prev_page_url;
    const next = pagination.next_page_url;
    const content = <nav>
        <ul className="pagination">
            <li className={`page-item ${prev ? 'active' : ''}`}>
                <a className={`page-link ${!prev ? 'disabled' : ''}`}
                    rel="prev" onClick={() => onChange(prev)} aria-label="« Précédent">{'<'}</a>
            </li>
            <li className={`page-item ${next ? 'active' : ''}`}>
                <a className={`page-link ${!next ? 'disabled' : ''}`}
                    rel="next" onClick={() => onChange(next)} aria-label="Suivant »">{'>'}</a>
            </li>
        </ul>
    </nav>
    return <>{pagination && (pagination.next_page_url || pagination.prev_page_url) && content}</>
}

export const CardStat = ({ icon, label, title, color = 'card-header-rose' }) => {
    return <div className="card card-stats">
        <div className={"card-header-icon " + color}>
            <div className="card-icon">
                <i className="material-icons">{icon}</i>
            </div>
            <p className="card-category" style={{ color: '#999' }}>{label}</p>
            <h3 className="card-title" style={{ color: '#999' }}>{title}</h3>
        </div>
    </div>
}