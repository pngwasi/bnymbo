import { Controller } from "stimulus"
import SlimSelect from 'slim-select'
import { musicsHtmlWithBtnAction } from "@js/admin/utils/Html"
import FL from "@js/admin/utils/Loading"
import { initUppy } from "@js/admin/utils/Uppy"
import { ApiAdmin } from "@js/api/api"
import { Audio } from "@js/admin/utils/Audio"


export default class extends Controller {
    static targets = [
        'slimSelectGenres',
        'slimSelectCategory',
        'musicsList',
        'uppyInit'
    ]

    initialize = () => {
        this.local = document.querySelector('html').getAttribute('lang')
        this.genreElement = null
        this.categoryElement = null
    }

    connect() {
        this.slimInitSelect()
        this.getMusicCardList()
        this.initUppyUploader()
    }

    disconnect() {
        this.genresSelect && this.genresSelect.destroy()
        this.categorySelect && this.categorySelect.destroy()
    }

    async slimInitSelect() {
        this.genresSelect = new SlimSelect({
            select: this.slimSelectGenres,
            placeholder: (this.local === 'en' ? 'Genres Music' : 'Genres Musique')
        })

        this.categorySelect = new SlimSelect({
            select: this.slimSelectCategory,
            showSearch: false,
            placeholder: (this.local === 'en' ? 'Music category' : 'Catégorie Musique')
        })
        this.slimSelectGenres.addEventListener('change', ({ target: { value } }) => {
            if (this.genreElement && value) {
                const ev = new Event('input', { bubbles: true });
                ev.simulated = true;
                this.genreElement.value = value
                this.genreElement.dispatchEvent(ev)
            }
        })
        this.slimSelectCategory.addEventListener('change', ({ target: { value } }) => {
            if (this.categoryElement && value) {
                const ev = new Event('input', { bubbles: true });
                ev.simulated = true;
                this.categoryElement.value = value
                this.categoryElement.dispatchEvent(ev)
            }
        })
    }

    /**
     * @param {array} musics 
     * @param {string} action 
    */
    musicsListCardHtml(musics, action) {
        const views = musics.map(music => {
            return (`<div class="col-md-4 col-lg-3 col-6">${musicsHtmlWithBtnAction(music)}</div>`)
        }).join('\n')
        let html = document.createRange().createContextualFragment(views)
        this.musicsList[action](html)
    }

    async getMusicCardList() {
        FL.prSet(this.element)
        try {
            const { data } = await ApiAdmin.get(this.musicsList.dataset.urlGetMusics)
            this.musicsListCardHtml(data, 'appendChild')
            Audio.actionPlayMusic(this.musicsList, data)
            Audio.actionRemoveMusic(this.musicsList, data, () => { })
        } catch (_) { }
        FL.prUnset(this.element)
    }


    initUppyUploader() {
        const genres = this.genresSelect.data.data.map(data => data.value)
        const categorys = this.categorySelect.data.data.map(data => data.value)
        const uppy = initUppy({
            locale: this.local,
            target: this.uppyInit,
            endpoint: this.uppyInit.dataset.urlNewMusics,
            genres: genres,
            categorys: categorys
        })
        uppy.on('dashboard:file-edit-start', () => {
            const initInput = () => {
                this.categoryElement = this.uppyInit.querySelector('input[placeholder="Category"]')
                this.genreElement = this.uppyInit.querySelector('input[placeholder="Genres"]')
            }
            setTimeout(initInput, 1000);
        })
        uppy.on('dashboard:file-edit-complete', () => {
            this.genresSelect.set('')
            this.categorySelect.set('')
            this.categoryElement = null
            this.genreElement = null
        })
        uppy.on('upload-success', (file, response) => {
            this.musicsListCardHtml(response.body, 'prepend')
            Audio.actionPlayMusic(this.musicsList, response.body)
        })
    }

    /**
     * @returns {HTMLElement}
    */
    get uppyInit() {
        return this.uppyInitTarget
    }
    /**
     * @returns {HTMLElement}
    */
    get musicsList() {
        return this.musicsListTarget
    }

    /**
     * @returns {HTMLElement}
    */
    get slimSelectCategory() {
        return this.slimSelectCategoryTarget
    }

    /**
     * @returns {HTMLElement}
    */
    get slimSelectGenres() {
        return this.slimSelectGenresTarget
    }
}