import { Controller } from "stimulus"


export default class extends Controller {
    dispached = []

    /**
     * @param {string} property 
     */
    initEvent(property) {
        this.tabs.forEach(tab => tab[property]('click', e => {
            e.preventDefault()
            this.dispacheEventF(tab)
        }))
    }

    initialize() {
        this.dispached = []
        this.tabs = Array.from(this.element.querySelectorAll('.tabs--dash a.nav-link'))
        this.actives = Array.from(this.element.querySelectorAll('.tabs--dash a.nav-link.active'))
    }

    connect() {
        this.initEvent('addEventListener')
        window.setTimeout(() => {
            this.actives.forEach(tab => this.dispacheEventF(tab))
        }, 500)
    }

    disconnect() {
        this.initEvent('removeEventListener')
    }

    /**
     * 
     * @param { HTMLElement } tab
     */
    dispacheEventF = (tab) => {
        const id = tab.getAttribute('href').replace('#', '')
        const event = `tab-${id}`
        if (!this.dispached.includes(event)) {
            window.dispatchEvent(new CustomEvent(event))
            this.dispached.push(event)
        }
    }
}
