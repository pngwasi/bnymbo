import { Controller } from "stimulus"
import { ApiAdmin } from "../../api/api"
import FL from "../utils/Loading"


export default class extends Controller {
    static targets = []

    connect() { }

    /**
     * @param {Event} e 
     */
    newCategory = async (e) => {
        e.preventDefault()
        const form = e.target
        const datas = new FormData(form)
        FL.set()
        try {
            await ApiAdmin.post(form.getAttribute('action'), datas)
            window.location.reload()
        } catch (_) { }
        FL.unset()
    }
}
