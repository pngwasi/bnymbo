import { Controller } from "stimulus"
import { setI18nLanguage } from "@js/api/api"
import { ChangeLocale } from "@js/api/actions/Locale"
import Utils from "../utils/utils"

export default class extends Controller {
    static targets = ['Locale']

    connect() {
        this.locale.value = document.querySelector('html').lang
        this.locale.addEventListener('change', this.changeLocale)
    }

    /**
     * @param { Event } e
     */
    changeLocale = async (e) => {
        const { value } = e.target
        setI18nLanguage(value)
        await ChangeLocale(this.data.get('locale'), value)
        Utils.swup.loadPage({
            url: window.location.pathname + window.location.search
        }, true)
    }

    disconnect() {
        this.locale.removeEventListener('change', this.changeLocale)
    }

    /** 
     * @return { HTMLSelectElement }
    */
    get locale() {
        return this.LocaleTarget
    }

}
