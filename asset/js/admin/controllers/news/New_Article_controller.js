import { Controller } from "stimulus"
import { EditorJS } from '@js/admin/utils/Editorjs'
import Utils from "@js/admin/utils/utils"
import { ApiAdmin } from "@/js/api/api"
import FL from "@js/admin/utils/Loading"
import { convertToHtml } from "@/js/functions/jsonToHtml"

export default class extends Controller {
    static targets = ['title', 'modalViewer']
    urls = {
        linkEndpoint: this.data.get('linkEndpoint'),
        addArticle: this.data.get('addArticle')
    }

    initialize() {
        Utils.dropify()
    }

    async connect() {
        this.editor = EditorJS({}, this.urls.linkEndpoint)
    }

    disconnect() {
        this.editor.destroy()
    }

    /**
     * 
     * @param { Event } e 
     */
    newArticle = async (e) => {
        e.preventDefault()
        FL.set()
        try {
            const datas = new FormData(e.target)
            const json = await this.editor.save()
            datas.append('json', JSON.stringify(json))
            const { data } = await ApiAdmin.post(this.urls.addArticle, datas)
            this.showInModal(json.blocks, data.title)
            this.editor.clear()
            this.clearInput()
        } catch (error) {
            console.error(error)
        }
        FL.unset()
    }

    /**
     * @param { Array } blocks 
     * @param { string } title
     */
    showInModal(blocks, title) {
        const html = document.createRange().createContextualFragment(convertToHtml(blocks))
        const body = this.modalViewer.querySelector('.modal-body')
        this.modalViewer.querySelector('.modal-title').textContent = title
        Array.from(body.children)
            .forEach(element => element.parentNode.removeChild(element))
        body.appendChild(html)
        $(this.modalViewer).modal('show')
    }

    clearInput() {
        this.element.querySelectorAll('input[type="text"]')
            .forEach(element => element.value = '')
    }

    /**
     * @returns { HTMLInputElement }
     */
    get title() {
        return this.titleTarget
    }

    /**
     * @returns { HTMLElement }
     */
    get modalViewer() {
        return this.modalViewerTarget
    }

}
