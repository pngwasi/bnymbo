import { Controller } from "stimulus"
import { Audio } from "@js/admin/utils/Audio"


export default class extends Controller {

    static targets = ['audio']

    initialize() {
        Audio.actionPlayMusic(this.audio, [{ url: this.data.get('audio') }])
    }

    /**
     * @returns { HTMLElement }
     */
    get audio() {
        return this.audioTarget;
    }
}
