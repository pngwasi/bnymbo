import { Application } from "stimulus"
import { definitionsFromContext } from "stimulus/webpack-helpers"
import Utils from "./utils/utils"
import '@grafikart/spinning-dots-element'
import '@uppy/core/dist/style.css'
import '@uppy/dashboard/dist/style.css'
import '@uppy/url/dist/style.css'
import 'slim-select/dist/slimselect.min.css'


Utils.init()

const application = Application.start()
const context = require.context("./controllers", true, /\.js|jsx$/)
application.load(definitionsFromContext(context))
