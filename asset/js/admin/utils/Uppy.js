import Uppy from '@uppy/core'
import Dashboard from '@uppy/dashboard'
import XHRUpload from '@uppy/xhr-upload'
import Url from '@uppy/url'
import en_US from '@uppy/locales/lib/en_US'
import fr_FR from '@uppy/locales/lib/fr_FR'
import Utils from "./utils"

/**
 * 
 * @param { Object } param0 
 */

export const initUppy = ({ locale, target, endpoint, genres, categorys }) => {
    const uppy = Uppy({
        debug: true,
        autoProceed: false,
        locale: (locale === 'en' ? en_US : fr_FR),
        restrictions: {
            maxFileSize: (20 * 1024 * 1024),
            allowedFileTypes: ['audio/mpeg', 'audio/mp3']
        },
        onBeforeUpload: (files) => {

            for (const key in files) {
                const file = files[key]
                const meta = file['meta']
                if ((meta.title && meta.title.trim().length < 1) || !meta.title) {
                    uppy.info(file.name + ', \n' + (locale === 'en' ? 'Field title\'s required' : 'Champs titre requis'))
                    return false
                } else if ((meta.category && meta.category.trim().length < 2) || !meta.category) {
                    uppy.info(file.name + ', \n' + (locale === 'en' ? 'Category field\'s required' : 'Champs Catégorie requis'))
                    return false
                } else if (!categorys.includes(meta.category)) {
                    uppy.info(file.name + ', \n' + (locale === 'en' ? 'Category field\'s invalid' : 'Champ catégorie non valide'))
                    return false
                } else if ((meta.genres && meta.genres.trim().length > 0) && !genres.includes(meta.genres)) {
                    uppy.info(file.name + ', \n' + (locale === 'en' ? 'Genres field\'s invalid' : 'Champ genres non valide'))
                    return false
                } else if ((meta.unitPrice && meta.unitPrice.trim().length > 0) && isNaN(parseFloat(meta.unitPrice))) {
                    uppy.info(file.name + ', \n' + (locale === 'en' ? 'Unit Price field\'s invalid' : 'Champ prix unitaire non valide'))
                    return false
                }
            }
        }
    })
    uppy.use(Dashboard, {
        target,
        inline: true,
        replaceTargetContent: true,
        showProgressDetails: true,
        height: 470,
        browserBackButtonClose: true,
        metaFields: [
            { id: 'title', name: 'title', placeholder: (locale === 'en' ? 'Title' : 'Titre') },
            { id: 'unitPrice', name: 'unitPrice', placeholder: (locale === 'en' ? 'Unit Price' : 'Prix Unitaire') },
            { id: 'category', name: 'category', placeholder: 'Category' },
            { id: 'genres', name: 'genres', placeholder: 'Genres' },
        ]
    })
    uppy.use(Url, {
        target: Dashboard,
        companionUrl: 'https://companion.uppy.io/',
    })
    uppy.use(XHRUpload, {
        target: Dashboard,
        endpoint,
        fieldName: 'music_file',
        withCredentials: true,
        timeout: 0,
        headers: {
            "Accept": "application/json, text/plain, */*",
            "X-Requested-With": "XMLHttpRequest"
        }
    })
    uppy.on('file-added', (file) => {
        const initImageInput = () => {
            const element = document.getElementById(`uppy_${file.id}`)
            const innerIcon = element.querySelector('.uppy-Dashboard-Item-previewInnerWrap')
            const inputs = document.createElement('input')
            inputs.type = 'file'
            inputs.className = 'file-upload-m'
            inputs.accept = 'image/*'
            inputs.style.cursor = 'pointer'
            const firstElement = innerIcon.firstElementChild
            firstElement.style.zIndex = '2';
            firstElement.style.display = 'block';

            const div = document.createElement('div')
            div.style.width = "100%"
            div.style.height = "100%"
            div.style.display = 'none'

            const img = document.createElement("img")
            img.style.objectFit = 'cover'
            img.style.width = "100%"
            img.style.height = "100%"

            div.appendChild(img)
            innerIcon.appendChild(inputs)
            innerIcon.appendChild(div)

            inputs.addEventListener("change", (e) => {
                const filei = inputs.files[0];
                if (filei.size && filei.size > (5 * (1024 ** 2))) {
                    uppy.info((locale === 'en' ?
                        'Limit size This file exceeds maximum allowed size of 5MB' :
                        'Taille limite Ce fichier dépasse la taille maximale autorisée de 5Mo'))
                    return false
                } else if (filei.type && !filei.type.includes('image')) {
                    uppy.info((locale === 'en' ?
                        'Invalid file Type' :
                        'type fichier invalid'))
                    return false
                }
                uppy.setFileMeta(file.id, {
                    music_img: filei
                })
                firstElement.style.display = 'none';
                div.style.display = 'initial';
                img.src = URL.createObjectURL(filei);
            })
        }
        setTimeout(initImageInput, 500);
    })

    uppy.on('upload-error', (file, error, response) => {
        if (response.body) {
            let { message, errors } = response.body
            errors = { ...{ name: [file.name] }, ...errors }
            Utils.notifyExceptionMessage({
                data: { message, errors }
            })
        }
    })
    return uppy
}