import Utils from "./utils"

export const Audio = {
    /**
     * @param { Array } datas
     * @param { Element } musicsListElement 
     */
    actionPlayMusic(musicsListElement, datas) {
        datas.forEach(data => {
            const e = musicsListElement.querySelector(`[data-play="${data.url}"]`)
            const icon = e.querySelector('i')
            const audio = e.querySelector('audio')
            audio.addEventListener("pause", async e => {
                icon.innerHTML = 'play_circle_outline'
            })
            audio.addEventListener("play", () => {
                icon.innerHTML = 'pause_circle_outline'
            });
            e.addEventListener("click", async e => {
                if (audio.paused) {
                    Utils.INprogress.set()
                    await audio.play()
                    Utils.INprogress.unset()
                } else {
                    audio.pause()
                }
            })
        })
    },
    /**
     * @param { CallableFunction } deleteRequestFn
     * @param { Array } datas
     * @param { Element } musicsListElement 
    */
    actionRemoveMusic(musicsListElement, datas, deleteRequestFn) {
        datas.forEach(data => {
            if (data.delete_url) {
                const element = musicsListElement.querySelector(`[data-delete-url="${data.delete_url}"]`)
                element.addEventListener('click', async e => {
                    const confirm = window.confirm('Confirm ?')
                    if (confirm) {
                        Utils.INprogress.set()
                        try {
                            await deleteRequestFn(data.delete_url)
                            element.parentElement.parentElement.innerHTML = '<i class="material-icons" style="color:red">clear</i>'
                        } catch (_) { }
                        Utils.INprogress.unset()
                    }
                })
            }
        })
    }
}