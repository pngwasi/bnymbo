import Swup from 'swup';
import SwupScrollPlugin from '@swup/scroll-plugin';
import NProgress from 'nprogress';
import { AxiosAdmin } from '../../api/api';
import $ from 'jquery';
import 'dropify/dist/css/dropify.css';
import 'dropify';

const Instance = {
    swup: null
}

export default class Utils {

    static get swup() {
        return Instance.swup
    }

    static notifySuccess(message) {
        window.$.notify({
            message: message
        }, {
            type: "success",
            z_index: 11055,
            placement: {
                from: "bottom",
                align: "right"
            },
        })
    }

    static notifyExceptionMessage({ data: { message, errors } }) {
        let text = message;
        if (typeof errors === 'object') {
            text += '<br>'
            for (const key in errors) {
                text += `${errors[key].join(' ')}<br>`
            }
        }

        window.$.notify({
            icon: 'add_alert',
            message: text
        }, {
            type: "danger",
            z_index: 11055,
            placement: {
                from: "bottom",
                align: "right"
            },
        })
    }

    static interceptor() {
        AxiosAdmin.interceptors.request.use(req => {
            return req
        })
        AxiosAdmin.interceptors.response.use(res => {
            return res
        }, (error) => {
            this.notifyExceptionMessage(error.response)
            return Promise.reject(error)
        })
    }

    static dropify() {
        $('.dropify').dropify();
    }

    static init() {
        this.interceptor()
        const h = document.querySelector('#nav-title')
        if (!h) return
        const swup = new Swup({
            containers: ["#app-dashboard", "#nav-title", '#sidebar-wrapper'],
            plugins: [new SwupScrollPlugin()],
            cache: false
        });
        Instance.swup = swup
        this.Nprogress(swup)
        return swup
    };

    static get INprogress() {
        NProgress.configure({
            minimum: 0.4
        })
        const nprogress = {
            unset() {
                NProgress.done()
            },
            set() {
                NProgress.start()
            }
        }
        return nprogress
    }
    /**
     * @param {Swup} swup 
     */
    static Nprogress(swup) {
        const nprogress = this.INprogress
        swup.on('transitionStart', () => nprogress.set())
        swup.on('contentReplaced', () => nprogress.unset())
    }
}
