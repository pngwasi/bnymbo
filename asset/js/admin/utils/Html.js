

export const musicsHtml = (music, editButton = '', likesAndDownloads = true) => {
    const img = music.image
    return (`<div class="uppy-Root">
                <ul class="uppy-Dashboard-files">
                    <li class="uppy-u-reset uppy-DashboardItem">
                        <div class="uppy-DashboardItem-preview" style="height: 120px;">
                            <div class="uppy-Dashboard-Item-previewInnerWrap" style="background-color: ${img ? 'rgba(0,0,0,.56)' : 'rgb(6, 141, 187)'};">
                                <div class="uppy-Dashboard-Item-previewIconWrap" style="z-index: 2; display: ${img ? 'none' : 'block'};">
                                    <span class="uppy-Dashboard-Item-previewIcon" style="color: rgb(6, 141, 187);">
                                        <svg aria-hidden="true" focusable="false" class="UppyIcon" width="25" height="25" viewBox="0 0 25 25">
                                            <path d="M9.5 18.64c0 1.14-1.145 2-2.5 2s-2.5-.86-2.5-2c0-1.14 1.145-2 2.5-2 .557 0 1.079.145 1.5.396V7.25a.5.5 0 0 1 .379-.485l9-2.25A.5.5 0 0 1 18.5 5v11.64c0 1.14-1.145 2-2.5 2s-2.5-.86-2.5-2c0-1.14 1.145-2 2.5-2 .557 0 1.079.145 1.5.396V8.67l-8 2v7.97zm8-11v-2l-8 2v2l8-2zM7 19.64c.855 0 1.5-.484 1.5-1s-.645-1-1.5-1-1.5.484-1.5 1 .645 1 1.5 1zm9-2c.855 0 1.5-.484 1.5-1s-.645-1-1.5-1-1.5.484-1.5 1 .645 1 1.5 1z" fill="#049BCF" fill-rule="nonzero"></path>
                                        </svg>
                                    </span>
                                    <svg aria-hidden="true" focusable="false" class="uppy-Dashboard-Item-previewIconBg" width="58" height="76" viewBox="0 0 58 76">
                                        <rect fill="#FFF" width="58" height="76" rx="3" fill-rule="evenodd"></rect>
                                    </svg>
                                </div>
                                <div style="width: 100%; height: 100%; display: ${img ? 'block' : 'none'};">
                                    <img ${img ? 'src="' + img + '" ' : '(src)'} style="object-fit: cover; width: 100%; height: 100%;">
                                </div>
                            </div>
                        </div>
                        <div class="uppy-Dashboard-Item-fileInfoAndButtons pl-0 mt-2">
                            <div class="uppy-Dashboard-Item-fileInfo">
                                <a style="color:#000" href="${music.url_profile}">
                                    <div class="uppy-Dashboard-Item-name" title="${music.title}">${music.title}</div>
                                </a>
                                ${likesAndDownloads ? `
                                    <div class="uppy-Dashboard-Item-status">
                                        <span class="mr-2"><i class="material-icons" style="color:#f35d5d; font-size: 15px">favorite_border</i> ${music.likes}</span>
                                        <span class="mr-2"><i class="material-icons" style="color:#51c2f7; font-size: 15px">cloud_download</i> ${music.downloads}</span>
                                    </div>
                                `: ''}
                            </div>
                            ${editButton}
                        </div>
                    </li>
                </ul>
            </div>`)
}

export const musicsHtmlWithBtnAction = (music, playBtn = true, delBtn = true, likesAndDownloads = true) => musicsHtml(music, `
    <div class="uppy-DashboardItem-actionWrapper">
        ${playBtn ? `
            <button class="uppy-u-reset uppy-DashboardItem-action uppy-DashboardItem-action--edit music-action-play" data-play="${music.url}" type="button" aria-label="${music.title}" title="Play">
                <div style="display:none">
                    <audio preload="none" controls playsinline controlsList="nodownload">
                        <source src="${music.url}" type="audio/mpeg">
                    </audio>
                </div>
               <i class="material-icons" style="font-size: 19px;">play_circle_outline</i>
            </button>
        `: ''}
        ${delBtn ? `
            <button class="uppy-u-reset uppy-DashboardItem-action uppy-DashboardItem-action--remove music-action-remove" data-delete-url="${music.delete_url}" type="button" aria-label="Remove file" title="Remove file">
                <svg aria-hidden="true" focusable="false" class="UppyIcon" width="13" height="13" viewBox="0 0 18 18">
                    <path d="M9 0C4.034 0 0 4.034 0 9s4.034 9 9 9 9-4.034 9-9-4.034-9-9-9z"></path>
                    <path fill="#FFF" d="M13 12.222l-.778.778L9 9.778 5.778 13 5 12.222 8.222 9 5 5.778 5.778 5 9 8.222 12.222 5l.778.778L9.778 9z"></path>
                </svg>
            </button>
        ` : ''}
    </div>
`, likesAndDownloads)
/**
 * 
 * @param { Object } album 
 */
export const htmlAlbum = (album) => {
    return `
        <div class="card card-background" style="background-image: url('${album.album_image}');">
            <div class="card-body" style="min-height: auto;">
                <h6 class="card-category text-info"><i class="material-icons">album</i></h6>
                <a href="${album.url}">
                    <h6 class="card-title">${album.album_name}</h6>
                </a>
                <div class="mt-3">
                    <a href="javascript:;" class="inline-block text-white mr-2">
                        <i class="material-icons" style="font-size: 15px">library_music</i> ${album.musics}
                    </a>
                    <a href="javascript:;" class="inline-block text-white">
                        <i class="material-icons" style="color:#51c2f7; font-size: 15px">cloud_download</i> ${album.downloads}
                    </a>
                </div>
            </div>
        </div>`
}

/**
 * 
 * @param { Object } playlist 
 */
export const htmlPlaylist = (playlist) => {
    return `
    <div class="card card-background" style="background-color: white">
        <div class="card-body" style="min-height: auto;">
            <h6 class="card-category text-info"><i class="material-icons" style="font-size: 36px">playlist_play</i></h6>
            <a href="${playlist.url}">
                <h6 class="card-title">${playlist.name}</h6>
            </a>
            <div class="mt-2">
                <div>
                    <a href="javascript:;" class="inline-block text-white mr-2">
                        <i class="material-icons" style="color:#51c2f7; font-size: 19px">queue_music</i> ${playlist.musics}
                    </a>
                </div>
            </div>
        </div>
    </div>`
}
/**
 * 
 * @param { Object } music 
 */
export const HtmlAudio = (music) => `
<div class="container-audio">
    <div class="mb-1 ml-1">
        <b><small>
            <a href="${music.url_profile}" style="color:#444">${music.title}</a>
        </small></b>
    </div>
    <audio preload="none" controls playsinline controlsList="nodownload">
        <source src="${music.url}" type="audio/mpeg">
        Your browser dose not Support the audio Tag
    </audio>
</div>
`