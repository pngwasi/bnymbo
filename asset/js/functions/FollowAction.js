import { followAction } from "../api/actions/Follow"

/**
 * 
 * @param { DocumentFragment } fragment 
 * @param { HTMLElement } element 
 */
export const FollowAction = (fragment, element) => {
    Array.from((fragment || element).querySelectorAll('.btn-follow'))
    .forEach(el => {
        const followSpan = el.querySelector('.follow')
        const followingSpan = el.querySelector('.following')
        const followUrlAction = el.dataset.urlAction
        if (!followSpan || !followingSpan || !followUrlAction) return
        el.addEventListener('click', () => followAction(followUrlAction, followSpan, followingSpan))
    })
}