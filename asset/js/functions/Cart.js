export const cartStorage = 'bnb-cart';

export const cartEvent = 'update-card';

export const cartIStorage = () => window.localStorage.getItem(cartStorage)

/**
 * 
 * @param { Array } items 
 */
export const updateItemsCart = (items) => {
    window.localStorage.setItem(cartStorage, JSON.stringify(items));
}