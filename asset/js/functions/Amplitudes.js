
export class Amplitudes {

    /**
     * @returns Object
     */
    static instance() {
        return this.Amplitude
    }

    /**
     * @param {Array} songs 
     */
    static async init(songs = [], params = {}) {
        const Amplitude = this.Amplitude = await import('amplitudejs')
        if (!songs.length) return
        songs.forEach(song => {
            const newIndex = Amplitude.addSong(song);
            this.appendToSongDisplay(song, newIndex);
            // Amplitude.bindNewElements();
        })
        Amplitude.init({ songs: songs, ...params });
        if (Amplitude.getConfig().audio) Amplitude.getConfig().audio.preload = 'none'
        document.querySelector('.show-playlist').addEventListener('click', () => {
            document.querySelector('#white-player-playlist-container').classList.remove('slide-out-top');
            document.querySelector('#white-player-playlist-container').classList.add('slide-in-top');
            document.querySelector('#white-player-playlist-container').style.display = "block";
        });
        document.querySelector('.close-playlist').addEventListener('click', () => {
            document.querySelector('#white-player-playlist-container').classList.remove('slide-in-top');
            document.querySelector('#white-player-playlist-container').classList.add('slide-out-top');
            document.querySelector('#white-player-playlist-container').style.display = "none";
        });
        return Amplitude
    }


    static appendToSongDisplay(song, index) {
        const playlistElement = document.querySelector('.white-player-playlist');
        const playlistSong = document.createElement('div');
        playlistSong.setAttribute('class', 'white-player-playlist-song amplitude-song-container amplitude-play-pause');
        playlistSong.setAttribute('data-amplitude-song-index', index);
        const playlistSongImg = document.createElement('div')
        playlistSongImg.className = 'playlistSongImg'
        const _playlistSongImg = document.createElement('img');
        _playlistSongImg.style.objectFit = 'cover'
        _playlistSongImg.style.width = "100%"
        _playlistSongImg.style.height = "100%"
        _playlistSongImg.setAttribute('src', song.cover_art_url);
        playlistSongImg.appendChild(_playlistSongImg)
        const playlistSongMeta = document.createElement('div');
        playlistSongMeta.setAttribute('class', 'playlist-song-meta');
        const playlistSongName = document.createElement('span');
        playlistSongName.setAttribute('class', 'playlist-song-name');
        playlistSongName.innerHTML = song.name;
        const playlistSongArtistAlbum = document.createElement('span');
        playlistSongArtistAlbum.setAttribute('class', 'playlist-song-artist');
        playlistSongArtistAlbum.innerHTML = song.artist + ' &bull; ' + song.album;
        playlistSongMeta.appendChild(playlistSongName);
        playlistSongMeta.appendChild(playlistSongArtistAlbum);
        playlistSong.appendChild(playlistSongImg);
        playlistSong.appendChild(playlistSongMeta);
        playlistElement.appendChild(playlistSong);
    }
}
