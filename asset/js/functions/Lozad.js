import lozad from 'lozad';

const observer = lozad();

export const LozadObserver = () => observer.observe();
