import { INprogress } from "../../functions/NProgress"
import Api from "../api"

/**
 * 
 * @param { String } url 
 * @param { String } locale 
 */
export const ChangeLocale = async (url, locale) => {
    INprogress.set()
    try {
        await Api.post(encodeURI(url), { locale });
    } catch (error) { }
    INprogress.unset()
    return;
}