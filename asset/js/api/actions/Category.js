import { INprogress } from "../../functions/NProgress"
import Api from "../api"

/**
 * 
 * @param { String } url 
 * @param { String } locale 
 */
export const ChangeCategory = async (url, category) => {
    INprogress.set()
    try {
        await Api.post(encodeURI(url), { category });
    } catch (error) { }
    INprogress.unset()
    return;
}