import { INprogress } from "../../functions/NProgress"
import Api from "../api"
import { SwupApp } from "../../functions/Swup"

/**
 * 
 * @param { String } urlLikeAction 
 * @param { HTMLElement } LikeElement 
 */
export const LikeAction = async (urlLikeAction, LikeElement) => {
    INprogress.set()
    const span = LikeElement.querySelector('.count')
    try {
        const url = `${urlLikeAction}?url=${window.location.href}&nonce=${Date.now()}`
        const { data, status } = await Api.post(encodeURI(url));
        if (status === 200) {
            if (data.action === 'like') {
                LikeElement.classList.add('active');
            } else if (data.action === 'unlike') {
                LikeElement.classList.remove('active');
            }
            span.textContent = data.count
        }
    } catch ({ response }) {
        INprogress.unset()
        if (response && response.status === 302) {
            const url = new URL(response.data.redirect_url)
            SwupApp.swup.loadPage({
                url: url.pathname + url.search
            })
        }
    }
    INprogress.unset()
}