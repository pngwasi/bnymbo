import { INprogress } from "../../functions/NProgress";
import Api from "../api";

/**
 * 
 * @param { String } url 
 * @param { Object } datas 
 */
export const AddToCard = async (url, datas) => {
    INprogress.set()
    try {
        const { data } = await Api.post(encodeURI(url), datas);
        INprogress.unset()
        return data
    } catch ({ response }) {
        INprogress.unset()
        return null
    }
}