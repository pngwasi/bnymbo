import { INprogress } from "../../functions/NProgress";
import { SwupApp } from "../../functions/Swup";
import Api from "../api";

/**
 * 
 * @param { String } urlLikeAction 
 * @param { HTMLElement } followSpan 
 * @param { HTMLElement } followingSpan 
 */
export const followAction = async (followUrlAction, followSpan, followingSpan) => {
    INprogress.set()
    try {
        const url = `${followUrlAction}?url=${window.location.href}&nonce=${Date.now()}`
        const { data, status } = await Api.post(encodeURI(url));
        if (status === 200) {
            if (data.action === 'follow') {
                followSpan.style.display = 'none'
                followingSpan.style.display = 'block'
            } else if (data.action === 'unfollow') {
                followSpan.style.display = 'block'
                followingSpan.style.display = 'none'
            }
        }
    } catch ({ response }) {
        INprogress.unset()
        if (response && response.status === 302) {
            const url = new URL(response.data.redirect_url)
            SwupApp.swup.loadPage({
                url: url.pathname + url.search
            })
        }
    }
    INprogress.unset()
}