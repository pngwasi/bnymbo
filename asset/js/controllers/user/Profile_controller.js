import { Controller } from "stimulus"

export default class extends Controller {

    static targets = ['imageDisplay']

    connect() { }

    onChangeImage = e =>
        this.imageDisplay.src = URL.createObjectURL(e.target.files[0]);

    /**
     * @returns { HTMLImageElement }
    */
    get imageDisplay() {
        return this.imageDisplayTarget
    }

}
