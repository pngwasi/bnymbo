import { Controller } from "stimulus"
import { LikeAction } from '../api/actions/Like'

export default class extends Controller {

    static targets = ['playBtn', 'LikeEl']

    connect() {
        this.urlLikeAction = this.LikeElement.dataset.urlAction
    }

    like = () =>
        LikeAction(this.urlLikeAction, this.LikeElement);

    /**
     * @returns { HTMLElement }
     */
    get LikeElement() {
        return this.LikeElTarget
    }
}
