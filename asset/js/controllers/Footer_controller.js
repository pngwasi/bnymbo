import { Controller } from "stimulus"
import { setI18nLanguage } from "../api/api"
import { ChangeLocale } from "../api/actions/Locale"
import { SwupApp } from "../functions/Swup"

export default class extends Controller {
    static targets = ['Locale']

    connect() {
        this.locale.value = document.querySelector('html').lang
        this.locale.addEventListener('change', this.changeLocale)
    }

    /**
     * @param { Event } e
     */
    changeLocale = async (e) => {
        const { value } = e.target
        setI18nLanguage(value)
        await ChangeLocale(this.data.get('locale'), value)
        window.scrollTo({
            top: 0
        })
        SwupApp.swup.loadPage({
            url: window.location.pathname + window.location.search
        }, true)
    }

    disconnect() {
        this.locale.removeEventListener('change', this.changeLocale)
    }

    /** 
     * @return { HTMLSelectElement }
    */
    get locale() {
        return this.LocaleTarget
    }

}
