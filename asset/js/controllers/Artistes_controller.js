import { Controller } from "stimulus"
import { FollowAction } from "../functions/FollowAction"


export default class extends Controller {

    connect() {
        this.followersAction()
    }

    /**
     * @param { DocumentFragment } fragment 
     */
    followersAction = (fragment)  => FollowAction(fragment, this.element)

}
