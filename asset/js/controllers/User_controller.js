import { Controller } from "stimulus"
import Api from "../api/api"
import { SwupApp } from "../functions/Swup"
import { LozadObserver } from "../functions/Lozad"
import { FollowAction } from "../functions/FollowAction"

let scrollTop = 0
let scrollX = 0
let lastUrl = null

export default class extends Controller {

    async connect() {
        const path = window.location.pathname.split('/')
        const url = path.slice(0, path.length - 1).join('/')
        if (lastUrl !== url) {
            lastUrl = url
            scrollTop = 0
        }
        const activeNav = this.element.querySelector('.page-header-user .nav .active')
        activeNav && activeNav.scrollIntoView({ inline: "center"})

        this.showMore = this.element.querySelector('#load--more')
        this.rowElement = this.element.querySelector('#rowElement')
        if (this.showMore && this.rowElement) {
            this.nextPageUrl = this.showMore.getAttribute('data-next')
            this.showMore.addEventListener('click', this.loadMore)
        }
        const auth = document.querySelector('.station--user')
        SwupApp.swup.scrollTo(auth ? scrollTop : 0);
        window.addEventListener("scroll", this.performWindowScroll)
        this.followersAction()
    }

    removeEvent() {
        if (this.showMore) {
            this.showMore.removeEventListener('click', this.loadMore)
        }
    }

    performWindowScroll() {
        scrollTop = window.pageYOffset
    }

    disconnect() {
        this.removeEvent()
        window.removeEventListener("scroll", this.performWindowScroll)
    }

    /**
     * @param { DocumentFragment } fragment 
     */
    followersAction = (fragment)  => FollowAction(fragment, this.element)

    loadMore = async () => {
        if (!this.nextPageUrl) return
        this.setSpinner(this.showMore)
        try {
            const { data } = await Api.get(this.nextPageUrl)
            this.nextPageUrl = data.nextPageUrl
            if (!data.nextPageUrl) {
                this.removeEvent()
                this.showMore.remove()
            }
            const fragment = this.contextualFragment(data.html)
            this.followersAction(fragment)
            this.rowElement.appendChild(fragment)
            LozadObserver()
        } catch (error) { }
        this.unsetSpinner()
    }

    /**
     * @param {array} fragment 
     * @returns { DocumentFragment }
    */
    contextualFragment(fragment) {
        return document.createRange().createContextualFragment(fragment)
    }

    /**
     * @param { HTMLElement } element
     */
    setSpinner(element) {
        const spinner = this.spinner = element.querySelector('.loading')
        this.spinnerInnerHTML = spinner.innerHTML.trim()
        const splited = this.spinnerInnerHTML.split(' ')[0]
        spinner.innerHTML = splited
        let cout = 0
        this.timer = window.setInterval(() => {
            if (cout === 3) {
                cout = 0
                spinner.innerHTML = ''
            }
            spinner.innerHTML += splited
            cout++
        }, 600)
    }

    unsetSpinner() {
        if (this.spinner) {
            this.spinner.innerHTML = this.spinnerInnerHTML
            clearInterval(this.timer)
        }
    }
}
