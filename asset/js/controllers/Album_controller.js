import { Controller } from "stimulus"
import { LikeAction } from '../api/actions/Like'
import { Amplitudes } from "../functions/Amplitudes"

export default class extends Controller {
    songsList = $music || []
    static targets = ['playBtn', 'LikeEl']

    connect() {
        window.requestAnimationFrame(this.init)
    }

    init = async () => {
        this.urlLikeAction = this.LikeElement.dataset.urlAction
        this.am = await Amplitudes.init(this.songsList)
        if (!this.audioElement) return
        this.audioElement.addEventListener('play', () => {
            if (!this.playBtn.classList.contains('active')) {
                this.playBtn.classList.add('active')
            }
        })
        this.audioElement.addEventListener("pause", () => {
            if (this.playBtn.classList.contains('active')) {
                this.playBtn.classList.remove('active')
            }
        })
    }


    playOrPause() {
        if (!this.audioElement) return
        if (!this.playBtn.classList.contains('active')) {
            this.audioElement.play()
        } else {
            this.audioElement.pause()
        }
    }

    like = () =>
        LikeAction(this.urlLikeAction, this.LikeElement);

    disconnect() {
        if (this.am) {
            this.am.pause()
            this.songsList.forEach((m, i) => this.am.removeSong(i))
        }
    }

    /**
     * @returns { HTMLElement }
     */
    get LikeElement() {
        return this.LikeElTarget
    }
    /**
     * @returns { HTMLAudioElement }
     */
    get audioElement() {
        if (this.am) {
            return this.am.getConfig().audio
        }
        return null;
    }

    /**
     * @returns {HTMLElement}
    */
    get playBtn() {
        return this.playBtnTarget
    }

}
