import { Controller } from "stimulus"
import { cartEvent } from "../functions/Cart"
import { AddToCard } from "../api/actions/AddToCard"

export default class extends Controller {

    static targets = ['addCartAction', 'add', 'view']
    redirect = false

    initialize() {
        this.addtocardUrl = this.data.get('addtocardUrl')
        try {
            this.datas = JSON.parse(this.data.get('details'))
        } catch (error) {
            this.datas = {}
            console.error('Datas are required !')
        }
    }

    connect() {
        this.addCartAction.addEventListener('click', this.addToCard)
    }

    viewCartHtml() {
        this.add.style.display = 'none';
        this.view.style.display = 'inline';
        this.redirect = true
        this.addCartAction.href = this.data.get('href')
    }

    /**
     * 
     * @param { Object } item
     */
    addItemCartStorage(items) {
        this.viewCartHtml()
        window.dispatchEvent(new CustomEvent(cartEvent, {
            detail: {
                itemLength: items.length
            }
        }))
    }

    /**
     * 
     * @param { Event } e 
     */
    addToCard = async (e) => {
        if (!this.redirect) {
            e.preventDefault()
            const cartItem = await AddToCard(this.addtocardUrl, this.datas)
            cartItem && this.addItemCartStorage(cartItem)
        }
    }

    disconnect() {
        this.addCartAction.removeEventListener('click', this.addToCard)
    }

    /**
     * @returns { HTMLLinkElement }
     */
    get addCartAction() {
        return this.addCartActionTarget
    }

    /**
     * @returns { HTMLElement }
     */
    get add() {
        return this.addTarget
    }

    /**
     * @returns { HTMLElement }
     */
    get view() {
        return this.viewTarget
    }

}
