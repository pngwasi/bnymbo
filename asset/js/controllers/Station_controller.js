import { Controller } from "stimulus"
import { Amplitudes } from "../functions/Amplitudes"
import Api from "../api/api"
import { LikeAction } from '../api/actions/Like'

export default class extends Controller {
    songsList = $music || []
    noPlayed = true
    static targets = ['playBtn', 'LikeEl']

    connect() {
        window.requestAnimationFrame(this.init)
    }

    init = async () => {
        this.urlLikeAction = this.LikeElement.dataset.urlAction
        this.playingUlr = this.playBtn.dataset.urlPlayings
        this.am = await Amplitudes.init(this.songsList)
        if (!this.audioElement) return
        this.audioElement.addEventListener('play', () => {
            if (!this.playBtn.classList.contains('active')) {
                this.playBtn.classList.add('active')
            }
            this.playingCount()
        })
        this.audioElement.addEventListener("pause", () => {
            if (this.playBtn.classList.contains('active')) {
                this.playBtn.classList.remove('active')
            }
        })

    }

    async playingCount() {
        try {
            this.noPlayed && await Api.post(this.playingUlr)
            this.noPlayed = false
        } catch (_) { }
    }

    playOrPause() {
        if (!this.audioElement) return
        if (!this.playBtn.classList.contains('active')) {
            this.audioElement.play()
        } else {
            this.audioElement.pause()
        }
    }

    like = () =>
        LikeAction(this.urlLikeAction, this.LikeElement);

    disconnect() {
        if (this.am) {
            this.am.pause()
            this.songsList.forEach((m, i) => this.am.removeSong(i))
        }
    }

    /**
     * @returns { HTMLElement }
     */
    get LikeElement() {
        return this.LikeElTarget
    }
    /**
     * @returns { HTMLAudioElement }
     */
    get audioElement() {
        return this.am.getConfig().audio
    }

    /**
     * @returns {HTMLElement}
    */
    get playBtn() {
        return this.playBtnTarget
    }
}
