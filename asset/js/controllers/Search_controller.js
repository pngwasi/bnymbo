import { Controller } from "stimulus"

export default class extends Controller {

    connect() {
        const activeNav = this.element.querySelector('.page-header .nav .current_page_item')
        activeNav && activeNav.scrollIntoView({ inline: "center" })
    }

}
