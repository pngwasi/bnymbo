import { Controller } from "stimulus"
import { cartEvent } from "../functions/Cart";

const SCROLLING_NAVBAR_OFFSET_TOP = 50;

export default class extends Controller {

    static targets = ['cartIcon']

    initialize() {
        this.collapsed = false
        this.primary = $(".primary-nav")
        this.collapse = document.querySelector('button.navbar-toggler')
        this.searchBar = document.querySelector('#toggle--search-bar')
        this.$searchBar = Array.from(document.querySelectorAll('.search-bar--event'))
        this.$closeSearchBar = document.querySelector('#toggle--search-bar .close')
    }

    connect() {
        this.scrollNav()
        this.cartIconState()
        window.addEventListener('scroll', this.scrollNav)
        this.collapse.addEventListener('click', this.collapseCb)
        this.$searchBar.forEach(el => el.addEventListener('click', this.toggleSearchBar))
        this.$closeSearchBar.addEventListener('click', this.toggleSearchBar)
    }

    disconnect() {
        window.removeEventListener('scroll', this.scrollNav)
        window.removeEventListener(cartEvent, this.updateCartIconState)
        this.collapse.removeEventListener('click', this.collapseCb)
        this.$searchBar.forEach(el => el.removeEventListener('click', this.toggleSearchBar))
        this.$closeSearchBar.removeEventListener('click', this.toggleSearchBar)
    }

    collapseCb = () => {
        if (!this.collapsed) {
            ($(window).scrollTop() <= SCROLLING_NAVBAR_OFFSET_TOP) && this.primary.addClass("affix");
            this.collapsed = true
        } else {
            ($(window).scrollTop() <= SCROLLING_NAVBAR_OFFSET_TOP) && this.primary.removeClass("affix");
            this.collapsed = false
        }
    }

    scrollNav = () => {
        if ($(window).scrollTop() > SCROLLING_NAVBAR_OFFSET_TOP) {
            if (!this.primary.hasClass("affix")) {
                this.primary.addClass("affix");
            }
        } else if (!this.collapsed) {
            this.primary.removeClass("affix");
        }
    }

    toggleSearchBar = () => {
        const h = this.searchBar.classList
        if (h.contains('open')) {
            h.remove('open')
            this.inputSearch.blur()
        }else {
            h.add('open')
            this.inputSearch.focus()
        }
    }
    
    /**
     * @returns { HTMLInputElement }
     */
    get inputSearch() {
       return document.querySelector('#search-bar--input')
    }
    /**
     * 
     * @param { Event } e 
     */
    updateCartIconState = ({ detail: { itemLength } }) => {
        this.cartIcon.forEach((el) => {
            if (!itemLength) {
                el.classList.add('cart--shopping')
            } else {
                el.classList.remove('cart--shopping')
                el.querySelector('.notification').textContent = itemLength
            }
        })
    }

    cartIconState() {
        window.addEventListener(cartEvent, this.updateCartIconState)
    }

    /**
     * @returns { HTMLElement[] }
     */
    get cartIcon() {
        return this.cartIconTargets
    }

}
