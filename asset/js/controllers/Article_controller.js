import { Controller } from "stimulus"
import { convertToHtml } from "@js/functions/jsonToHtml";

export default class extends Controller {
    static targets = ['content']

    connect() {
        const html = convertToHtml(this.blocks)
        this.content.innerHTML = html
    }
    /**
     * @returns { Array }
     */
    get blocks() {
        return window.$article.blocks
    }

    /**
     * @returns { HTMLElement }
     */
    get content() {
        return this.contentTarget
    }
}
