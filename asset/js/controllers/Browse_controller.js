import { Controller } from "stimulus"
import { SwupApp } from "../functions/Swup"
import { ChangeCategory } from "../api/actions/Category";

export default class extends Controller {

    connect() {
        Array.from(this.element.querySelectorAll('#caterogies--list a'))
            .forEach((el) => {
                const type = el.getAttribute('data-type')
                if (!type) return
                el.addEventListener('click', () => {
                    ChangeCategory(this.data.get('categoryUrl'), type)
                    .then(() => SwupApp.swup.loadPage({
                        url: window.location.pathname + window.location.search
                    }, true))
                })
            })
        const activeNav = this.element.querySelector('.page-header .nav .current_page_item')
        activeNav && activeNav.scrollIntoView({ inline: "center" })
    }

}
