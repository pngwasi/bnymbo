import { useState, useEffect } from 'react'
import Api from "@/js/api/api";

/**
 * 
 * @param { string } url 
 */
export const useCartItems = (url) => {
    const [cartItems, setCartItems] = useState([])
    const [total, setTotal] = useState(0)
    const [loaded, setLoaded] = useState(false)
    const [coupon, setCoupon] = useState({
        code: '',
        amount: 0,
        off: 0
    })
    useEffect(() => {
        (async () => {
            try {
                const { data: { items, coupon } } = await Api.get(encodeURI(url))
                setCartItems(items)
                setTotal(items.reduce((acc, item) => {
                    acc = acc + (+item.price)
                    return acc
                }, 0))
                setLoaded(true)
                setCoupon(coupon)
            } catch (_) {
                console.log(_.reponse);
            }
        })()
    }, [])

    return {
        cartItems,
        setCartItems,
        total,
        setTotal,
        loaded,
        setLoaded,
        coupon,
        setCoupon
    }
}