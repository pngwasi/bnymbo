import React, { Fragment, useState, useCallback, useMemo, useContext, createContext } from 'react'
import { render, unmountComponentAtNode } from 'react-dom'
import i18n from "i18next";
import { useTranslation, initReactI18next, Trans } from "react-i18next";
import { Checked } from './svg/Svg';
import { useCartItems } from './hooks/CartItems';
import { Spinner } from './components/Components';
// import { cartEvent } from "@/js/functions/Cart";
// import Api from "@/js/api/api";
// import { INprogress } from "@/js/functions/NProgress";


const SERVICES = {
    services: 'services',
    paymentCard: 'paymentCard',
    paypal: 'paypal'
}
const user = { ...window.authUser };

const SectionContext = createContext({
    section: SERVICES.services,
    changeSection: () => { },
    url: {}
})

const CheckedIcon = () => <span className="badge"><Checked /> </span>

const Services = () => {
    const { t } = useTranslation();
    const [service, setService] = useState('Vodacom')

    return <>
        <div className="row justify-content-center">
            <div className="col-4 col-md-3">
                <div className="card clickable selected" onClick={() => setService('Vodacom')}>
                    <div className="card-body">
                        <div style={{ height: "50px" }}>
                            <img className="img-cover-full" src="/images/services/vodacom.jpg" alt="Vodacom" />
                        </div>
                    </div>
                    {service === 'Vodacom' && <CheckedIcon />}
                </div>
            </div>
            <div className="col-4 col-md-3">
                <div className="card clickable selected" onClick={() => setService('Airtel')}>
                    <div className="card-body">
                        <div style={{ height: "50px" }}>
                            <img className="img-cover-full" src="/images/services/airtel.gif" alt="Airtel" />
                        </div>
                    </div>
                    {service === 'Airtel' && <CheckedIcon />}
                </div>
            </div>
            <div className="col-4 col-md-3">
                <div className="card clickable selected" onClick={() => setService('Orange')}>
                    <div className="card-body">
                        <div style={{ height: "50px" }}>
                            <img className="img-cover-full" src="/images/services/orange.png" alt="Orange" />
                        </div>
                    </div>
                    {service === 'Orange' && <CheckedIcon />}
                </div>
            </div>
        </div>
        <div className="row justify-content-center mt-5">
            <div className="col-12 col-sm-12 col-md-9">
                <div style={{ fontSize: '12px' }} className="mb-2">
                    {t('Actually, we only support numbers starting with')} <span>(+243)</span>.
                </div>
                <div className="row">
                    <div className="col-md-3 col-sm-4 col-5">
                        <div className="form-group m-0">
                            <input type="text" placeholder={service} readOnly={true} className="form-control" />
                        </div>
                    </div>
                    <div className="col-md-6 col-sm-8 col-7">
                        <div className="form-group m-0">
                            <input type="text" placeholder={t('Phone Number')} className="form-control" />
                        </div>
                    </div>
                </div>
                <div className="form-group form-check mt-5">
                    <input type="checkbox" className="form-check-input" id="defaultNumber" />
                    <label className="form-check-label" htmlFor="defaultNumber">{t('Use this number as default for')} {service}</label>
                </div>
            </div>
        </div>
    </>
}

const PaymentCard = () => {
    const { t } = useTranslation();

    return <div className="card-payment m-3">
        <div className="login-form m-0">
            <p>
                <label htmlFor="cardname">{t('Name on Card')}</label>
                <input id="cardname" type="text" name='cardname' placeholder={t('Name on Card')} className="input" />
            </p>
            <p>
                <label htmlFor="cardnumber">{t('Card Number')}</label>
                <input id="cardnumber" type="text" name='cardnumber' placeholder={t('Card Number')} className="input" />
            </p>
            <div className="row">
                <div className="col-sm-12 col-md-6">
                    <p>
                        <label htmlFor="m_y">{t('MM / YY')}</label>
                        <input id="m_y" type="text" name='m_y' placeholder={t('MM / YY')} className="input" />
                    </p>
                </div>
                <div className="col-sm-12 col-md-6">
                    <p>
                        <label htmlFor="security_code">{t('Security Code')}</label>
                        <input id="security_code" type="text" name='security_code' placeholder={t('Security Code')} className="input" />
                    </p>
                </div>
            </div>
            <div className="row">
                <div className="col-sm-12 col-md-6">
                    <p>
                        <label htmlFor="zip">{t('Zip/Code Postal')}</label>
                        <input id="zip" type="text" name='zip' placeholder={t('Zip/Code Postal')} className="input" />
                    </p>
                </div>
            </div>
            <div className="form-group form-check mt-2">
                <input type="checkbox" className="form-check-input" id="remember_card_info" />
                <label className="form-check-label" htmlFor="remember_card_info">{t('Remember this Card')}</label>
            </div>
        </div>
    </div>
}

const PayPal = () => {
    const { t } = useTranslation();
    return <div className="m-3">
        {t("In order to complete your transaction, we will transfer you over to PayPal's secure servers")}.
    </div>
}

const CheckoutServices = () => {
    const { t } = useTranslation();
    const { changeSection, section: service } = useContext(SectionContext)

    const componentsItems = useMemo(() => ({
        services: <Services />,
        paymentCard: <PaymentCard />,
        paypal: <PayPal />
    }))

    const handleService = useCallback(({ target: { value } }) => {
        changeSection(value)
    });
    return <div className="mt-3">
        <div className="custom-control custom-radio">
            <input type="radio" id="services" value="services" checked={service === SERVICES.services} name="paymentService" onChange={handleService} className="custom-control-input" />
            <label className="custom-control-label" htmlFor="services">{t('Services')}</label>
        </div>
        <div className="custom-control custom-radio">
            <input type="radio" id="paymentCard" value="paymentCard" checked={service === SERVICES.paymentCard} name="paymentService" onChange={handleService} className="custom-control-input" />
            <label className="custom-control-label" htmlFor="paymentCard">{t('Payment Card')}</label>
        </div>
        <div className="custom-control custom-radio">
            <input type="radio" id="paypal" value="paypal" checked={service === SERVICES.paypal} name="paymentService" onChange={handleService} className="custom-control-input" />
            <label className="custom-control-label" htmlFor="paypal">
                <span className="mr-3">{t('PayPal')}</span>
                <img src="/images/services/paypal.jpg" alt="PayPal Icon" className="ml-space-sm" width="74" height="20" />
            </label>
        </div>
        <div className="card my-3" style={{ borderColor: "#dedfe0", background: "#f8f8f9" }}>
            <div className="card-body">
                {componentsItems[service]}
                <span className="float-right" style={{ fontSize: "11px", color: "#cacbcc" }}>
                    <Trans i18nKey="secure" lang="en" >
                        Secure<br />Connection
                    </Trans>
                </span>
            </div>
        </div>
    </div>
}

const PaymentProccessAction = ({ loaded, total, orgin_total, coupon }) => {
    const { t } = useTranslation();
    const { section } = useContext(SectionContext)
    const available = (section === SERVICES.services && orgin_total)

    const content = <>
        <h4 className="card-title">{t('Summary')}</h4>
        <div className="card-text">
            <div>
                <span>{t('Original price')}:</span>
                <span className="float-right">${(+orgin_total).nround(3)}</span>
            </div>
            <div>
                <span>{t('Coupon discounts')}:</span>
                <span className="float-right">-${coupon}</span>
            </div>
        </div>
        <hr />
        <div className="card-text">
            <div>
                <b>{t('Total')}:</b>
                <b className="float-right">${(+total).nround(3)}</b>
            </div>
        </div>
        <p style={{ fontSize: '12px' }} className="mt-2 mb-1">{t('By completing your purchase you agree to these Terms of Service')}.</p>
        <button disabled={!available}
            type="button" className={`btn-block ${available ? 'button button-primary' : 'btn'}`}>
            {section === SERVICES.paypal ? t('Proceed') : t('Complete Payment')}
        </button>
    </>

    return <div className="card">
        <div className="card-body disabled-action">
            {!loaded && <Spinner />}
            {loaded && content}
        </div>
    </div>
}

const BillingDetails = () => {
    const { t } = useTranslation();
    return <div>
        <div>{t('Billing email')}: <b>{user.email}</b></div>
    </div>
}

const Checkout = ({ urls }) => {
    const [section, setSection] = useState(SERVICES.services)
    const { loaded, total, coupon } = useCartItems(urls.cartItems)
    const changeSection = useCallback(s => setSection(s))
    const defaultSection = useMemo(() => ({
        section, changeSection, urls
    }), [section])

    return <>
        <SectionContext.Provider value={defaultSection}>
            <div className="row">
                <div className="col-sm-12 col-md-8">
                    <div className="billing-details">
                        <BillingDetails />
                        <CheckoutServices />
                    </div>
                </div>
                <div className="col-sm-12 col-md-4">
                    <PaymentProccessAction {...{ loaded, orgin_total: total, coupon: coupon.amount, total: (total - coupon.amount) }} />
                </div>
            </div>
        </SectionContext.Provider>
    </>
}

/**
 * @param {HTMLElement} element
 */
export const init = (element, locale, urls = {}) => {
    i18n
        .use(initReactI18next)
        .init({
            resources: {
                fr: {
                    translation: {
                        "Summary": "Résumé",
                        "Original price": "Prix d'origine",
                        "Coupon discounts": "Rabais sur les coupons",
                        "Total": "Total",
                        "By completing your purchase you agree to these Terms of Service": "En finalisant votre achat, vous acceptez ces conditions d'utilisation",
                        "Complete Payment": "Compléter le paiement",
                        "Billing email": "E-mail de facturation",
                        "Actually, we only support numbers starting with": "Actuellement, nous supportons seulement les numero commencant par",
                        "Use this number as default for": "Utiliser ce numéro par défaut pour",
                        "Remember this Card": 'Se souvenir de cette carte',
                        "secure": 'Connexion<br />Sécurisée',
                        "Name on Card": 'Nom sur la carte',
                        'Card Number': 'Numéro de carte',
                        "Security Code": "Code de sécurité",
                        "In order to complete your transaction, we will transfer you over to PayPal's secure servers": "Afin de compléter votre transaction, nous vous transférerons vers les serveurs sécurisés de PayPal",
                        "Proceed": "Procéder"
                    }
                }
            },
            lng: locale,
            interpolation: {
                escapeValue: false
            }
        });
    render(<Checkout urls={urls} />, element)
    return () => unmountComponentAtNode(element)
}