import React, { Fragment, useCallback, useRef } from 'react'
import { render, unmountComponentAtNode } from 'react-dom'
import i18n from "i18next";
import { useTranslation, initReactI18next } from "react-i18next";
import { cartEvent } from "@/js/functions/Cart";
import Api from "@/js/api/api";
import { INprogress } from "@/js/functions/NProgress";
import { useCartItems } from './hooks/CartItems';
import { Spinner } from './components/Components';


const ImageItem = ({ url }) => (
    <div className="sm-list">
        <article className="block-loop-sm block-loop-item magin-none">
            <figure className="post-thumbnail" style={{ backgroundImage: `url(${url})` }}></figure>
        </article>
    </div>
)

const CartItems = ({ items, onDeleteCartItem, loaded }) => {
    const { t } = useTranslation();

    const content = <div className="table-responsive">
        <table className="table" style={{ fontSize: 'small' }}>
            <thead>
                <tr>
                    <td scope="col"></td>
                    <td scope="col"></td>
                    <td scope="col">{t('Product')}</td>
                    <td scope="col">{t('Price')}</td>
                    <td scope="col">{t('Type')}</td>
                    <td scope="col">{t('Total')}</td>
                </tr>
            </thead>
            <tbody>
                {items.map((item) => (
                    <tr key={item.id}>
                        <th scope="row">
                            <a style={{ cursor: 'pointer' }} onClick={() => onDeleteCartItem(item)}>X</a>
                        </th>
                        <td><ImageItem url={item.image} /></td>
                        <td>{item.product}</td>
                        <td>${item.price}</td>
                        <td>{item.section}</td>
                        <td>${item.price}</td>
                    </tr>
                ))}
            </tbody>
        </table>
        {items.length < 1 && <h5 className="text-center mt-4">{t('Your cart is empty')}</h5>}
    </div>

    return <div className="card-items">
        {!loaded && <Spinner />}
        {loaded && content}
        <hr />
    </div>
}

const CouponForm = ({ applyCoupon, couponInput }) => {
    const { t } = useTranslation();
    return <div className="mt-4">
        <div className="coupon">
            <form autoComplete="off" onSubmit={applyCoupon} method="POST">
                <input type="text" ref={couponInput} name="coupon_code" className="input-text input" id="coupon_code"
                    placeholder={t('Coupon code')} />
                <button type="submit" className="button ml-1" name="apply_coupon">{t('Apply coupon')}</button>
            </form>
        </div>
    </div>
}

const CartTotals = ({ subtotal, total, coupon = {} }) => {
    const { t } = useTranslation();
    return <div className="table-responsive">
        <table cellSpacing="0" className="shop_table shop_table_responsive table">
            <tbody>
                <tr className="cart-subtotal">
                    <th>{t('Coupon')} <small>{coupon.code}</small></th>
                    <td>
                        <span className="woocommerce-Price-amount amount">
                            <span className="woocommerce-Price-currencySymbol">$</span>
                            {coupon.amount} <span>({coupon.off}%)</span>
                        </span>
                    </td>
                </tr>
                <tr className="cart-subtotal">
                    <th>{t('Subtotal')}</th>
                    <td data-title="Subtotal">
                        <span className="woocommerce-Price-amount amount">
                            <span className="woocommerce-Price-currencySymbol">$</span>{(+subtotal).nround(3)}
                        </span>
                    </td>
                </tr>
                <tr className="order-total">
                    <th>{t('Total')}</th>
                    <td data-title="Total">
                        <strong>
                            <span className="woocommerce-Price-amount amount">
                                <span className="woocommerce-Price-currencySymbol">$</span>{(+total).nround(3)}</span>
                        </strong>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
}

const Cart = ({ urls }) => {
    const { t } = useTranslation();
    const { total, cartItems, setTotal, coupon, setCartItems, loaded } = useCartItems(urls.cartItems)
    const couponInput = useRef(null);

    const onDeleteCartItem = useCallback(async (item) => {
        INprogress.set()
        const { data } = await Api.delete(encodeURI(`${urls.cartItemDelete}?itemKey=${item.itemKey}`))
        INprogress.unset()
        setCartItems(items => items.filter(i => i !== item))
        setTotal(total => total - item.price)
        window.dispatchEvent(new CustomEvent(cartEvent, {
            detail: {
                itemLength: data.length
            }
        }))
    }, [])

    const applyCoupon = useCallback(async (e) => {
        e.preventDefault()
        if (!couponInput.current) return
        const value = couponInput.current.value.trim()
        console.log(value)
    }, [])

    return <>
        <CartItems items={cartItems} loaded={loaded} onDeleteCartItem={onDeleteCartItem} />
        {(cartItems.length > 0) && <CouponForm couponInput={couponInput} applyCoupon={applyCoupon} />}
        <div className="py-5"></div>
        <div>
            <h2>{t('Cart totals')}</h2>
            <CartTotals total={total} subtotal={total - coupon.amount} coupon={coupon} />
            {(cartItems.length > 0) && (
                <div className="wc-proceed-to-checkout">
                    <a href={(+urls.isAuth) ? encodeURI(urls.checkout) : encodeURI(`${urls.login}?url=${urls.checkout}&nonce=${Date.now()}`)}
                        className="checkout-button button alt wc-forward">{t('Proceed to checkout')}</a>
                </div>
            )}
        </div>
    </>
}


/**
 * 
 * @param { HTMLElement } element 
 */
export const init = (element, locale, urls = {}) => {
    i18n
        .use(initReactI18next)
        .init({
            resources: {
                fr: {
                    translation: {
                        "Cart totals": "Totaux du panier",
                        'Total': 'Total',
                        'Subtotal': 'Subtotal',
                        'Coupon code': 'Code Coupon',
                        'Proceed to checkout': 'Passer à la caisse',
                        'Apply coupon': 'Appliquer Coupon',
                        'Product': 'Produit',
                        'Price': 'Prix',
                        'Type': 'Type',
                        'Your cart is empty': 'Votre panier est vide',
                        'Loading': 'Chargement',
                        'Coupon': 'Coupon'
                    }
                }
            },
            lng: locale,
            interpolation: {
                escapeValue: false
            }
        });
    render(<Cart urls={urls} />, element)
    return () => unmountComponentAtNode(element)
}