import { Controller } from "stimulus"


export default class extends Controller {

    initialize() {
        this.urls = {
            cartItems: this.data.get('cartItems'),
        }
    }

    async connect() {
        const { init } = await import('./react/Chechout.jsx')
        this.react = init(this.element, document.querySelector('html').lang, this.urls)
    }

    disconnect() {
        this.react && this.react()
    }

}
