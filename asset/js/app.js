import 'intersection-observer'
import '@stimulus/polyfills'
import './functions/functions'
import Popper from 'popper.js'
import $ from 'jquery'
import { Application } from "stimulus"
import { definitionsFromContext } from "stimulus/webpack-helpers"
import { SwupApp } from './functions/Swup'


window.Popper = Popper
window.$ = window.jQuery = $
import 'bootstrap'

SwupApp.init()
const application = Application.start()
const context = require.context("./controllers", true, /\.js$/)
application.load(definitionsFromContext(context))
// if ('serviceWorker' in navigator) {
//     window.addEventListener('load', function () {
//         navigator.serviceWorker.register('/sw.js').then(function (registration) {
//             console.log('ServiceWorker registration successful with scope: ', registration.scope);
//         }, function (err) {
//             console.log('ServiceWorker registration failed: ', err);
//         });
//     });
// }