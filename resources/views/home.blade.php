@extends('layouts.app')

@section('title', __('Amazing songs'))

@section('content')
<div data-controller="Home">
    <header>
        @include('layouts.navbar', ['dark' => true])
        <div class="bg-slide--container">
            <div id="carousel-example-1z" class="carousel slide carousel-fade" data-ride="carousel"
                data-interval="false">
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-1z" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-1z" data-slide-to="1"></li>
                    <li data-target="#carousel-example-1z" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active">
                        <div class="view" style="background-image: url({{ asset('/images/artistes/fally1-rv.jpg') }});">
                            <div class="mask rgba-black-light d-flex align-items-center">
                                <div class="container">
                                    <div class="text-white wow fadeIn">
                                        <div class="entry-header typewriter">
                                            <h1 class="entry-title" data-controller="Title">
                                                {{ __('Amazing songs') }}</h1>
                                        </div>
                                        <div class="entry-text">
                                            <p class="mb-4 home-text-desc">
                                                {{ __('The best resource for Congolese and other music') }}.
                                            </p>
                                        </div>
                                        <a href="{{ route('browse.release') }}"
                                            class="btn btn-success wp-block-button__link">{{ __('New Arrival') }}</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item">
                        <div class="view" style="background-image: url({{ asset('/images/artistes/koffi1-rv.jpg') }});">
                            <div class="mask rgba-black-light d-flex align-items-center">
                                <div class="container">
                                    <div class="text-white wow fadeIn">
                                        <div class="entry-header">
                                            <h2 class="entry-title">{{ __('Discover songs you’ll love') }}</h2>
                                        </div>
                                        <div class="entry-text">
                                            <p class="mb-4 home-text-desc">
                                                {{ __('Find the most popular artists and listen their music on BnB') }} ({{ $app_name }})
                                            </p>
                                        </div>
                                        <a href="{{ route('browse-index') }}"
                                            class="btn btn-primary wp-block-button__link">{{ __('Browse') }}</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="view lozad" data-background-image="{{ asset('/images/artistes/lokua2.jpg') }}">
                            <div class="mask rgba-black-light d-flex align-items-center">
                                <div class="container">
                                    <div class="text-white wow fadeIn">
                                        <div class="entry-header">
                                            <h2 class="entry-title">{{ __('For any artist') }}</h2>
                                        </div>
                                        <div class="entry-text">
                                            <p class="mb-4 home-text-desc">
                                                {{ __('A platform for any artist and fan') }}
                                            </p>
                                        </div>
                                        <a href="{{ $artiste_url }}"
                                            class="btn btn-secondary wp-block-button__link">{{ __('Propose') }}</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <main>
        <div class="container my-5">
            <div class="release-container">
                <h5>{{ __('Mundane') }}</h5>
                <div class="row mt-4">
                    @include('layouts.items', ['items' => $musics['mundane']])
                </div>
            </div>
        </div>
        <div class="bg-disp">
            <div class="container">
                <div class="release-container">
                    <h5>{{ __('Religious') }}</h5>
                    <div class="row mt-4">
                        @include('layouts.items', ['items' => $musics['christian']])
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row my-5">
                <div class="col-sm-12 col-md-6">
                    <h5><a href="{{ route('browse.popular') }}">{{ __('Top popular songs') }}</a></h5>
                    <div class="last-top block-loop-index mt-4">
                        @include('layouts.items-sm', ['items' => $musics['popular']])
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <h5><a href="{{ route('news') }}">{{ __('News') }}</a></h5>
                    <div class="row">
                        @include('layouts.items-news', ['items' => $news])
                    </div>
                </div>
            </div>
        </div>
        <div class="bg-parallax">
            <div class="wp-block-cover lozad"
                data-background-image="{{ asset('images/simples/slider-01-1920x1080.jpg') }}">
                <div class="text-center" style="width: 65%;z-index: 1;color: #f8f9f9;">
                    <h1>{{ __('Are you an artist') }} ?</h1>
                    <a href="{{ $artiste_url }}" class="btn btn-secondary wp-block-button__link btn-success mt-5">
                        {{ __('Submit your songs') }}
                    </a>
                </div>
            </div>
        </div>
    </main>
</div>
@endsection