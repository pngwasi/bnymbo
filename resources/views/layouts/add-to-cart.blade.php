@inject('CartTable', 'App\Statics\CartTable')
<span data-controller="Cart" data-Cart-details='@json($cartDetails)' data-Cart-href="{{ route('cart') }}"
    data-Cart-addtocard-url="{{ route('cart.add') }}">
    <a href="{{ array_key_exists(
            "{$cartDetails['id']}-{$cartDetails['section']}",
            (Request::session()->get($CartTable->cartSession) ?: [])) ? route('cart') : '' }}"
        data-target="Cart.addCartAction" rel="tag">

        <span style="display: {{ array_key_exists(
            "{$cartDetails['id']}-{$cartDetails['section']}",
            (Request::session()->get($CartTable->cartSession) ?: [])) ? 'none' : 'inline' }}" data-target="Cart.add">
            {{ __('Add to cart') }}  <b>${{ $price }}</b>
        </span>
        <span data-target="Cart.view"
            style="display: {{ array_key_exists(
            "{$cartDetails['id']}-{$cartDetails['section']}",
            (Request::session()->get($CartTable->cartSession) ?: [])) ? 'inline' : 'none' }}">{{ __('View cart') }}</span>
    </a>
</span>