<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="/favicon.ico">
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <title>@yield('title') -- {{ $app_name }}</title>
    <link rel="icon" href="{{ asset('images/logos/eagle-logo-100x70.png') }}" type="image/png">
    <meta name="description" content="{{ __('A Congolese and other music sales resource') }} !">
    <meta name="keywords" content="BNB">
    <meta name="author" content="{{ $app_name }}" />
    <meta name="robots" content="follow"/>
    <meta name="fragment" content="!">
    <meta name="theme-color" content="#000" />
    @yield('meta-tags')
    <link href="{{ mix('css/style.css') }}" rel="stylesheet">
    @yield('css')
    {{-- <link rel="manifest" href="manifest.json"> --}}
</head>
<body>
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NKDMSK6" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <div id="app-swup">
        @yield('content')
    </div>
    <div id="app-swup-footer">
        @include('layouts.footer')
    </div>
    @yield('js')
    <script src="{{ mix('js/application.js') }}"></script>
</body>
</html>
