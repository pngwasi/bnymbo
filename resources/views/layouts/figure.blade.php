{{-- <figure class="post-thumbnail" style="background-image: url({{ $image !== null ? asset($image) : '' }}); {{ $style ?? '' }}"></figure> --}}

<figure class="post-thumbnail lozad" data-background-image="{{ $image !== null ? asset($image) : '' }}" style="{{ $style ?? '' }}"></figure>
