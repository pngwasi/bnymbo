@foreach ($items as $item)
<div class="{{ $col ?? 'col-4 col-lg-2' }}">
    <div class="followers-user">
        <article class="block-loop-item">
            <a href="{{ $item->follower->artiste ? $item->follower->route() : 'javascript:;' }}">
                @include('layouts.figure', ['image' => $item->follower->image === 'avatar.jpg' ? null :
                $item->follower->_image(), 'style' => 'border-radius:50%;'])
            </a>
            <div class="item-header mt-2">
                <div style="text-align:center">
                    <a href="{{ $item->follower->artiste ? $item->follower->route() : 'javascript:;' }}"
                        class="artiste">
                        <b>{{ $item->follower->_name() }}</b>
                    </a>
                    <p>
                        @if($item->follower->artiste)
                        @auth
                        @if (Auth::user()->id !== $item->follower->id)
                        <button class="btn-follow"
                            data-url-action="{{ route('followUser', ['id' => $item->follower->id]) }}"
                            data-action="follow">
                            <span class="following"
                                style="display: {{ $item->follower->followed ? 'block': 'none' }}">{{ __('Following') }}</span>
                            <span class="follow"
                                style="display: {{ $item->follower->followed ? 'none': 'block' }}">{{ __('Follow') }}</span>
                        </button>
                        @endif
                        @endauth

                        @guest
                        <button class="btn-follow"
                            data-url-action="{{ route('followUser', ['id' => $item->follower->id]) }}"
                            data-action="follow">
                            <span class="follow">{{ __('Follow') }}</span>
                            <span class="following"></span>
                        </button>
                        @endguest
                        @endif
                    </p>
                </div>
            </div>
        </article>
    </div>
</div>
@endforeach