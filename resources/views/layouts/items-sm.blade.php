@foreach ($items as $item)
<article class="block-loop-sm block-loop-item">
    <a href="{{ $item->route() }}">
        @include('layouts.figure', ['image' => $item->_image()])
    </a>
    <div class="item-header">
        <div>
            <a class="artiste" href="{{ $item->route() }}">
                <b>{{ $item->_title() }}</b>
            </a>
        </div>
        <div><a href="{{ $item->user->route() }}" class="artiste">
                {{ $item->user->_name() }}
            </a></div>
    </div>
    <footer class="entry-footer">
        <span class="play"></span>
        <span>
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none"
                stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="svg-icon">
                <circle cx="12" cy="12" r="1"></circle>
                <circle cx="19" cy="12" r="1"></circle>
                <circle cx="5" cy="12" r="1"></circle>
            </svg>
        </span>
    </footer>
</article>
@endforeach