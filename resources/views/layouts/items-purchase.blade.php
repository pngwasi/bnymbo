@foreach ($items as $item)
    @switch($item->orderable->getMorphClass())
        @case('App\Models\Music')
            @include('layouts.items', ['items' => [$item->orderable]])
            @break
        @case('App\Models\Album')
            @include('layouts.items-albums', ['items' => [$item->orderable]])
            @break
        @case('App\Models\Playlist')
            @include('layouts.items-playlists', ['items' => [$item->orderable]])
            @break
        @default
            <span></span>
    @endswitch
@endforeach