<header class="tabs-header">
    <nav id="page-navigation mt-5" class="page-navigation page-header">
        <div class="container">
            <ul class="nav">
                @if ($tabs)
                @foreach ($tabs as $tab)
                <li class="page_item page-item-15 {{ Request::url() == $tab['route'] ? 'current_page_item' : '' }}">
                    <a href="{{ $tab['route'] }}" {{ Request::url() == $tab['route'] ? 'aria-current="page"' : '' }}>{{ ucfirst($tab['name']) }}</a></li>
                @endforeach
                @endif
            </ul>
        </div>
    </nav>
</header>