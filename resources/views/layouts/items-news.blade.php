@foreach ($items as $item)
<div class="{{ $col ?? 'col-6 col-lg-4' }}">
    <article class="block-loop-item">
        <a href="{{ $item->route() }}">
            @include('layouts.figure', ['image' => $item->_image()])
        </a>
        <div class="item-header mt-2">
            <div>
                <a href="{{ $item->route() }}" class="artiste">
                    <b>{{ Str::substr($item->_title(), 0, 22) }} ...</b>
                </a>
            </div>
            <div>
                <a href="{{ $item->route() }}" class="artiste">
                    {{ $item->created_at->toFormattedDateString() }}
                </a>
            </div>
        </div>
    </article>
</div>
@endforeach