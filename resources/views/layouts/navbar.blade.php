<header>
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark primary-nav {{ isset($dark) && $dark ?: 'affix-fix' }}"
        data-controller="NavBar" id="primary-nav">
        <div class="container-fluid">
            <button class="navbar-toggler mr-1" type="button" data-toggle="collapse"
                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation" id="navbar-toggler-b">
                <span class="navbar-toggler-icon"></span>
            </button>
            <a class="navbar-brand " href="{{ route('home') }}">
                <img src="{{ asset('images/logos/eagle-logo-100x70.png') }}" width="30" height="30"
                    class="d-inline-block" alt="">
                <b>{{ __('BNB') }}</b>
            </a>
            <div class="navbar-toggler pad--none">
                <ul class="navbar-nav navbar-nav--outline nav-flex-icons d-inline-block">
                    <x-cart-icon class="d-inline-block" />
                    <li class="nav-item d-inline-block ml-2">
                        <a class="nav-link search-bar--event" style="cursor:pointer" id="search-bar--event1">
                            <svg style="width:24px;height:24px" viewBox="0 0 24 24">
                                <path fill="currentColor"
                                    d="M9.5,3A6.5,6.5 0 0,1 16,9.5C16,11.11 15.41,12.59 14.44,13.73L14.71,14H15.5L20.5,19L19,20.5L14,15.5V14.71L13.73,14.44C12.59,15.41 11.11,16 9.5,16A6.5,6.5 0 0,1 3,9.5A6.5,6.5 0 0,1 9.5,3M9.5,5C7,5 5,7 5,9.5C5,12 7,14 9.5,14C12,14 14,12 14,9.5C14,7 12,5 9.5,5Z" />
                            </svg>
                        </a>
                    </li>
                    @auth
                    <li class="nav-item d-inline-block px-2">
                        <a
                            href="{{ route((Auth::user()->artiste == true ? 'user': 'user.profile'), ['id' => Auth::user()->id, 'name' => Str::slug(Auth::user()->name)]) }}">
                            <div class="avatar avatar-96 photo">
                                <img class="img-cover-full nav-img"
                                    src="{{ asset(Auth::user()->image === 'avatar.jpg' ? 'images/avatars/avatar.jpg' : Auth::user()->_image()) }}">
                            </div>
                        </a>
                    </li>
                    @endauth
                    @guest
                    <li class="nav-item d-inline-block">
                        <a class="nav-link signin" href="{{ route('login') }}">{{ __('Sign In') }}</a>
                    </li>
                    @endguest
                </ul>

            </div>
            <div class="collapse navbar-collapse ml-lg-3" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item mr-lg-2">
                        <a class="nav-link" href="{{ route('home') }}">{{ __('Discover') }}</a>
                    </li>
                    <li class="nav-item mr-lg-2">
                        <a class="nav-link" href="{{ route('browse-index') }}">{{ __('Browse') }}</a>
                    </li>
                    <li class="nav-item mr-lg-2">
                        <a class="nav-link" href="{{ route('artistes') }}">{{ __('Artistes') }}</a>
                    </li>
                    <li class="nav-item mr-lg-2">
                        <a class="nav-link" href="{{ route('news') }}">{{ __('News') }}</a>
                    </li>
                </ul>
            </div>
            <ul class="navbar-nav navbar-nav--outline nav-flex-icons d-none d-lg-flex">
                <x-cart-icon class />
                <li class="nav-item">
                    <a class="nav-link search-bar--event" style="cursor:pointer" id="search-bar--event2">
                        @include('templates.svg.search-svg')
                    </a>
                </li>
                @guest
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}">{{ __('Sign Up') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link signin" href="{{ route('login') }}">{{ __('Sign In') }}</a>
                </li>
                @endguest
                @auth
                <li class="nav-item pt-1">
                    <a
                        href="{{ route((Auth::user()->artiste == true ? 'user': 'user.profile'), ['id' => Auth::user()->id, 'name' => Str::slug(Auth::user()->name)]) }}">
                        <span class="user-display-name">{{ Auth::user()->_name() }}</span>
                        <div class="avatar avatar-96 photo">
                            <img class="img-cover-full nav-img"
                                src="{{ asset(Auth::user()->image === 'avatar.jpg' ? 'images/avatars/avatar.jpg' : Auth::user()->_image()) }}">
                        </div>
                    </a>
                </li>
                @endauth
            </ul>
        </div>
    </nav>

    <section id="toggle--search-bar" class="search_area navbar_fixed">
        <form data-swup-form action="{{ route('search') }}" method="get">
            <div class="search_inner">
                <input type="text" name="s" id="search-bar--input" placeholder="{{ __('Search') }}...">
                <button type="button" class="close ti-close" aria-label="Close" id="toggle--search-bar--input">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </form>
    </section>
</header>