@auth
@foreach ($items as $item)
<div class="{{ $col ?? 'col-4 col-lg-2' }}">
    <div class="followers-user">
        <article class="block-loop-item">
            <a href="{{ $item->user->artiste ? $item->user->route() : 'javascript:;' }}">
                @include('layouts.figure', ['image' => $item->user->image === 'avatar.jpg' ? null :
                $item->user->_image(), 'style' => 'border-radius:50%;'])
            </a>
            <div class="item-header mt-2">
                <div style="text-align:center">
                    <a href="{{ $item->user->artiste ? $item->user->route() : 'javascript:;' }}" class="artiste">
                        <b>{{ $item->user->_name() }}</b>
                    </a>
                    <p>
                        @if (Auth::user()->id !== $item->user->id)
                        <button class="btn-follow" data-id="1"
                            data-url-action="{{ route('followUser', ['id' => $item->user->id]) }}" data-action="follow">
                            <span class="following" style="display: block">{{ __('Following') }}</span>
                            <span class="follow" style="display: none">{{ __('Follow') }}</span>
                        </button>
                        @endif
                    </p>
                </div>
            </div>
        </article>
    </div>
</div>
@endforeach
@endauth