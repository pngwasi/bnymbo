<footer class="site-footer" data-controller="Footer" data-Footer-locale="{{ route('locale') }}">
    <div class="container">
        <div class="row py-5">
            <div class="col-12 col-md-3 py-3">
                <h3>{{ $app_name }}</h3>
                <p class="copywrite-text">
                    {{ __('Copyright © ').now()->year }} {{ __('All rights reserved') }} <br>
                    <span>{{ __('Made by') }} <a href="javascript:;" target="_blank">PNG_</a></span>
                </p>
                <p class="mt-2">
                    <select data-target="Footer.Locale" style="font-size: 70%">
                        @foreach ($langs as $lang)
                        <option value="{{ $lang['key'] }}">{{ $lang['name'] }}</option>
                        @endforeach
                    </select>
                </p>
            </div>
            <div class="col-12 col-md-3 py-3">
                <h3>{{ __('About') }}</h3>
                <ul>
                    <li><a href="#">{{ __('About us') }}</a></li>
                    <li><a href="#">{{ __('Our Services') }}</a></li>
                    <li><a href="#">{{ __('Contact Us') }}</a></li>
                </ul>
            </div>
            <div class="col-12 col-md-3 py-3">
                <h3>{{ __('Social') }}</h3>
                <ul>
                    <li><a href="#">Facebook</a></li>
                    <li><a href="#">Instagram</a></li>
                    <li><a href="#">Twitter</a></li>
                </ul>
            </div>
            <div class="col-12 col-md-3 py-3">
                <h3>{{ __('Subscribe') }}</h3>
                <form action="#" onsubmit="return false" method="post" class="subscribe-form">
                    <input type="email" name="subscribe-email" placeholder="E-mail" id="subscribeemail">
                    <button type="submit" class="button btn-sm mt-1">{{ __('Subscribe') }}</button>
                </form>
            </div>
        </div>
    </div>
</footer>