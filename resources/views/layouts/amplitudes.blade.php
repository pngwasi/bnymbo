<div class="example-container">
    <div class="left">
        <div id="white-player">
            <div class="white-player-top">
                <div>&nbsp;</div>
                <div class="center">
                    <span class="now-playing">{{ __('Now Playing') }}</span>
                </div>
                <div>
                    <img src="{{ asset('/images/mix/svg/amplitudes/show-playlist.svg') }}" class="show-playlist" />
                </div>
            </div>
            <div id="white-player-center">
                <div class="main-album-art-cover">
                    <img data-amplitude-song-info="cover_art_url" class="main-album-art" />
                </div>
                <div class="song-meta-data">
                    <span data-amplitude-song-info="name" class="song-name"></span>
                    <span data-amplitude-song-info="artist" class="song-artist"></span>
                </div>
                <div class="time-progress">
                    <div id="progress-container">
                        <input type="range" class="amplitude-song-slider" />
                        <progress id="song-played-progress" class="amplitude-song-played-progress"></progress>
                        <progress id="song-buffered-progress" class="amplitude-buffered-progress" value="0"></progress>
                    </div>
                    <div class="time-container">
                        <span class="current-time">
                            <span class="amplitude-current-minutes"></span>:<span
                                class="amplitude-current-seconds"></span>
                        </span>
                        <span class="duration">
                            <span class="amplitude-duration-minutes"></span>:<span
                                class="amplitude-duration-seconds"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div id="white-player-controls">
                <div class="amplitude-shuffle amplitude-shuffle-off" id="shuffle"></div>
                <div class="amplitude-prev" id="previous"></div>
                <div class="amplitude-play-pause" id="play-pause"></div>
                <div class="amplitude-next" id="next"></div>
                <div class="amplitude-repeat" id="repeat"></div>
            </div>
            <div id="white-player-playlist-container">
                <div class="white-player-playlist-top">
                    <div></div>
                    <div><span class="queue">{{ __('Queue') }}</span></div>
                    <div><img src="{{ asset('/images/mix/svg/amplitudes/close.svg') }}" class="close-playlist" /></div>
                </div>
                <div class="white-player-up-next">{{ __('Up Next') }}</div>
                <div class="white-player-playlist"></div>
                <div class="white-player-playlist-controls">
                    <div class="playlist-album-art">
                        <img data-amplitude-song-info="cover_art_url" />
                    </div>
                    <div class="playlist-controls">
                        <div class="playlist-meta-data">
                            <span data-amplitude-song-info="name" class="song-name"></span>
                            <span data-amplitude-song-info="artist" class="song-artist"></span>
                        </div>
                        <div class="playlist-control-wrapper">
                            <div class="amplitude-prev" id="playlist-previous"></div>
                            <div class="amplitude-play-pause" id="playlist-play-pause"></div>
                            <div class="amplitude-next" id="playlist-next"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a id="share-item" class="more-on-ssu clickable">-- {{ __('Share Song') }} --</a>
        <script data-swup-reload-script>
            document.getElementById('share-item')
            .addEventListener('click', async function() {
                if ('share' in window.navigator) {
                    window.navigator.share({
                        title: "{{ $title ?? '' }}",
                        url: "{{ $url ?? '' }}",
                        text: "{{ $app_name }}"
                    })
                }
            })
        </script>
    </div>
</div>