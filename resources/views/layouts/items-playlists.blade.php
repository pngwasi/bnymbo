@foreach ($items as $item)
<div class="{{ $col ?? 'col-4 col-lg-2' }}">
    <article class="block-loop-item">
        <div>
            <a href="{{ $item->route() }}">
                @include('templates.svg.playlist-svg')
            </a>
        </div>
        <div class="item-header mt-2">
            <div>
                <a href="{{ $item->route() }}" class="artiste">
                    <b>{{ $item->_name() }} ({{ $item->items->count() }})</b>
                </a>
            </div>
            <div>
                <a href="{{ $item->user->route() }}" class="artiste">
                    {{ $item->user->_name() }}
                </a>
            </div>
        </div>
    </article>
</div>
@endforeach