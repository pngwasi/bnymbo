@foreach ($items as $item)
<div class="{{ $col ?? 'col-4 col-lg-2' }}">
    <div class="followers-user">
        <article class="block-loop-item">
            <a href="{{ $item->route() }}">
                @include('layouts.figure', ['image' => $item->image === 'avatar.jpg' ? null :
                $item->_image(), 'style' => 'border-radius:50%;'])
            </a>
            <div class="item-header mt-2">
                <div style="text-align:center">
                    <a href="{{ $item->route()}}" class="artiste">
                        <b>{{ $item->_name() }}</b>
                    </a>
                    <p>
                        @if (!isset($noFollow))
                        @auth
                        @if (Auth::user()->id !== $item->id)
                        <button class="btn-follow" data-url-action="{{ route('followUser', ['id' => $item->id]) }}"
                            data-action="follow">
                            <span class="following"
                                style="display: {{ $item->followed ? 'block': 'none' }}">{{ __('Following') }}</span>
                            <span class="follow"
                                style="display: {{ $item->followed ? 'none': 'block' }}">{{ __('Follow') }}</span>
                        </button>
                        @endif
                        @endauth

                        @guest
                        <button class="btn-follow" data-url-action="{{ route('followUser', ['id' => $item->id]) }}"
                            data-action="follow">
                            <span class="follow">{{ __('Follow') }}</span>
                            <span class="following"></span>
                        </button>
                        @endguest
                        @endif
                    </p>
                </div>
            </div>
        </article>
    </div>
</div>
@endforeach