<div class="dropdown">
    <a class="dropdown-toggle" type="button" id="dropdownCaterogiesList" data-toggle="dropdown" aria-haspopup="true"
        aria-expanded="false">
        @foreach ($categories as $category)
            @if ($category['type'] == $category_type)
                <b>{{ $category['name'] }}</b>
            @endif
        @endforeach
    </a>
    <div class="dropdown-menu" id="caterogies--list" aria-labelledby="dropdownCaterogiesList">
        @foreach ($categories as $category)
            <a style="cursor: pointer" class="dropdown-item" data-type="{{ $category['type'] }}">{{$category['name']}}</a>
        @endforeach
    </div>
</div>