<div class="sidebar" data-color="purple" data-background-color="black">
    <div class="logo text-center">
        <a href="javascript:void(0)" class="simple-text logo-normal">
            {{ $app_name }}
        </a>
    </div>
    <div class="sidebar-wrapper" >
        <ul class="nav" id="sidebar-wrapper">
            <li class="nav-item {{ Str::contains(Request::fullUrl(), route('admin.home')) ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('admin.home') }}">
                    <i class="material-icons">dashboard</i> 
                    <p>{{ __('Dashboard') }}</p>
                </a>
            </li>
            <li class="nav-item {{ Str::contains(Request::fullUrl(), route('admin.artist')) ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('admin.artist') }}">
                    <i class="material-icons">art_track</i>
                    <p>{{ __('Artists') }}</p>
                </a>
            </li>
            <li class="nav-item {{ Str::contains(Request::fullUrl(), route('admin.music_album')) ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('admin.music_album') }}">
                    <i class="material-icons">album</i>
                    <p>{{ __('Musics') }}</p>
                </a>
            </li>
            <li class="nav-item {{ Str::contains(Request::fullUrl(), route('admin.client')) ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('admin.client') }}">
                    <i class="material-icons">supervised_user_circle</i>
                    <p>{{ __('Clients') }}</p>
                </a>
            </li>
            <li class="nav-item {{ Str::contains(Request::fullUrl(), route('admin.purchase')) ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('admin.purchase') }}">
                    <i class="material-icons">shopping_basket</i>
                    <p>{{ __('Purchases') }}</p>
                </a>
            </li>
            <li class="nav-item {{ Str::contains(Request::fullUrl(), route('admin.news')) ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('admin.news') }}">
                    <i class="material-icons">dvr</i>
                    <p>{{ __('News') }}</p>
                </a>
            </li>
        </ul>
    </div>
</div>