<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="turbolinks-cache-control" content="no-cache">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />

    <title>@yield('title') - {{ $app_name }}</title>
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('dashboard/assets/img/apple-icon.png') }}">
    <link rel="icon" type="image/png" href="{{ asset('dashboard/assets/img/favicon.png') }}">
    <link href="{{ asset('dashboard/assets/css/material-dashboard.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('dashboard/assets/demo/demo.css') }}" rel="stylesheet" />
    <link href="{{ mix('css/dashboard.css') }}" rel="stylesheet">
    @yield('css')
</head>

<body class="sidebar-mini">
    @yield('app-content')
    <div class="loading-ntf hidden">
        <div class='uil-ring-css' style='transform:scale(0.79);'>
            <spinning-dots style="
                    width:100px;
                    stroke-width:20px;
                    color: #68c3ff;" />
        </div>
    </div>
    <script src="{{ asset("dashboard/assets/js/core/jquery.min.js") }}"></script>
    <script src="{{ asset("dashboard/assets/js/core/popper.min.js") }}"></script>
    <script src="{{ asset("dashboard/assets/js/core/bootstrap-material-design.min.js") }}"></script>
    <script src="{{ asset("dashboard/assets/js/plugins/moment.min.js") }}"></script>
    <script src="{{ asset("dashboard/assets/js/plugins/chartist.min.js") }}"></script>
    <script src="{{ asset("dashboard/assets/js/plugins/bootstrap-notify.js") }}"></script>
    <script src="{{ asset("dashboard/assets/js/plugins/bootstrap-datetimepicker.min.js") }}"></script>
    <script src="{{ asset("dashboard/assets/js/material-dashboard.min.js") }}"></script>
    <script src="{{ asset("dashboard/assets/demo/demo.js") }}"></script>
    <script src="{{ mix('js/dashboard.js') }}"></script>
    @yield('js')
</body>

</html>