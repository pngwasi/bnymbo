<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
    <div class="container-fluid">
        <div class="navbar-wrapper">
            <a class="navbar-brand" href="javascript:;" id="nav-title">
                <span class="bold">@yield('nav-title')</span>
            </a>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
        <div class="collapse navbar-collapse justify-content-end" data-controller="Footer"
            data-Footer-locale="{{ route('locale') }}">
            <form class="navbar-form d-none"></form>
            <select data-target="Footer.Locale" class="px-3" style="font-size: 70%">
                @foreach ($langs as $lang)
                <option value="{{ $lang['key'] }}">{{ $lang['name'] }}</option>
                @endforeach
            </select>
            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a class="nav-link" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                        <i class="material-icons">notifications</i>
                        <span class="notification">0</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="javascript:;" id="navbarDropdownProfile" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        {{ Auth::user()->email }}
                        <i class="material-icons">person</i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                        <a class="dropdown-item" href="#" data-toggle="modal"
                            data-target="#adminModify">{{ __('Profile') }}</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#" onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                            {{ __('Log out') }}
                        </a>
                        <form id="logout-form" action="{{ route('admin.logout') }}" method="POST"
                            style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="modal fade" id="adminModify" data-controller="Admin-Profile" tabindex="-1" role="dialog"
    aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myLargeModalLabel">{{ Auth::user()->email }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form data-action="Admin-Profile#adminModify" enctype="multipart/form-data" method="POST"
                action="{{ route('admin.profile', [], false) }}" autocomplete="off">
                <div class="modal-body">
                    <div class="row mt-3 justify-content-center">
                        <div class="col-md-9 col-12">
                            <div class="card-body">
                                <div class="form-group">
                                    <label class="pure-material-textfield-outlined w-100">
                                        <input type="email" disabled value="{{ Auth::user()->email }}">
                                        <span>{{ __('E-Mail Address') }}</span>
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label class="pure-material-textfield-outlined w-100">
                                        <input type="password" name="current_password">
                                        <span>{{ __('Current Password') }}</span>
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label class="pure-material-textfield-outlined w-100">
                                        <input type="password" name="password">
                                        <span>{{ __('New Password') }}</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-info">{{ __('Save') }}
                        <div class="ripple-container"></div>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>