<form action="" method="get" data-swup-form>
    <div class="row">
        <div class="col-md-5 col-sm-12">
            <div class="input-group no-border">
                <input type="text" name="q" class="form-control" value="{{ request('q') }}"
                    placeholder="{{ __('Search Name') }}...">
                <button type="submit" class="btn btn-white btn-round btn-just-icon">
                    <i class="material-icons">search</i>
                </button>
            </div>
        </div>
        @isset($filter)
        <div class="col-md-5 col-sm-12">
            <div class="form-group">
                <select class="form-control" data-style="btn btn-link" name="f">
                    <option value="">{{ __('All') }}</option>
                    <option value="actif" {{ request('f') === 'actif' ? 'selected': '' }}>{{ __('Actif') }}</option>
                    <option value="inactif" {{ request('f') === 'inactif' ? 'selected': '' }}>{{ __('Inactif') }}
                    </option>
                </select>
            </div>
        </div>
        <button type="submit" class="btn btn-primary btn-sm">{{ __('Enter') }}</button>
        @endisset
    </div>
</form>