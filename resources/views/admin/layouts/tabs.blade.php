<div class=" card-nav-tabs card-plain tabs--dash">
    <div class=" pl-0 ml-0">
        <div class="nav-tabs-navigation">
            <div class="nav-tabs-wrapper">
                <ul class="nav nav-tabs" data-tabs="tabs">
                    @if ($tabs)
                    @foreach ($tabs as $tab)
                    <li class="nav-item {{ Request::url() == $tab['route'] ? 'active' : '' }}">
                        <a class="nav-link {{ Request::url() == $tab['route'] ? 'active' : '' }}"
                            href="{{ $tab['route'] }}">
                            @isset($tab['icon'])
                            <i class="material-icons">{{ $tab['icon'] }}</i>
                            @endisset
                            {{ Str::ucfirst($tab['name']) }}
                        </a>
                    </li>
                    @endforeach
                    @endif
                </ul>
            </div>
        </div>
        <hr>
    </div>
</div>