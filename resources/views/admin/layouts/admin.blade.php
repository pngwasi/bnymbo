@extends('admin.layouts.app-admin')

@section('app-content')
<div class="wrapper">
    @include('admin.layouts.sidebar')
    <div class="main-panel">
        @include('admin.layouts.navbar')
        <div class="content">
            <div id="app-dashboard">
                @yield('content')
            </div>
        </div>
        @include('admin.layouts.footer')
    </div>
</div>
@endsection