@extends('admin.layouts.admin')
@section('nav-title', __('News'))

@section('content')
<div class="container-fluid" data-controller="News" aria-live="polite">
    @include('admin.layouts.tabs', [
    'tabs' => [
    [ 'name' => __('New article'), 'route' => route('admin.news')],
    [ 'name' => __('Articles'), 'route' => route('admin.news.articles')],
    [ 'name' => __('Categories'), 'route' => route('admin.news.categories')]
    ]
    ])
    <div class="card">
        <div class="card-body news">
            @yield('news-content')
        </div>
    </div>
</div>
@endsection