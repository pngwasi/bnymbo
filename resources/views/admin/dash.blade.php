@extends('admin.layouts.admin')

@section('nav-title', __('Dashboard'))

@section('content')
<div class="container-fluid" data-controller="Dash">
    <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header card-header-warning card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">art_track</i>
                    </div>
                    <p class="card-category">{{ __('Artistes') }}</p>
                    <h3 class="card-title">{{ $artistes }}</h3>
                </div>
                <div class="card-footer"></div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">supervised_user_circle</i>
                    </div>
                    <p class="card-category">{{ __('Clients') }}</p>
                    <h3 class="card-title">{{ $clients }}</h3>
                </div>
                <div class="card-footer"></div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header card-header-danger card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">album</i>
                    </div>
                    <p class="card-category">{{ __('Musics') }}</p>
                    <h3 class="card-title">{{ $musics }}</h3>
                </div>
                <div class="card-footer"></div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header card-header-info card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">
                            public
                        </i>
                    </div>
                    <p class="card-category">{{ __('Downloads') }}</p>
                    <h3 class="card-title">{{ $downloads }}</h3>
                </div>
                <div class="card-footer"></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="card card-chart">
                <div class="card-header card-header-success">
                    <div class="ct-chart py-5" id="dailySalesChart"></div>
                </div>
                <div class="card-body">
                    <h4 class="card-title">{{ __('24h') }}</h4>
                    <p class="card-category"></p>
                </div>
                <div class="card-footer"></div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card card-chart">
                <div class="card-header card-header-warning">
                    <div class="ct-chart py-5" id="websiteViewsChart"></div>
                </div>
                <div class="card-body">
                    <h4 class="card-title">{{ __('Week') }}</h4>
                    <p class="card-category"></p>
                </div>
                <div class="card-footer"></div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card card-chart">
                <div class="card-header card-header-danger">
                    <div class="ct-chart py-5" id="completedTasksChart"></div>
                </div>
                <div class="card-body">
                    <h4 class="card-title">{{ __('Year') }}</h4>
                    <p class="card-category"></p>
                </div>
                <div class="card-footer"></div>
            </div>
        </div>
    </div>
</div>
@endsection