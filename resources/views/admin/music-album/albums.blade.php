@extends('admin.music_album')

@section('music-album')
@include('admin.layouts.search', ['filter' => true])
{{ $albums->links() }}
<table class="table">
    <thead>
        <tr>
            <th class="text-center">#</th>
            <th>{{ __('Image') }}</th>
            <th>{{ __('Title') }}</th>
            <th>{{ __('Author') }}</th>
            <th>{{ __('Price') }}</th>
            <th>{{ __('Status') }}</th>
            <th>{{ __('Recorded on') }}</th>
            <th>{{ __('Musics') }}</th>
            <th>{{ __('Actions') }}</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($albums as $album)
        <tr>
            <td class="text-center">{{ $album->id }}</td>
            <td>
                <div class="img-container">
                    <img class="img-cover-full" src="{{ asset($album->_image()) }}" alt="{{ $album->_name() }}"
                        rel="nofollow">
                </div>
            </td>
            <td><a href="{{ $album->adminRoute() }}">{{ $album->_name() }}</a></td>
            <td><a href="{{ $album->user->adminRoute() }}">{{ $album->user->_name() }}</a></td>
            <td>${{ $album->_price() }}</td>
            @if ($album->active)
            <th class="text-success">{{ __('Actif') }}</th>
            @else
            <th class="text-danger">{{ __('Inactif') }}</th>
            @endif
            <td>{{ $album->created_at }}</td>
            <td>{{ $album->musics->count() }}</td>
            <td class="td-actions">
                <a href="{{ $album->adminRoute() }}" class="btn btn-info">
                    <i class="material-icons">link</i>
                </a>
                <button class="btn {{ $album->active ? 'btn-danger': 'btn-primary' }}" onclick="event.preventDefault();
                    document.getElementById('delete-album-form-{{ $album->id }}').submit();">
                    <i class="material-icons">{{ $album->active ? 'close': 'done' }}</i>
                </button>
                <form id="delete-album-form-{{ $album->id }}"
                    action="{{ route('admin.musics.album.modify', ['id' => $album->id, 'redirect' => request()->fullUrl()]) }}"
                    method="POST" style="display: none;">
                    @method('PATCH')
                    @csrf
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<div class="text-center">
    @empty($albums->items())
    <span class="text-warning">{{ __('No Items found') }}</span>
    @endempty
</div>
@endsection