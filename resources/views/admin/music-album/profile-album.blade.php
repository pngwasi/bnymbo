@extends('admin.layouts.admin')
@section('nav-title', __('Album').' | '.$album->name)
@section('content')
<div class="container-fluid" data-controller="album--Profile">
    <button type="button" class="btn btn-secondary btn-sm" onclick="history.back()">
        <i class="material-icons">chevron_left</i>
    </button>
    <div class="row">
        <div class="col-md-10 ml-auto mr-auto">
            <div class="card profil-card">
                <div class="raduis">
                    <div class="card-header card-header-danger card-icon-img shadow-dark-c"
                        style="background-image: url({{ asset($album->_image()) }});">
                        <span class="mask bg-gradient-default opacity-8"></span>
                        <div class="container-fluid d-flex align-items-center mt-5">
                            <div class="row">
                                <div class="col-12">
                                    <h5 class="display-4 text-white">{{ $album->_name() }}</h5>
                                </div>
                                <div class="col-lg-7 col-md-10">
                                    <p class="text-white mt-0 mb-3">{{ $album->description ?? '...' }}</p>
                                </div>
                                <div class="col-12 mb-1">
                                    <div class="user-links">
                                        <span>
                                            <a href="javascript:;">
                                                <i class="material-icons">favorite_outline</i>
                                                {{ $album->likes->count() }}
                                            </a>
                                        </span>
                                        <span>
                                            <a href="javascript:;">
                                                <i class="material-icons">cloud_download</i>
                                                {{ $album->downloads->count() }}
                                            </a>
                                        </span>
                                        <span>
                                            <a href="javascript:;">
                                                <i class="material-icons">shopping_basket</i>
                                                ${{ $album->orders->sum('total') }}
                                            </a>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <h5>{{ __('Artiste: ') }} 
                                        <a href="{{ $album->user->adminRoute() }}">
                                            {{ $album->user->_name() }}
                                        </a>
                                        {{ __('Price: ') }} ${{ $album->_price() }}
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body ">
                    <div class="tabs--dash">
                        @include('admin.layouts.tabs', [
                        'tabs' => [
                        [ 'name' => __('Music'). " (" .$album->musics->count(). ")", 'route' =>
                        route('admin.musics.album.profile.home', ['id' =>
                        $album->id]), 'icon' => 'library_music'],
                        [ 'name' => __('Report'), 'route' => route('admin.musics.album.profile.report', ['id' =>
                        $album->id]), 'icon' => 'report'],
                        [ 'name' => __('Settings'), 'route' => route('admin.musics.album.profile.settings', ['id' =>
                        $album->id]), 'icon' => 'settings'],
                        ]
                        ])
                    </div>
                    <div class="tab-content">
                        @yield('album-profile')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection