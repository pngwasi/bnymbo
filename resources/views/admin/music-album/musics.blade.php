@extends('admin.music_album')

@section('music-album')
@include('admin.layouts.search', ['filter' => true])
{{ $musics->links() }}
<table class="table">
    <thead>
        <tr>
            <th class="text-center">#</th>
            <th>{{ __('Image') }}</th>
            <th>{{ __('Title') }}</th>
            <th>{{ __('Author') }}</th>
            <th>{{ __('Price') }}</th>
            <th>{{ __('Status') }}</th>
            <th>{{ __('Recorded on') }}</th>
            <th>{{ __('Album') }}</th>
            <th>{{ __('Actions') }}</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($musics as $music)
        <tr>
            <td class="text-center">{{ $music->id }}</td>
            <td>
                <div class="img-container">
                    <img class="img-cover-full" src="{{ asset($music->_image()) }}" alt="{{ $music->_title() }}"
                        rel="nofollow">
                </div>
            </td>
            <td>
                <a href="{{ $music->adminRoute() }}">
                    {{ $music->_title() }}
                </a>
            </td>
            <td>
                <a href="{{ $music->user->adminRoute() }}">
                    {{ $music->user->_name() }}
                </a>
            </td>
            <td>${{ $music->_price() }}</td>
            @if ($music->active)
            <th class="text-success">{{ __('Actif') }}</th>
            @else
            <th class="text-danger">{{ __('Inactif') }}</th>
            @endif
            <td>{{ $music->created_at }}</td>
            @if ($music->album)
            <td>
                <a href="{{ $music->album->adminRoute() }}">{{ $music->album->_name() }}</a>
            </td>
            @else
            <td>--</td>
            @endif

            <td class="td-actions">
                <a href="{{ $music->adminRoute() }}" class="btn btn-info">
                    <i class="material-icons">link</i>
                </a>
                <button class="btn {{ $music->active ? 'btn-danger': 'btn-primary' }}" onclick="event.preventDefault();
                    document.getElementById('delete-music-form-{{ $music->id }}').submit();">
                    <i class="material-icons">{{ $music->active ? 'close': 'done' }}</i>
                </button>
                <form id="delete-music-form-{{ $music->id }}"
                    action="{{ route('admin.musics.music.modify', ['id' => $music->id, 'redirect' => request()->fullUrl()]) }}"
                    method="POST" style="display: none;">
                    @method('PATCH')
                    @csrf
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<div class="text-center">
    @empty($musics->items())
    <span class="text-warning">{{ __('No Items found') }}</span>
    @endempty
</div>
@endsection