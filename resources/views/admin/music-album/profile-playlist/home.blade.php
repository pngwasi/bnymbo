@extends('admin.music-album.profile-playlist')

@section('playlist-profile')
<div data-controller="playlist--Profile-Music"
    data-playlist--Profile-Music-musics="{{ route('admin.music-album.playlist.musics', ['id' => $playlist->id], false) }}"
    data-playlist--Profile-Music-artist-musics="{{ route('admin.artist.get-artist-musics-list', ['id' => $playlist->user->id], false) }}"
    data-playlist--Profile-Music-add-to-playlist="{{ route('admin.music-album.playlist.add-to-playlist', ['id' => $playlist->id], false) }}">
    <button class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#addMusics">
        <i class="material-icons">add_circle</i>{{__('Add Musics')}}
    </button>

    <div class="mt-2 row" data-target="playlist--Profile-Music.musicsList"
        style="overflow-y: scroll; max-height: 425px;"></div>

    <div class="modal fade" id="addMusics" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">{{__('Add Musics') }}
                        <small>({{ $playlist->_name() }})</small></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form data-action="playlist--Profile-Music#searchMusics"
                        action="{{ route('admin.artist.search-artist-music', ['id' => $playlist->user->id], false) }}"
                        method="post">
                        <div class="input-group no-border">
                            <input type="text" name="title" class="form-control"
                                placeholder="{{ __('Search Musics') }}...">
                            <button type="submit" class="btn btn-white btn-round btn-just-icon">
                                <i class="material-icons">search</i>
                            </button>
                        </div>
                    </form>
                    <div class="row" style="overflow-y: scroll; max-height: 425px;" data-target="playlist--Profile-Music.musicsListModal"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection