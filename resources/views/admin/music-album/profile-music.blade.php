@extends('admin.layouts.admin')
@section('nav-title', __('Music').' | '.$music->_title())
@section('content')
<div class="container-fluid" data-controller="music--Profile" data-music--Profile-audio="{{ $music->adminPlayer() }}">
    <button type="button" class="btn btn-secondary btn-sm" onclick="history.back()">
        <i class="material-icons">chevron_left</i>
    </button>
    <div class="row">
        <div class="col-md-10 ml-auto mr-auto">
            <div class="card profil-card">
                <div class="raduis">
                    <div class="card-header card-header-danger card-icon-img shadow-dark-c"
                        style="background-image: url({{ asset($music->_image()) }});">
                        <span class="mask bg-gradient-default opacity-8"></span>
                        <div class="container-fluid d-flex align-items-center mt-5">
                            <div class="row">
                                <div class="col-12">
                                    <h5 class="display-4 text-white">{{ $music->_title() }}</h5>
                                </div>
                                <div class="col-12 my-3" data-target="music--Profile.audio">
                                    <button class="btn btn-outline-light" data-play="{{ $music->adminPlayer() }}"
                                        type="button" title="Play">
                                        <div style="display:none">
                                            <audio preload="none" controls playsinline controlsList="nodownload">
                                                <source src="{{ $music->adminPlayer() }}" type="audio/mpeg">
                                            </audio>
                                        </div>
                                        <i class="material-icons" style="font-size: 19px;">play_circle_outline</i>
                                    </button>
                                </div>
                                <div class="col-12 mb-1">
                                    <div class="user-links">
                                        <span>
                                            <a href="javascript:;">
                                                <i class="material-icons">favorite_outline</i>
                                                {{ $music->likes->count() }}
                                            </a>
                                        </span>
                                        <span>
                                            <a href="javascript:;">
                                                <i class="material-icons">cloud_download</i>
                                                {{ $music->downloads->count() }}
                                            </a>
                                        </span>
                                        <span>
                                            <a href="javascript:;">
                                                <i class="material-icons">shopping_basket</i>
                                                {{ $music->orders->sum('total') }}
                                            </a>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <h5>{{ __('Artiste: ') }} 
                                        <a href="{{ $music->user->adminRoute() }}">
                                            {{ $music->user->_name() }}
                                        </a>
                                        {{ __('Price: ') }} ${{ $music->_price() }}
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body ">
                    <div class="tabs--dash">
                        @include('admin.layouts.tabs', [
                        'tabs' => [
                        [ 'name' => __('Report'), 'route' => route('admin.musics.music.profile.home', ['id' =>
                        $music->id]), 'icon' => 'report'],
                        [ 'name' => __('Settings'), 'route' => route('admin.musics.music.profile.settings', ['id' =>
                        $music->id]), 'icon' => 'settings'],
                        ]
                        ])
                    </div>
                    <div class="tab-content">
                        @yield('music-profile')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection