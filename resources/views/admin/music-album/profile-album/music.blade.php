@extends('admin.music-album.profile-album')

@section('album-profile')
<div data-controller="album--Profile-Music">
    <button class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#addMusics">
        <i class="material-icons">add_circle</i>
        {{__('Add Musics')}}
    </button>
    <div class="mt-2 row" data-target="album--Profile-Music.musicsList"
        data-url-get-musics="{{ route('admin.music-album.album.get-musics', ['id' => $album->id], false) }}"
        style="overflow-y: scroll; max-height: 425px;"></div>

    <div class="modal fade" id="addMusics" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">{{__('Add Musics') }}
                        <small>({{ $album->name }})</small></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row mb-3">
                        <div class="col-md-10 col-12">
                            <div class="row">
                                <div class="col-lg-6 col-12">
                                    <select data-target="album--Profile-Music.slimSelectGenres">
                                        <option data-placeholder="true"></option>
                                        @foreach ($genres as $genre)
                                        <option value="{{ $genre }}">{{ $genre }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <select data-target="album--Profile-Music.slimSelectCategory">
                                        <option data-placeholder="true"></option>
                                        @foreach ($categorys as $category)
                                        <option value="{{ $category['type'] }}">{{ $category['name'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-target="album--Profile-Music.uppyInit"
                        data-url-new-musics="{{ route('admin.music-album.album.new-musics', ['id' => $album->id], false) }}"
                        class="uppy-initial"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection