@extends('admin.layouts.admin')
@section('nav-title', __('Playlist').' | '.$playlist->name)
@section('content')
<div class="container-fluid" data-controller="playlist--Profile">
    <button type="button" class="btn btn-secondary btn-sm" onclick="history.back()">
        <i class="material-icons">chevron_left</i>
    </button>
    <div class="row">
        <div class="col-md-10 ml-auto mr-auto">
            <div class="card profil-card">
                <div class="raduis">
                    <div class="card-header bg-gradient-default card-header-danger card-icon-img ">
                        <div class="container-fluid d-flex align-items-center mt-5">
                            <div class="row">
                                <div class="col-12">
                                    <h5 class="display-4 text-white">
                                        <div style="height: 40px;width:40px;" class="d-inline-block">
                                            @include('templates.svg.playlist-svg')
                                        </div>
                                        {{ $playlist->_name() }}
                                    </h5>
                                </div>
                                <div class="col-12 mb-1">
                                    <div class="user-links">
                                        <span>
                                            <a href="javascript:;">
                                                <i class="material-icons">favorite_outline</i>
                                                {{ $playlist->likes->count() }}
                                            </a>
                                        </span>
                                        <span>
                                            <a href="javascript:;">
                                                <i class="material-icons">cloud_download</i>
                                                {{ $playlist->downloads->count() }}
                                            </a>
                                        </span>
                                        <span>
                                            <a href="javascript:;">
                                                <i class="material-icons">shopping_basket</i>
                                                ${{ $playlist->orders->sum('total') }}
                                            </a>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <h5>{{ __('Artiste: ') }}
                                        <a href="{{ $playlist->user->adminRoute() }}">
                                            {{ $playlist->user->_name() }}
                                        </a>
                                        {{ __('Price: ') }} ${{ $playlist->_price() }}
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body ">
                    <div class="tabs--dash">
                        @include('admin.layouts.tabs', [
                        'tabs' => [
                        [ 'name' => __('Music')." (" .$playlist->items->count(). ")", 'route' =>
                        route('admin.musics.playlist.profile.home', ['id' =>
                        $playlist->id]), 'icon' => 'library_music'],
                        [ 'name' => __('Report'), 'route' => route('admin.musics.playlist.profile.report', ['id' =>
                        $playlist->id]), 'icon' => 'report'],
                        [ 'name' => __('Settings'), 'route' => route('admin.musics.playlist.profile.settings', ['id' =>
                        $playlist->id]), 'icon' => 'settings'],
                        ]
                        ])
                    </div>
                    <div class="tab-content">
                        @yield('playlist-profile')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection