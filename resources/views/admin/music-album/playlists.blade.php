@extends('admin.music_album')

@section('music-album')
@include('admin.layouts.search', ['filter' => true])
{{ $playlists->links() }}
<table class="table">
    <thead>
        <tr>
            <th class="text-center">#</th>
            <th>{{ __('Title') }}</th>
            <th>{{ __('Author') }}</th>
            <th>{{ __('Price') }}</th>
            <th>{{ __('Status') }}</th>
            <th>{{ __('Recorded on') }}</th>
            <th>{{ __('Musics') }}</th>
            <th>{{ __('Actions') }}</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($playlists as $playlist)
        <tr>
            <td class="text-center">{{ $playlist->id }}</td>
            <td><a href="{{ $playlist->adminRoute() }}">{{ $playlist->_name() }}</a></td>
            <td><a href="{{ $playlist->user->adminRoute() }}">{{ $playlist->user->_name() }}</a></td>
            <td>${{ $playlist->_price() }}</td>
            @if ($playlist->active)
            <th class="text-success">{{ __('Actif') }}</th>
            @else
            <th class="text-danger">{{ __('Inactif') }}</th>
            @endif
            <td>{{ $playlist->created_at }}</td>
            <td>{{ $playlist->items->count() }}</td>
            <td class="td-actions">
                <a href="{{ $playlist->adminRoute() }}" class="btn btn-info">
                    <i class="material-icons">link</i>
                </a>
                <button class="btn {{ $playlist->active ? 'btn-danger': 'btn-primary' }}" onclick="event.preventDefault();
                    document.getElementById('delete-playlist-form-{{ $playlist->id }}').submit();">
                    <i class="material-icons">{{ $playlist->active ? 'close': 'done' }}</i>
                </button>
                <form id="delete-playlist-form-{{ $playlist->id }}"
                    action="{{ route('admin.musics.playlist.modify', ['id' => $playlist->id, 'redirect' => request()->fullUrl()]) }}"
                    method="POST" style="display: none;">
                    @method('PATCH')
                    @csrf
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<div class="text-center">
    @empty($playlists->items())
    <span class="text-warning">{{ __('No Items found') }}</span>
    @endempty
</div>
@endsection