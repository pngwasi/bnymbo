@extends('admin.layouts.admin')
@section('nav-title', __('Client'))

@section('content')
<div data-controller="Client" class="client" data-Client-get-clients="{{ route('admin.client.all') }}"
    data-Client-search-client="{{ route('admin.client.search') }}"
    data-Client-client-details="{{ route('admin.client.details') }}"
    data-Client-client-following="{{ route('admin.client.following') }}"
    data-Client-client-followers="{{ route('admin.client.followers') }}"
    data-Client-client-likes="{{ route('admin.client.likes') }}"
    data-Client-client-purchase="{{ route('admin.client.purchase') }}"
    data-Client-client-to-artiste="{{ route('admin.client.clientToArtist') }}">
    <div class="row justify-content-center">
        <spinning-dots style="
                    width:50px;
                    stroke-width:5px;
                    color: #68c3ff;" />
    </div>
</div>
@endsection