@extends('admin.news')

@section('news-content')
<div data-controller="news--New-Article" data-news--New-Article-link-endpoint="{{ route('admin.news.editor-link') }}"
    data-news--New-Article-add-article="{{ route('admin.news.add-article') }}">
    <div class="row">
        <div class="col-md-4 col-sm-12">
            <form data-action="news--New-Article#newArticle" method="post" autocomplete="off"
                enctype="multipart/form-data">
                <div class="form-group">
                    <label class="pure-material-textfield-outlined w-100">
                        <input type="text" name="title" data-target="news--New-Article.title" autofocus>
                        <span>{{ __('Title') }}</span>
                    </label>
                </div>
                <div class="form-group my-3">
                    <div class="w-100">
                        <label for="category"></label>
                        <select id="category" class="form-control" name="category">
                            <option value="" selected>{{ __('Categories') }}...</option>
                            @foreach ($categories as $category)
                            <option value="{{  $category->id }}">{{  $category->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-12">
                        <input type="file" class="dropify" accept="image/*"
                            data-allowed-file-extensions="jpg jepg png gif" name="image" data-max-file-size="5M" />
                    </div>
                </div>
                <button type="submit" class="btn btn-primary btn-outline-dark">{{ __('Save') }}</button>
            </form>
        </div>
        <div class="col-md-8 col-sm-12">
            <div class="editor-landing">
                <div class="text-center"><b>{{ __('Content') }}</b></div>
                <div class="card">
                    <div class="card-body">
                        <div id="editorjs"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" tabindex="-1" data-target="news--New-Article.modalViewer" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body"></div>
            </div>
        </div>
    </div>
</div>
@endsection