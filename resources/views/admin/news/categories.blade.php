@extends('admin.news')

@section('news-content')
<div class="row justify-content-center">
    <div class="col-12 col-lg-7">
        <form action="{{ route('admin.news.add-category') }}" method="post" data-action="News#newCategory">
            <div class="form-row">
                <div class="col-lg-7 col-12">
                    <div class="form-group">
                        <label class="pure-material-textfield-outlined w-100">
                            <input type="text" name="name" autofocus>
                            <span>{{ __('Category Name') }}</span>
                        </label>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary btn-sm">{{ __('Save') }}</button>
        </form>
        <table class="table">
            <thead>
                <tr>
                    <th class="text-center">#</th>
                    <th>{{ __('Name') }}</th>
                    <th>{{ __('Actions') }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($categories as $category)
                <tr>
                    <td class="text-center">{{ $category->id }}</td>
                    <td>{{ $category->name }}</td>
                    <td class="td-actions">
                        --
                        {{-- <form onsubmit="return confirm('?')" class="d-inline" id="delete-category-form-{{ $category->id }}"
                            action="{{ route('admin.news.category.delete', ['id' => $category->id, 'redirect' => request()->fullUrl()]) }}"
                            method="POST">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="btn btn-danger">
                                <i class="material-icons">close</i>
                            </button>
                        </form> --}}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection