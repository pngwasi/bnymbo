@extends('admin.news')

@section('news-content')
@include('admin.layouts.search', ['filter' => true])
{{ $articles->links() }}
<table class="table">
    <thead>
        <tr>
            <th class="text-center">#</th>
            <th>{{ __('Image') }}</th>
            <th>{{ __('Title') }}</th>
            <th>{{ __('Views') }}</th>
            <th>{{ __('Recorded on') }}</th>
            <th>{{ __('Status') }}</th>
            <th>{{ __('Actions') }}</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($articles as $article)
        <tr>
            <td class="text-center">{{ $article->id }}</td>
            <td>
                <div class="img-container">
                    <img class="img-cover-full" src="{{ asset($article->_image()) }}" alt="{{ $article->_title() }}"
                        rel="nofollow">
                </div>
            </td>
            <td>{{ Str::substr($article->_title(), 0, 35) }}</td>
            <td>{{ $article->views }}</td>
            <td>{{ $article->created_at }}</td>

            @if ($article->active)
            <th class="text-success">{{ __('Actif') }}</th>
            @else
            <th class="text-danger">{{ __('Inactif') }}</th>
            @endif

            <td class="td-actions">
                <a href="{{ $article->route() }}" class="btn btn-info" target="\_blank" t>
                    <i class="material-icons">link</i>
                </a>
                <button class="btn {{ $article->active ? 'btn-danger': 'btn-primary' }}" onclick="event.preventDefault();
                    document.getElementById('delete-article-form-{{ $article->id }}').submit();">
                    <i class="material-icons">{{ $article->active ? 'close': 'done' }}</i>
                </button>
                <form id="delete-article-form-{{ $article->id }}"
                    action="{{ route('admin.news.article.modify', ['id' => $article->id, 'redirect' => request()->fullUrl()]) }}"
                    method="POST" style="display: none;">
                    @method('PATCH')
                    @csrf
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<div class="text-center">
    @empty($articles->items())
    <span class="text-warning">{{ __('No Items found') }}</span>
    @endempty
</div>
@endsection