@extends('admin.layouts.admin')
@section('nav-title', __('Purchase'))
@section('content')
<div class="container-fluid" data-controller="Artists" aria-live="polite">
    @include('admin.layouts.tabs', [
    'tabs' => [
    [ 'name' => __('Purchases'), 'route' => route('admin.purchase')],
    ]
    ])
    @yield('artist-out')
    <div class="card">
        <div class="card-body">
            @yield('purchase-content')
        </div>
    </div>
</div>
@endsection