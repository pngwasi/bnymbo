@extends('admin.layouts.admin')
@section('nav-title', __('Artists'))
@section('content')
<div class="container-fluid" data-controller="Artists" aria-live="polite">
    <div>
        <button class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#NewArtist">
            <i class="material-icons">add_circle</i>
            {{__('New')}}
        </button>
    </div>
    @include('admin.layouts.tabs', [
    'tabs' => [
    [ 'name' => __('Artistes'), 'route' => route('admin.artist')],
    [ 'name' => __('Status Inactif'), 'route' => route('admin.artist.status')],
    [ 'name' => __('Artiste Request'), 'route' => route('admin.artist.request')]
    ]
    ])
    @yield('artist-out')
    <div class="card">
        <div class="card-body">
            @yield('artist')
        </div>
    </div>
    @include('admin.artist.modals.NewArtist')
</div>
@endsection