{{ $artistes->links() }}
<table class="table">
    <thead>
        <tr>
            <th class="text-center">#</th>
            <th>{{ __('Image') }}</th>
            <th>{{ __('Name') }}</th>
            <th>{{ __('Email') }}</th>
            <th>{{ __('Status') }}</th>
            <th>{{ __('Recorded on') }}</th>
            <th>{{ __('Subscribers') }}</th>
            <th>{{ __('Musics') }}</th>
            <th>{{ __('Actions') }}</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($artistes as $artiste)
        <tr>
            <td class="text-center">{{ $artiste->id }}</td>
            <td>
                <div class="img-container">
                    <img class="img-cover-full" src="{{ asset($artiste->_image()) }}" alt="{{ $artiste->_name() }}"
                        rel="nofollow">
                </div>
            </td>
            <td>{{ $artiste->_name() }}</td>
            <td>{{ $artiste->email }}</td>
            @if ($artiste->active)
            <th class="text-success">{{ __('Actif') }}</th>
            @else
            <th class="text-danger">{{ __('Inactif') }}</th>
            @endif
            <td>{{ $artiste->created_at }}</td>
            <td>{{ $artiste->followers->count() }}</td>
            <td>{{ $artiste->musics->count() }}</td>
            <td class="td-actions">
                <a href="{{ $artiste->adminRoute() }}" class="btn btn-info">
                    <i class="material-icons">person</i>
                </a>
                <button class="btn {{ $artiste->active ? 'btn-danger': 'btn-primary' }}" onclick="event.preventDefault();
                    document.getElementById('delete-artist-form-{{ $artiste->id }}').submit();">
                    <i class="material-icons">{{ $artiste->active ? 'close': 'done' }}</i>
                </button>
                <form id="delete-artist-form-{{ $artiste->id }}"
                    action="{{ route('admin.artist.modify', ['artiste' => $artiste->id, 'redirect' => request()->fullUrl()]) }}"
                    method="POST" style="display: none;">
                    @method('PATCH')
                    @csrf
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

<div class="text-center">
    @empty($artistes->items())
    <span class="text-warning">{{ __('No Items found') }}</span>
    @endempty
</div>