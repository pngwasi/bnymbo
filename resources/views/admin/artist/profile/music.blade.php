@extends('admin.artist.profile')

@section('artist-profile')
<div data-controller="artist--Profile-Music">
    <div class="card">
        <div class="card-body">
            <div class="tabs--dash">
                <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                        <ul class="nav nav-tabs pl-0 ml-0" data-tabs="tabs">
                            <li class="nav-item">
                                <a class="nav-link active show" href="#Album" data-toggle="tab">
                                    <i class="material-icons">album</i> {{ __('Album') }}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#Playlist" data-toggle="tab">
                                    <i class="material-icons">playlist_add</i> {{ __('Playlist') }}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link " href="#Musics" data-toggle="tab">
                                    <i class="material-icons">play_circle_outline</i> {{ __('Musics') }}
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <hr>
            </div>

            <div class="tab-content">
                <div class="tab-pane active show" data-target="artist--Profile-Music.albumTab"
                    data-url-create-album="{{ route('admin.artist.create-album', ['id' => $user->id], false) }}"
                    data-url-albums="{{ route('admin.artist.get-album', ['id' => $user->id], false) }}" id="Album">
                    <div class="row">
                        <div class="col-12 col-md-3">
                            <span class="bmd-form-group">
                                <div class="input-group no-border">
                                    <input type="text" value="" class="form-control"
                                        placeholder="{{ __('Search Album') }}...">
                                    <button type="submit" class="btn btn-white btn-round btn-just-icon">
                                        <i class="material-icons">search</i>
                                    </button>
                                </div>
                            </span>
                        </div>
                        <button class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#newAlbum">
                            <i class="material-icons">add_circle</i>
                            {{__('Add Album')}}
                        </button>
                    </div>

                    <div class="row" data-target="artist--Profile-Music.albumsView"></div>
                </div>

                <div class="tab-pane" data-target="artist--Profile-Music.playlistTab"
                    data-url-new-playlist="{{ route('admin.artist.new-playlist', ['id' => $user->id], false) }}"
                    data-url-playlists="{{ route('admin.artist.get-playlists', ['id' => $user->id], false) }}"
                    id="Playlist">
                    <button class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#addPlaylist">
                        <i class="material-icons">add_circle</i>
                        {{__('Add Playlist')}}
                    </button>
                    <div class="row" data-target="artist--Profile-Music.playlistsView"></div>
                </div>

                <div class="tab-pane " data-target="artist--Profile-Music.musicTab"
                    data-url-music-post="{{ route('admin.artist.new-artist-music', ['id' => $user->id], false) }}"
                    id="Musics">
                    <div class="row mb-3">
                        <div class="col-md-8 col-12">
                            <div class="row">
                                <div class="col-lg-6 col-12">
                                    <select data-target="artist--Profile-Music.slimSelectGenres">
                                        <option data-placeholder="true"></option>
                                        @foreach ($genres as $genre)
                                        <option value="{{ $genre }}">{{ $genre }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <select data-target="artist--Profile-Music.slimSelectCategory">
                                        <option data-placeholder="true"></option>
                                        @foreach ($categorys as $category)
                                        <option value="{{ $category['type'] }}">{{ $category['name'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-12 col-lg-8">
                            <div data-target="artist--Profile-Music.uppyInit" class="uppy-initial"></div>
                        </div>
                        <div class="col-12 col-md-12 col-lg-4">
                            <div style="overflow-y: scroll; height: 475px;"
                                data-target="artist--Profile-Music.audioList"
                                data-url-get-musics="{{ route('admin.artist.get-artist-musics', ['id' => $user->id], false) }}">
                            </div>
                        </div>
                    </div>

                    <div class="row mt-1">
                        <div class="col-12">
                            <div class="card" style="box-shadow: none !important">
                                <div class="card-body">
                                    <div class="row m">
                                        <div class="col-md-5 col-12">
                                            <span class="bmd-form-group">
                                                <form data-action="artist--Profile-Music#searchMusics"
                                                    action="{{ route('admin.artist.search-artist-music', ['id' => $user->id], false) }}"
                                                    method="post">
                                                    <div class="input-group no-border">
                                                        <input type="text"
                                                            data-action="artist--Profile-Music#onSearchMusic"
                                                            name="title" class="form-control"
                                                            placeholder="{{ __('Search Musics') }}...">
                                                        <button type="submit"
                                                            class="btn btn-white btn-round btn-just-icon">
                                                            <i class="material-icons">search</i>
                                                        </button>
                                                    </div>
                                                </form>
                                            </span>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="mt-2 row" data-target="artist--Profile-Music.musicsList"
                                        data-url-get-musics="{{ route('admin.artist.get-artist-musics-list', ['id' => $user->id], false) }}"
                                        style="overflow-y: scroll; max-height: 425px; height: 425px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="addPlaylist" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">{{ __('New music playlist') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form data-action="artist--Profile-Music#newPlaylist" enctype="multipart/form-data" method="POST"
                    autocomplete="off">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="card-body">
                                    <div class="form-group">
                                        <label class="pure-material-textfield-outlined w-100">
                                            <input type="text" name="name">
                                            <span>{{ __('Playlist name') }}</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info btn-sm">{{ __('Save') }}
                            <div class="ripple-container"></div>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="newAlbum" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">{{ __('New Album') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form data-action="artist--Profile-Music#newAlbum" enctype="multipart/form-data" method="POST"
                    autocomplete="off">
                    <div class="modal-body">

                        <div class="row mt-3">
                            <div class="col-md-3 offset-md-1 mt-md-5 mr-md-5 justify-content-center">
                                <input type="file" class="dropify" accept="image/*"
                                    data-allowed-file-extensions="jpg jepg png gif" name="image"
                                    data-default-file="{{ asset('images/simples/album-placeholder.png') }}"
                                    data-max-file-size="5M" />
                            </div>
                            <div class="col-md-6">
                                <div class="card-body">
                                    <div class="form-group">
                                        <label class="pure-material-textfield-outlined w-100">
                                            <input type="text" name="title">
                                            <span>{{ __('Title') }}</span>
                                        </label>
                                    </div>
                                    <div class="form-group bmd-form-group is-filled">
                                        <label class="label-control bmd-label-static">{{ __('Release date') }}</label>
                                        <input type="text" class="form-control datetimepicker" name="release_date">
                                    </div>
                                    <div class="form-group">
                                        <label class="pure-material-textfield-outlined w-100">
                                            <input type="text" name="description">
                                            <span>{{ __('Description') }}</span>
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label class="pure-material-textfield-outlined w-100">
                                            <input type="text" name="unit_price">
                                            <span>{{ __('Unit Price') }} ({{ __('Optional') }})</span>
                                        </label>
                                    </div>
                                    @foreach ($categorys as $category)
                                    <div class="form-check form-check-radio">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="category"
                                                value="{{$category['type']}}">
                                            {{$category['name']}}
                                            <span class="circle">
                                                <span class="check"></span>
                                            </span>
                                        </label>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info">{{ __('Save') }}
                            <div class="ripple-container"></div>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection