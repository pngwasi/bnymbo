@extends('admin.artist.profile')

@section('artist-profile')
<div>
    <p>
        <button type="button" style="color:purple" class="btn btn-secondary">
            <i class="material-icons">toggle_off</i> {{ __('Disable') }}
        </button>
    </p>
    <p>
        <button type="button" style="color:red" class="btn btn-secondary">
            <i class="material-icons">delete_outline</i> {{ __('Trash') }}
        </button>
    </p>
    <p>
        <button type="button" style="color:red" class="btn btn-secondary">
            <i class="material-icons">delete_forever</i> {{ __('Remove Permanently') }}
        </button>
    </p>
</div>
@endsection