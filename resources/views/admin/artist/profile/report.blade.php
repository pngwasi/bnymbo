@extends('admin.artist.profile')

@section('artist-profile')
<div class="card" data-controller="artist--Profile-Repport">
    <div class="card-body">
        <div class="tabs--dash">
            <div class="nav-tabs-navigation">
                <div class="nav-tabs-wrapper">
                    <ul class="nav nav-tabs pl-0 ml-0" data-tabs="tabs">
                        <li class="nav-item">
                            <a class="nav-link active show" href="#Activity-tab" data-toggle="tab">
                                {{ __('Activity') }}
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#Followers-tab" data-toggle="tab">
                                {{ __('Followers') }}
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#Prints-tab" data-toggle="tab">
                                {{ __('Print') }}
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <hr>
        </div>

        <div class="tab-content">
            <div class="tab-pane active show" data-target="artist--Profile-Repport.activityTab"
                data-url-activities="{{ route('admin.artist.activities', ['id' => $user->id], false) }}"
                id="Activity-tab">
                <div class="material-datatables">
                    <table data-target="artist--Profile-Repport.activityTable"
                        class="display table  table-hover dataTable dtr-inline" style="width:100%">
                        <thead>
                            <tr>
                                <th>{{ __('Username') }}</th>
                                <th>{{ __('Email') }}</th>
                                <th>{{ __('Type') }}</th>
                                <th>{{ __('Service') }}</th>
                                <th>{{ __('Date') }}</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>{{ __('Username') }}</th>
                                <th>{{ __('Email') }}</th>
                                <th>{{ __('Type') }}</th>
                                <th>{{ __('Service') }}</th>
                                <th>{{ __('Date') }}</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>

            <div class="tab-pane" id="Followers-tab"
                data-url-followers="{{ route('admin.artist.followers', ['id' => $user->id], false) }}"
                data-target="artist--Profile-Repport.followersTab">
                <div class="material-datatables">
                    <table data-target="artist--Profile-Repport.followersTable"
                        class="display table  table-hover dataTable dtr-inline" style="width:100%">
                        <thead>
                            <tr>
                                <th>{{ __('Username') }}</th>
                                <th>{{ __('Email') }}</th>
                                <th>{{ __('Date') }}</th>
                                <th>{{ __('Profil') }}</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>{{ __('Username') }}</th>
                                <th>{{ __('Email') }}</th>
                                <th>{{ __('Date') }}</th>
                                <th>{{ __('Profil') }}</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>

            <div class="tab-pane" id="Prints-tab" data-target="artist--Profile-Repport.printTab">
                <div>
                    <button type="button" id="gn-repport" class="btn btn-secondary">
                        <i class="material-icons">print</i> {{ __('Generate Repport') }}
                    </button>
                </div>
                <hr>
                <div class="mb-4"> <small>{{ __('Preview Repport') }}</small> </div>
            </div>
        </div>
    </div>
</div>
@endsection