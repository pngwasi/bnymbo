@extends('admin.artist.profile')

@section('artist-profile')
<div class="row">
    <div class="col-md-6 col-12">
        <form data-action="artist--Profile#changePrimary" method="POST"
            action="{{ route('admin.artist.profile.primary', ['id' => $user->id], false) }}">
            @method('PUT')
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label class="pure-material-textfield-outlined w-100">
                        <input type="text" value="{{ $user->name }}" name="username">
                        <span>{{ __('Username') }}</span>
                    </label>
                </div>

                <div class="form-group col-md-6">
                    <label class="pure-material-textfield-outlined w-100">
                        <input type="email" value="{{ $user->email }}" name="email">
                        <span>{{ __('E-Mail Address') }}</span>
                    </label>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label class="pure-material-textfield-outlined w-100">
                        <input type="text" value="{{ $user->first_name }}" name="firstname">
                        <span>{{ __('First Name') }}</span>
                    </label>
                </div>
                <div class="form-group col-md-6">
                    <label class="pure-material-textfield-outlined w-100">
                        <input type="text" value="{{ $user->last_name }}" name="lastname">
                        <span>{{ __('Last Name') }}</span>
                    </label>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-12">
                    <label class="pure-material-textfield-outlined w-100">
                        <input type="text" value="{{ $user->description }}" name="description">
                        <span>{{ __('Description') }}</span>
                    </label>
                </div>
            </div>
            <button type="submit" class="btn btn-info btn-sm">{{ __('Save') }}</button>
        </form>
    </div>

    <div class="col-md-6 col-12">
        <form data-action="artist--Profile#changeSecondary"
            action="{{ route('admin.artist.profile.secondary', ['id' => $user->id], false) }}" method="post">
            @method('PUT')
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label class="pure-material-textfield-outlined w-100">
                        <input type="url" value="{{ $user->website }}" name="website">
                        <span>{{ __('Website') }}</span>
                    </label>
                </div>

                <div class="form-group col-md-6">
                    <label class="pure-material-textfield-outlined w-100">
                        <input type="url" value="{{ $user->facebook }}" name="facebook">
                        <span>{{ __('Facebook') }}</span>
                    </label>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label class="pure-material-textfield-outlined w-100">
                        <input type="url" value="{{ $user->twitter }}" name="twitter">
                        <span>{{ __('Twitter') }}</span>
                    </label>
                </div>

                <div class="form-group col-md-6">
                    <label class="pure-material-textfield-outlined w-100">
                        <input type="url" value="{{ $user->youtube }}" name="youtube">
                        <span>{{ __('Youtube') }}</span>
                    </label>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-12">
                    <label class="pure-material-textfield-outlined w-100">
                        <input type="url" value="{{ $user->instagram }}" name="instagram">
                        <span>{{ __('instagram') }}</span>
                    </label>
                </div>
            </div>
            <button type="submit" class="btn btn-info btn-sm">{{ __('Save') }}</button>
        </form>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-md-6 col-12">
        <p>{{ __("Generate a password") }}</p>
        <p><small>{{ __("A mail will be send to user E-mail address") }}</small></p>
        <form data-action="artist--Profile#generatePassword" method="POST"
            action="{{ route('admin.artist.profile.password', ['id' => $user->id], false) }}">
            <div class="form-row">
                <input type="hidden" name="hidden">
            </div>
            <button type="submit" class="btn btn-info">{{ __('Generate') }}</button>
        </form>
    </div>
</div>
@endsection