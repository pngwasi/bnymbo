@extends('admin.artist.profile')

@section('artist-profile')
<div>
    <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header card-header-warning card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">weekend</i>
                    </div>
                    <p class="card-category">Musics</p>
                    <h3 class="card-title">{{ $musics }}</h3>
                </div>
                <div class="card-footer"></div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header card-header-rose card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">equalizer</i>
                    </div>
                    <p class="card-category">{{ __('Albums') }}</p>
                    <h3 class="card-title">{{ $albums }}</h3>
                </div>
                <div class="card-footer"></div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">store</i>
                    </div>
                    <p class="card-category">{{ __('Playlists') }}</p>
                    <h3 class="card-title">{{ $playlists }}</h3>
                </div>
                <div class="card-footer"></div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header card-header-info card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">shopping_basket</i>
                    </div>
                    <p class="card-category">{{ __('Entry') }}</p>
                    <h3 class="card-title">${{ $orders }}</h3>
                </div>
                <div class="card-footer"></div>
            </div>
        </div>
    </div>
    {{-- <div class="row">
        <div class="col-md-4">
            <div class="card bg-gradient-default">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0 text-white">
                                Total
                                traffic</h5>
                            <span class="h2 font-weight-bold mb-0 text-white">350,897</span>
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-shape bg-white text-dark rounded-circle shadow">
                                <i class="ni ni-active-40"></i>
                            </div>
                        </div>
                    </div>
                    <p class="mt-3 mb-0 text-sm">
                        <span class="text-white mr-2"><i class="fa fa-arrow-up"></i>
                            3.48%</span>
                        <span class="text-nowrap text-light">Since last month</span>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card bg-gradient-primary">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0 text-white">New
                                users</h5>
                            <span class="h2 font-weight-bold mb-0 text-white">2,356</span>
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-shape bg-white text-dark rounded-circle shadow">
                                <i class="ni ni-atom"></i>
                            </div>
                        </div>
                    </div>
                    <p class="mt-3 mb-0 text-sm">
                        <span class="text-white mr-2"><i class="fa fa-arrow-up"></i>
                            3.48%</span>
                        <span class="text-nowrap text-light">Since last month</span>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card bg-gradient-danger">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0 text-white">
                                Performance</h5>
                            <span class="h2 font-weight-bold mb-0 text-white">49,65%</span>
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-shape bg-white text-dark rounded-circle shadow">
                                <i class="ni ni-spaceship"></i>
                            </div>
                        </div>
                    </div>
                    <p class="mt-3 mb-0 text-sm">
                        <span class="text-white mr-2"><i class="fa fa-arrow-up"></i>
                            3.48%</span>
                        <span class="text-nowrap text-light">Since last month</span>
                    </p>
                </div>
            </div>
        </div>
    </div> --}}
</div>
@endsection