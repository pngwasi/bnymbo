@extends('admin.layouts.admin')
@section('nav-title', __('Artist').' | '.'@'.$user->name)
@section('content')
<div class="container-fluid" data-controller="artist--Profile">
    <button type="button" class="btn btn-secondary btn-sm" onclick="history.back()">
        <i class="material-icons">chevron_left</i>
    </button>
    <div class="row">
        <div class="col-md-10 ml-auto mr-auto">
            <div class="card profil-card">
                <div class="raduis">
                    <div class="card-header card-header-danger card-icon-img shadow-dark-c"
                        style="background-image: url({{ asset($user->_image()) }});">
                        <span class="mask bg-gradient-default opacity-8"></span>
                        <div class="container-fluid d-flex align-items-center mt-5">
                            <div class="row">
                                <div class="col-12">
                                    <h5 class="display-4 text-white">{{ $user->_name() }}</h5>
                                </div>
                                <div class="col-lg-7 col-md-10">
                                    <p class="text-white mt-0 mb-5">{{ $user->description ?? '...' }}</p>
                                    <p>
                                        @include('templates.svg.user-link')
                                    </p>
                                    <p>{{ '@'.Str::title($user->first_name) }} {{ Str::title($user->last_name) }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body ">
                    <div class="tabs--dash">
                        @include('admin.layouts.tabs', [
                        'tabs' => [
                        [ 'name' => __('Traffic'), 'route' => route('admin.artist.profile', ['id' => $user->id]), 'icon'
                        =>
                        'home_work'],
                        [ 'name' => __('Music'), 'route' => route('admin.artist.profile.music', ['id' => $user->id]),
                        'icon' =>
                        'library_music'],
                        [ 'name' => __('Profile'), 'route' => route('admin.artist.profile.profile', ['id' =>
                        $user->id]), 'icon' =>
                        'perm_identity'],
                        [ 'name' => __('Report'), 'route' => route('admin.artist.profile.report', ['id' => $user->id]),
                        'icon' => 'report'],
                        [ 'name' => __('Shopping Basket'), 'route' => route('admin.artist.profile.orders', ['id' =>
                        $user->id]), 'icon' =>
                        'shopping_basket'],
                        [ 'name' => __('Settings'), 'route' => route('admin.artist.profile.settings', ['id' =>
                        $user->id]), 'icon' =>
                        'settings'],
                        ]
                        ])
                    </div>
                    <div class="tab-content">
                        @yield('artist-profile')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script id="artist-id" data-ui="{{ $user->id }}">
    window.$a_u = @json($user);
</script>
@endsection