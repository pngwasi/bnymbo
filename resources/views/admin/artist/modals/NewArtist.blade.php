
<div class="modal fade" id="NewArtist" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myLargeModalLabel">{{ __('New Artist') }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form data-action="Artists#newArtist" enctype="multipart/form-data" method="POST" action="{{ route('admin.artist.newArtist', [], false) }}" autocomplete="off">
                <div class="modal-body">
                    <div class="row mt-3">
                        <div class="col-md-3 offset-md-1 mt-md-5 mr-md-5 justify-content-center">
                            <input type="file" class="dropify" accept="image/*" data-allowed-file-extensions="jpg jepg png gif" name="image" data-default-file="{{ asset('images/simples/Headshot-Placeholder-1.png') }}" data-max-file-size="5M" />
                        </div>
                        <div class="col-md-6">
                            <div class="card-body">
                                <div class="form-group">
                                    <label class="pure-material-textfield-outlined w-100">
                                        <input name="username">
                                        <span>{{ __('Name') }}</span>
                                    </label>
                                </div>

                                <div class="form-group">
                                    <label class="pure-material-textfield-outlined w-100">
                                        <input name="email">
                                        <span>{{ __('E-Mail Address') }}</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-info">{{ __('Save') }}
                        <div class="ripple-container"></div>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>