@extends('admin.artist')

@section('artist')
@include('admin.artist.template.artistes-table', ['artistes' => $artistes])
@endsection