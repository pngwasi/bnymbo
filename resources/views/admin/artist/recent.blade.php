@extends('admin.artist')

@section('artist')
@include('admin.layouts.search')
@include('admin.artist.template.artistes-table', ['artistes' => $artistes])
@endsection