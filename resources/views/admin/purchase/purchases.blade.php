@extends('admin.purchase')

@section('purchase-content')
{{ $datas->links() }}
<table class="table">
    <thead>
        <tr>
            <th>{{ __('Product Name') }}</th>
            <th>{{ __('Product Type') }}</th>
            <th>{{ __('Product Image') }}</th>
            <th>{{ __('Product Price') }}</th>
            <th>{{ __('Service') }}</th>
            <th>{{ __('Amount') }}</th>
            <th>{{ __('Discounts') }}</th>
            <th>{{ __('Total') }}</th>
            <th>{{ __('Recorded On') }}</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($result as $item)
        <tr>
            <td><a href="{{ $item->product->route}}">{{ $item->product->name }}</a></td>
            <td>{{ $item->product->morthable }}</td>
            <td>
                @if ($item->product->image)
                <div class="img-container">
                    <img class="img-cover-full" src="{{ $item->product->image }}" alt="{{ $item->product->name}}"
                        rel="nofollow" />
                </div>
                @else
                <b>P</b>
                @endif
            </td>
            <td>${{ $item->product->price }}</td>
            <td>{{ $item->service_name }}</td>
            <td>${{ $item->total_paid }}</td>
            <td>${{ $item->discounts }}</td>
            <td>${{ $item->total }}</td>
            <td>{{ $item->created_at }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
<div class="text-center">
    @empty($datas->items())
    <span class="text-warning">{{ __('No Items found') }}</span>
    @endempty
</div>
@endsection