@extends('admin.layouts.app-admin')

@section('app-content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6 col-12">
            <div class="card">
                <div class="card-body">
                    <form class="form-signin" method="POST" action="{{ route('admin.login') }}">
                        @csrf
                        <div class="text-center mb-4 mt-5">
                            <h1 class="h3 mb-3 font-weight-normal">{{ $app_name }}</h1>
                            <p>{{ __('Administration') }}</p>
                        </div>
        
                        <div class="form-label-group">
                            <input type="email" id="email" class="form-control @error('email') is-invalid @enderror"
                                name="email" value="{{ old('email') }}" placeholder="Email adresse" required autofocus>
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
        
                        <div class="form-label-group">
                            <input type="password" id="password" name="password"
                                class="form-control @error('password') is-invalid @enderror" placeholder="Password" required>
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <button class="btn btn-lg btn-primary btn-block" type="submit">{{ __('Login') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection