@extends('admin.layouts.admin')
@section('nav-title', __('Musics'))

@section('content')
<div data-controller="Music-Album">
  <div class="container-fluid" aria-live="polite">
    <div>
      <button class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#GeneralPrice">
        <i class="material-icons">add_circle</i>
        {{__('General Price')}}
      </button>
      <span class="mx-3">
        <b>$<span data-target="Music-Album.price">{{ $price }}</span></b>
      </span>
    </div>
    @include('admin.layouts.tabs', [
    'tabs' => [
    [ 'name' => __('Musics'), 'route' => route('admin.music_album')],
    [ 'name' => __('Albums'), 'route' => route('admin.musics.albums')],
    [ 'name' => __('Playlists'), 'route' => route('admin.musics.playlists')],
    [ 'name' => __('Music Request'), 'route' => route('admin.musics.music-request')]
    ]
    ])
    <div class="card">
      <div class="card-body">
        @yield('music-album')
      </div>
    </div>
  </div>


  <div class="modal fade" id="GeneralPrice" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="myLargeModalLabel">{{ __('General Price, (Per Music)') }}</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <form data-action="Music-Album#generalPrice" enctype="multipart/form-data" method="POST"
          action="{{ route('admin.music-album.general-price', [], false) }}" autocomplete="off">
          <div class="modal-body">
            <div class="row ">
              <div class="card-body">
                <div class="form-group">
                  <label class="pure-material-textfield-outlined w-100">
                    <input name="price">
                    <span>{{ __('Price') }}</span>
                  </label>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-info">{{ __('Save') }}
              <div class="ripple-container"></div>
            </button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

@endsection