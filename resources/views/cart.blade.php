@extends('layouts.app')

@section('title', __('Cart'))

@section('content')

@include('layouts.navbar')
<div class="container mb-5 content-top">
    <div class="adding--card">
        <header class="mb-4">
            <h1>{{ __('Cart') }}</h1>
        </header>
        <div data-controller="Checkout--Cart" data-Checkout--Cart-cart-items="{{ route('cart.items') }}"
            data-Checkout--Cart-cart-item-delete="{{ route('cart.delete') }}"
            data-Checkout--Cart-is-auth="{{ Auth::check() ? 1 : 0 }}" data-Checkout--Cart-login="{{ route('login') }}"
            data-Checkout--Cart-checkout="{{ route('checkout') }}">
            <div class="text-center" style="color: #999999">
                <h5>{{ __('Loading') }}...</h5>
            </div>
        </div>
    </div>
</div>
@endsection