@extends('layouts.app')

@section('title', $playlist->_name() . ' | '. __('Playlist'))

@section('meta-tags')
<meta property="og:locale" content="{{ app()->getLocale() }}">
<meta property="og:type" content="playlist">
<meta property="og:title" content="{{ $playlist->_name() }}">
<meta property="og:description" content="{{ __('A Congolese and other music sales resource') }} !">
<meta property="og:url" content="{{ request()->fullUrl() }}">
<meta property="og:site_name" content="{{ $app_name }}">
@endsection

@section('content')
@include('layouts.navbar')
<div class="mb-5" data-controller="Playlist">
    <div class="container">
        <div class="playlist-user">
            <header class="entry-header typewriter">
                <h1 class="entry-title" data-controller="Title">
                    {{ $playlist->_name() }}
                </h1>
                <div class="entry-meta mt-4">
                    <div style="height: 40px;width:40px;display: inline-block;">
                        @include('templates.svg.playlist-svg')
                    </div>
                    <button data-action="Playlist#like" data-target="Playlist.LikeEl"
                        data-url-action="{{ route('likePlaylist', ['id' => $playlist->id]) }}"
                        class="btn-like {{ $liked ? 'active': '' }}">
                        @include('templates.svg.like')
                        <span class="count"> {{ $likesCount }}</span>
                    </button>
                    <span class="byline">
                        <span class="svg-icon">
                            <img alt="{{ $playlist->user->_name() }}" src="{{ asset($playlist->user->_image()) }}"
                                class="avatar avatar-48 photo img-cover-full" height="48" width="48">
                        </span>
                        <span class="author vcard">
                            <a class="url fn n author--name" href="{{ $playlist->user->route() }}">
                                {{ $playlist->user->_name() }}
                            </a>
                        </span>
                    </span>
                </div>
                <div class="entry-term mt-5">
                    <span class="entry-tag">
                        <span class="tag">
                            @include('layouts.add-to-cart')
                        </span>
                    </span>
                    <span class="entry-cat ml-3">
                        <span class="albums">
                            <a href="{{ route('browse.playlists') }}" rel="tag">{{ __('Playlists') }}</a>
                        </span>
                    </span>
                    <span class="entry-cat ml-3">
                        <span data-action="Album#like" data-target="Album.LikeEl" class="btn-like">
                            @include('templates.svg.music-svg')
                            <span>{{ $playlist->items->count() }}</span>
                        </span>
                    </span>
                </div>
            </header>
        </div>
    </div>
    <hr>
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-4 mt-3">
                <h5>{{ __('Stations') }}</h5>
                <div class="last-top block-loop-index mt-4" style="overflow-y: auto; max-height: 640px">
                    @include('layouts.items-sm', ['items' => $musics])
                </div>
            </div>
            <div class="col-12 col-lg-1"></div>
            <div class="col-12 col-lg-7 mt-4">
                <section>
                    <div class="release-container">
                        <h5>{{ __('More Playlists') }}</h5>
                        <div class="row mt-4">
                            @include('layouts.items-playlists', ['items' => $othersPlaylist, 'col' => 'col-6 col-lg-3'])
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
@endsection