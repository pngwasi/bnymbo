@extends('layouts.app')

@section('title', __('Artistes'))

@section('content')
@include('layouts.navbar')
<div data-controller="Artistes" class="container content-top">
    <div class="station--user">
        <div class="row mt-4">
            @include('layouts.items-artistes', ['items' => $artistes])
        </div>
    </div>

    <div class="text-center">
        <div class="paginate--stations">
            {{ $artistes->links() }}
        </div>
    </div>
</div>
@endsection