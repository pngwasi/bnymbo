@extends('layouts.app')

@section('content')

@include('layouts.navbar')
<div data-controller="Browse" data-Browse-category-url="{{ route('browse-set-category') }}">
    @include('layouts.browse-nav', [
    'tabs' => [
    [ 'name' => __('New Release'), 'route' => route('browse.release')],
    [ 'name' => __('Popular'), 'route' => route('browse.popular') ],
    [ 'name' => __('Stations'), 'route' => route('browse.stations')],
    [ 'name' => __('Albums'), 'route' => route('browse.albums')],
    [ 'name' => __('Playlists'), 'route' => route('browse.playlists')],
    [ 'name' => __('Genres'), 'route' => route('browse.genres')],
    ]
    ])
    <div class="container">
        @yield('browse')
    </div>
</div>
@endsection