<h1 class="page-title mb-4 ml-3" style="font-size: 1.25rem;letter-spacing: -1px;">
    {{ __("Search results for") }}: <strong>{{ request('s') }}</strong>
</h1>
<div class="search-result-nav">
    <nav id="page-navigation" class="page-navigation page-header">
        <div class="container">
            <ul class="nav">
                <li
                    class="page_item page-item-15 
                        {{ !in_array(request('section'), ['album', 'playlist', 'artiste', 'news']) ? 'current_page_item' : '' }}">
                    <a href="{{ route('search', ['s'=> request('s'), 'section' => 'station']) }}"
                        {{ !in_array(request('section'), ['album', 'playlist', 'artiste', 'news']) ? 'aria-current="page"' : '' }}>
                        {{ __("Station") }}
                        <span>{{ $result->total->station }}</span>
                    </a>
                </li>
                <li class="page_item page-item-15 {{ request('section') === 'album' ? 'current_page_item': '' }}">
                    <a href="{{ route('search', ['s'=> request('s'), 'section' => 'album']) }}"
                        {{ request('section') === 'album' ? 'aria-current="page"': '' }}>
                        {{ __("Album") }}
                        <span>{{ $result->total->album }}</span>
                    </a>
                </li>
                <li class="page_item page-item-15 {{ request('section') === 'playlist' ? 'current_page_item': '' }}">
                    <a href="{{ route('search', ['s'=> request('s'), 'section' => 'playlist']) }}"
                        {{ request('section') === 'playlist' ? 'aria-current="page"': '' }}>
                        {{ __("Playlist") }}
                        <span>{{ $result->total->playlist }}</span>
                    </a>
                </li>
                <li class="page_item page-item-15 {{ request('section') === 'artiste' ? 'current_page_item': '' }}">
                    <a href="{{ route('search', ['s'=> request('s'), 'section' => 'artiste']) }}"
                        {{ request('section') === 'artiste' ? 'aria-current="page"': '' }}>
                        {{ __("Artiste") }}
                        <span>{{ $result->total->artiste }}</span>
                    </a>
                </li>
                <li class="page_item page-item-15 {{ request('section') === 'news' ? 'current_page_item': '' }}">
                    <a href="{{ route('search', ['s'=> request('s'), 'section' => 'news']) }}"
                        {{ request('section') === 'news' ? 'aria-current="page"': '' }}>
                        {{ __("News") }}
                        <span>{{ $result->total->news }}</span>
                    </a>
                </li>
            </ul>
        </div>
    </nav>
</div>
<div class="row ml-0 mt-3">
    @switch($result->data->type)
    @case('station')
    @include('layouts.items', ['items' => $result->data->items])
    @break
    @case('album')
    @include('layouts.items-albums', ['items' => $result->data->items])
    @break
    @case('playlist')
    @include('layouts.items-playlists', ['items' => $result->data->items])
    @break
    @case('artiste')
    @include('layouts.items-artistes', ['items' => $result->data->items,'noFollow' => true])
    @break
    @case('news')
    @include('layouts.items-news', ['items' => $result->data->items, 'col' => 'col-4 col-lg-2'])
    @break
    @default
    <div class="col-12">
        <div class="user-placeholder">
            @include('templates.svg.empty-svg')
            <p> </p>
        </div>
    </div>
    @endswitch
</div>

@if (empty($result->data->items->items()))
<div class="user-placeholder">
    @include('templates.svg.empty-svg')
    <p></p>
</div>
@endif
<div class="text-center">
    <div class="paginate--stations">
        {{ $result->data->items->withQueryString()->links() }}
    </div>
</div>