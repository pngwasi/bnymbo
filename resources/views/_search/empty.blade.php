<article class="no-results not-found">
    <header class="mb-5">
        <h1>{{ __('Nothing Found') }}</h1>
    </header>
    <div class="entry-content">
        <p>
            {{ __('Sorry, but nothing matched your search terms') }}.
            {{ __('Please try again with some different keywords') }}.
        </p>
        <form role="search" data-swup-form action="{{ route('search') }}" method="get" class="search-form mt-4 mb-5">
            <input type="search" value="{{ request('s') }}" class="input" placeholder="{{ __('Search') }}…" name="s">
            <button type="submit" class="button button-primary">{{  __('Search') }}</button>
        </form>
        <p></p>
    </div>
</article>