@extends('layouts.app')

@section('title', __('News'))

@section('content')
@include('layouts.navbar')
<div class="container mb-5 content-top">
    <div class="news">
        <header class="mb-4">
            <h1>{{ __('News') }}</h1>
        </header>
        <div class="row">
            <div class="col-12 col-lg-10">
                <div class="row">
                    @foreach ($latest as $item)
                    <div class="col-6">
                        <article class="block-loop-item">
                            <a href="{{ $item->route() }}">
                                @include('layouts.figure', ['image' => $item->_image(), 'style' => 'height: 239.17px;'])
                            </a>
                            <div class="item-header mt-2">
                                <div>
                                    <a href="{{ $item->route() }}" class="artiste">
                                        <b>{{ Str::substr($item->_title(), 0, 35) }} ...</b>
                                    </a>
                                </div>
                                <div>
                                    <a href="{{ $item->route() }}" class="artiste">
                                        {{ $item->created_at->toFormattedDateString() }}
                                    </a>
                                </div>
                            </div>
                        </article>
                    </div>
                    @endforeach
                </div>
                <h4 class="mt-3">{{ __('News') }}</h4>
                <div class="last-top mt-4">
                    @foreach ($articles as $item)
                    <article class="block-loop-md block-loop-item">
                        <a href="{{ $item->route() }}">
                            @include('layouts.figure', ['image' => $item->_image()])
                        </a>
                        <div class="item-header">
                            <div>
                                <a href="{{ $item->route() }}" class="artiste">
                                    <b>{{ Str::substr($item->_title(), 0, 35) }} ...</b>
                                </a>
                            </div>
                            <div>
                                <a href="{{ $item->route() }}" class="artiste">
                                    {{ $item->created_at->toFormattedDateString() }}
                                </a>
                            </div>
                        </div>
                    </article>
                    @endforeach
                </div>
                <div class="text-center">
                    <div class="paginate--stations">
                        {{ $articles->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection