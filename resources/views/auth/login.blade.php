@extends('layouts.app')

@section('title', __('Login'))

@section('content')
@include('layouts.navbar')

<div class="container my-5">
    <div class="row justify-content-center align-items-center">
        <div class="col-md-6">
            <div class="login-form form-submittion mx-auto">
                <form enctype="multipart/form-data" action="{{ route('login', ['url' => request()->query('url')]) }}"
                    method="POST">
                    @csrf
                    <h2 class="mb-5">{{ __('Log In') }}</h2>
                    <div class="message"></div>
                    <p class="login-email">
                        <label for="email">{{ __('E-Mail Address') }}</label>
                        <input id="email" type="email" class="@error('email') is-invalid @enderror input" name="email"
                            value="{{ old('email') }}" required autocomplete="email" autofocus size="20">
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </p>

                    <p class="login-password">
                        <label for="password">{{ __('Password') }}</label>
                        <input id="password" type="password" class="@error('password') is-invalid @enderror input"
                            name="password" required size="20">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </p>

                    <p class="login-remember">
                        <label>
                            <input name="rememberme" type="checkbox" id="rememberme" name="remember"
                                {{ old('remember') ? 'checked' : '' }}>
                            <span> {{ __('Remember Me') }}</span>
                        </label>
                    </p>
                    <p class="login-submit">
                        <button type="submit" name="wp-submit" id="wp-submit" class="button button-primary">
                            {{ __('Login') }}
                        </button>
                    </p>
                    <a href="{{ route('password.request') }}" class="btn-lostpassword pretty-link">
                        {{ __('Forgot Your Password') }} ?
                    </a>
                    <a href="{{ route('register') }}" class="btn-register pretty-link">{{ __('Register') }}</a>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection