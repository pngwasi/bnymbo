@extends('layouts.app')

@section('title', __('Verify Your Email Address'))

@section('content')
@include('layouts.navbar')
<div class="container my-5">
    <div class="row justify-content-center">
    <div class="col-md-6">
            <div class="login-form form-submittion mx-auto">
                <h2>{{ __('Verify Your Email Address') }}</h2>
                <p>
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address') }}.
                        </div>
                    @endif
                </p>
                <p>
                    {{ __('Before proceeding, please check your email for a verification link') }}.
                    {{ __('If you did not receive the email') }},
                </p>
                <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                    @csrf
                    <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('Click here to request another') }}</button>.
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
