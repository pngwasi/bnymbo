@extends('layouts.app')

@section('title', __('Register'))

@section('content')
@include('layouts.navbar')
<div class="container my-5">
    <div class="row justify-content-center align-items-center">
        <div class="col-md-7">
            <div class="register-form form-submittion mx-auto">
                <form action="{{ route('register') }}" method="post">
                    @csrf
                    <h2 class="mb-5">{{ __('Register') }}</h2>
                    <div class="message"></div>
                    <p class="register-name">
                        <label for="name">{{ __('Name') }}</label>
                        <input id="name" type="text" class="@error('name') is-invalid @enderror input" name="name"
                            value="{{ old('name') }}" size="20" required autocomplete="name" autofocus>
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </p>
                    <div class="row">
                        <div class="col-lg-6 col-12">
                            <p class="register-email">
                                <label for="email">{{ __('E-Mail Address') }}</label>
                                <input id="email" type="email" class="@error('email') is-invalid @enderror input"
                                    name="email" value="{{ old('email') }}" required autocomplete="email" size="20">
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </p>
                        </div>
                        <div class="col-lg-6 col-12">
                            <p class="register-phone">
                                <label for="phone">{{ __('Phone Number') }}</label>
                                <input id="tel" type="phone" class="@error('phone') is-invalid @enderror input"
                                    name="phone" value="{{ old('phone') }}" required autocomplete="phone" size="20">
                                @error('phone')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </p>
                        </div>
                    </div>
                    <p class="register-password">
                        <label for="password">{{ __('Password') }}</label>
                        <input id="password" type="password" class="@error('password') is-invalid @enderror input"
                            name="password" required size="20" autocomplete="new-password">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </p>
                    <p class="register-password-confirm">
                        <label for="password-confirm">{{ __('Confirm Password') }}</label>
                        <input id="password-confirm" type="password" class="input" name="password_confirmation" required
                            autocomplete="new-password" size="20">
                    </p>

                    <p class="register-submit">
                        <button type="submit" name="wp-submit" id="wp-submit" class="button button-primary">
                            {{ __('Register') }}
                        </button>
                    </p>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection