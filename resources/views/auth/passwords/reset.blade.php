@extends('layouts.app')

@section('title', __('Reset Password'))


@section('content')
@include('layouts.navbar')

<div class="container my-5">
    <div class="row justify-content-center">
        <div class="col-md-6">
                <div class="login-form form-submittion mx-auto">
                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf
                        <h2>{{ __('Reset Password') }}</h2>
                        <input type="hidden" name="token" value="{{ $token }}">
                        <p class="reset-email">
                            <label for="email">{{ __('E-Mail Address') }}</label>
                            <input id="email" type="email" class="@error('email') is-invalid @enderror input" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </p>

                        <p class="reset-email">
                            <label for="password">{{ __('Password') }}</label>
                            <input id="password" type="password" class="@error('password') is-invalid @enderror input" name="password" required autocomplete="new-password">
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </p>

                        <p class="reset-password-confirm">
                            <label for="password-confirm">{{ __('Confirm Password') }}</label>
                            <input id="password-confirm" type="password" class="input" name="password_confirmation" required autocomplete="new-password">
                        </p>

                        <p class="reset-submit">
                            <button type="submit" class="button button-primary">
                                {{ __('Reset Password') }}
                            </button>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
