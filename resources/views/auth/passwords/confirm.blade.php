@extends('layouts.app')

@section('title', __('Confirm Password'))


@section('content')
@include('layouts.navbar')

<div class="container  my-5">
    <div class="row justify-content-center">
        <div class="col-md-5">
            <div class="login-form">
                <form method="POST" action="{{ route('password.confirm') }}">
                    @csrf
                    <h2>{{ __('Confirm Password') }}</h2>
                    <p class="confirm-password">
                        <label for="password">{{ __('Password') }}</label>
                        <p>{{ __('Please confirm your password before continuing') }}.</p>
                        <input id="password" type="password" class="@error('password') is-invalid @enderror input" name="password" required autocomplete="current-password">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </p>
                    <p class="confirm-submit">
                        <button type="submit" class="button button-primary">
                            {{ __('Confirm Password') }}
                        </button>
                        <p>
                            @if (Route::has('password.request'))
                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                {{ __('Forgot Your Password') }} ?
                            </a>
                            @endif
                        </p>
                    </p>
                </form>
            </div>
        </div>
    </div>
@endsection