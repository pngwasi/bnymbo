@extends('layouts.app')

@section('title', __('Reset Password'))

@section('content')
@include('layouts.navbar')

<div class="container my-5">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="login-form form-submittion mx-auto">
                <form method="POST" action="{{ route('password.email') }} ">
                    @csrf
                    <h2 class="mb-5">{{ __('Reset Password') }}</h2>
                    <div class="message"></div>

                    <p>
                        {{ __('Please enter your username or email address') }}. {{ __('You will receive a link to create a new password via email') }}.
                    </p>

                    <p class="forgot-email">
                        <label for="email">{{ __('E-Mail Address') }}</label>
                        <input id="email" type="email" class="@error('email') is-invalid @enderror input" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </p>

                    <p class="login-submit">
                        <button type="submit" name="wp-submit" id="wp-submit" class="button button-primary">
                            {{ __('Send Password Reset Link') }}
                        </button>
                    </p>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection