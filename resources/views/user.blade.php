@extends('layouts.app')

@section('title', $user->_name())

@section('meta-tags')
<meta property="og:locale" content="{{ app()->getLocale() }}">
<meta property="og:type" content="{{ $user->artiste ? 'artiste': 'client' }}">
<meta property="og:title" content="{{ $user->_name() }}">
<meta property="og:description"
    content="{{ $user->description ?: __('A Congolese and other music sales resource') }} !">
<meta property="og:url" content="{{ request()->fullUrl() }}">
<meta property="og:site_name" content="{{ $app_name }}">
<meta property="og:image" content="{{ asset($user->_image()) }}">
<meta property="og:image:type" content="image/jpeg" />
@endsection

@section('content')
@include('layouts.navbar', ['dark' => (!Auth::check() || (Auth::check() && Auth::user()->id !== $user->id))])
<div class="mb-5" data-controller="User">
    @if ((!Auth::check() || (Auth::check() && Auth::user()->id != $user->id)))
    <div class="station--user">
        <div id="carousel-example-1z" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                    <div class="view" style="background-image: url({{ asset($user->_image()) }});">
                        <div class="mask rgba-black-light d-flex align-items-center">
                            <div class="container user">
                                <header class="entry-header typewriter">
                                    <h1 class="entry-title" data-controller="Title">
                                        {{ $user->_name() }}</h1>
                                    <div class="entry-description">
                                        {{ $user->description }}
                                    </div>
                                    <div class="entry-meta">
                                        @if (!Auth::check() || (Auth::check() && Auth::user()->id != $user->id))
                                        <button class="btn-follow button-rounded"
                                            data-url-action="{{ route('followUser', ['id' => $user->id]) }}">
                                            <span class="following"
                                                style="display: {{ $followed ? 'block': 'none' }}">{{ __('Following') }}</span>
                                            <span class="follow"
                                                style="display: {{ $followed ? 'none': 'block' }}">{{ __('Follow') }}</span>
                                        </button>
                                        @endif
                                        @include('templates.svg.user-link')
                                    </div>
                                </header>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif

    <main>
        <div class="page-header-user">
            <nav class="user-navigation">
                <div class="container">
                    <ul class="nav">
                        @foreach([
                        ['name' => __('Stations'), 'route' => 'user', 'aliase' => 'stations', 'count' => $musicsCount ],
                        ['name' => __('Albums'), 'route' => 'user.albums', 'aliase' => 'albums', 'count' => $albumsCount
                        ],
                        ['name' => __('Playlists'), 'route' => 'user.playlists', 'aliase' => 'playlists', 'count' =>
                        $playlistsCount ],
                        ['name' => __('Followers'), 'route' => 'user.followers', 'aliase' => 'followers', 'count' =>
                        $followersCount ],

                        ['name' => __('Following'), 'route' => 'user.following', 'aliase' => 'following', 'count' =>
                        $followingCount ],
                        ['name' => __('Purchase'), 'route' => 'user.purchase', 'aliase'=> 'purchase', 'count' => null ],
                        ['name' => __('Profile'), 'route' => 'user.profile', 'aliase' => 'profile', 'count' => null ],
                        ['name' => __('Upload'), 'route' => 'user.upload', 'aliase' => 'upload', 'count' => null ],
                        ['name' => __('Desconnect'), 'route' => 'logout', 'aliase' => 'desconnect', 'count' => null ],

                        ] as $tab)

                        @if ((Auth::check() && Auth::user()->id == $user->id && Auth::user()->artiste == false &&
                        !in_array($tab['aliase'], ['stations', 'albums', 'playlists', 'followers'])))
                        <li
                            class="{{ Request::url() == route($tab['route'], ['id' => $user->id, 'name' => $user->nameSlug()]) ? 'active' : '' }}">
                            @if ($tab['aliase'] == 'desconnect')
                            <form data-swup-form
                                action="{{ route($tab['route'], ['id' => $user->id, 'name' => $user->nameSlug()]) }}"
                                style="display: inline" method="post">
                                @csrf
                                <button type="submit" class="desconnect-btn">{{ $tab['name'] }}</button>
                            </form>
                            @else
                            <a
                                href="{{ route($tab['route'], ['id' => $user->id, 'name' => $user->nameSlug()]) }}">{{ $tab['name'] }}<span>{{ $tab['count'] }}</span></a>
                            @endif
                        </li>
                        @else
                        @if ((!Auth::check() && !in_array($tab['aliase'], ['following', 'purchase', 'profile', 'upload',
                        'desconnect'])) || (Auth::check() && Auth::user()->id != $user->id && !in_array($tab['aliase'],
                        ['following', 'purchase', 'profile', 'upload', 'desconnect'])) || (Auth::check() &&
                        Auth::user()->id == $user->id && Auth::user()->artiste == true))
                        <li
                            class="{{ Request::url() == route($tab['route'], ['id' => $user->id, 'name' => $user->nameSlug()]) ? 'active' : '' }}">
                            @if ($tab['aliase'] == 'desconnect')
                            <form data-swup-form
                                action="{{ route($tab['route'], ['id' => $user->id, 'name' => $user->nameSlug()]) }}"
                                style="display: inline" method="post">
                                @csrf
                                <button type="submit" class="desconnect-btn">{{ $tab['name'] }}</button>
                            </form>
                            @else
                            <a
                                href="{{ route($tab['route'], ['id' => $user->id, 'name' => $user->nameSlug()]) }}">{{ $tab['name'] }}<span>{{ $tab['count'] }}</span></a>
                            @endif
                        </li>
                        @endif
                        @endif
                        @endforeach
                    </ul>
                </div>
            </nav>
        </div>
        <div class="container" style="{{ (Auth::check() && Auth::user()->id == $user->id) ? 'margin-top:70px': '' }}">
            <section class="mt-5">
                @if (isset($user_items) && $user_items !== false && empty($user_items->items()))
                <div class="user-placeholder">
                    @include('templates.svg.empty-svg')
                    <p> </p>
                </div>
                @endif
                @yield('user-content')
            </section>
            @if(isset($user_items) && $user_items !== false && $user_items->nextPageUrl())
            <div class="load--mr" id="load--more" data-next="{{ $user_items->nextPageUrl() ?? 'false' }}">
                <a href="javascript:;" class="jscroll-next button">
                    <span class="loading" style="font-size: 19px">• • •</span><span
                        class="screen-reader-text">{{ __('More') }}</span>
                </a>
            </div>
            @endif
        </div>
    </main>
</div>
@endsection