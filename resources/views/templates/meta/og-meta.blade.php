    {{-- <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16"> --}}
    <meta name="description" content="Jonathan Reinink&amp;#8217;s much-anticipated course on database performance in Laravel is now available, and has already sold over 1,200 copies!  In this course, Jonathan shows you how to drastically improve the performance of your Laravel applications by pushing more work to the database, all while still using the Eloquent ORM.">
    <meta property="og:locale" content="en_US">
    <meta property="og:type" content="article">
    <meta property="og:title" content="Eloquent Performance Patterns video course by Jonathan Reinink (sponsor)">
    <meta property="og:description" content="Jonathan Reinink&amp;#8217;s much-anticipated course on database performance in Laravel is now available, and has already sold over 1,200 copies!  In this course, Jonathan shows you how to drastically improve the performance of your Laravel applications by pushing more work to the database, all while still using the Eloquent ORM.">
    <meta property="og:url" content="https://laravel-news.com/eloquent-performance-patterns">
    <meta property="og:site_name" content="Laravel News">
    {{-- <meta property="article:publisher" content="https://www.facebook.com/laravelnews">
    <meta property="article:section" content="News">
    <meta property="article:published_time" content="2020-06-22T09:00:56-04:00"> --}}
    <meta property="og:image" content="https://i1.wp.com/wp.laravel-news.com/wp-content/uploads/2020/06/banner.png?fit=1120%2C560&amp;ssl=1?w=600&amp;h=304">
    <meta property="og:image:width" content="600">
    <meta property="og:image:height" content="304">