@extends('layouts.app')

@section('title', $route_genre. ' | '. __('Genre') )

@section('content')

@include('layouts.navbar')
<div data-controller="Browse" class="mb-5 content-top" data-Browse-category-url="{{ route('browse-set-category') }}">
    <div class="container">
        <header>
            <div class="d-inline-block mr-4">
                <div class="dropdown">
                    <a class="dropdown-toggle" type="button" id="dropdownGenreList" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                            <b>{{ $route_genre }}</b>
                    </a>
                    <div class="dropdown-menu" id="genre--list" aria-labelledby="dropdownGenreList">
                        @foreach ($genres as $genre)
                        <a href="{{ route('genre', ['name' => Str::slug($genre)]) }}"
                            class="dropdown-item">{{ $genre }}</a>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="d-inline-block">@include('layouts.categories')</div>
        </header>
    </div>
    <hr>
    <div class="container">
        @if (empty($musics->items()))
            <div class="user-placeholder">
                @include('templates.svg.empty-svg')
                <p> </p>
            </div>
        @endif
        <div class="row">
            @include('layouts.items', ['items' => $musics])
        </div>
        <div class="text-center">
            <div class="paginate--stations">
                {{ $musics->links() }}
            </div>
        </div>
    </div>
</div>
@endsection
