@extends('user')

@section('user-content')
<div class="row">
    <div class="col-12">
        <div class="upload--user">
            @if (Auth::user()->artiste == false)
                @if ($countRequest <= 4)
                <h1 class="display text-primary text-center">{{ __('Are you an arstit') }} ?</h1>
                <p class="has-medium-font-size text-center">
                    {{ __('Please fill in all the requested fields and we will contact you as soon as possible') }}
                </p>
                <form autocomplete="off" enctype="multipart/form-data"  action="{{ route('user.upload.artiste-request', [
                        'id' => Auth::user()->id, 
                        'name' => Str::slug(Auth::user()->name),
                        'url' => request()->fullUrl()
                    ]) }}" method="post">
                    @csrf
                    <div class="row justify-content-center">
                        <div class="col-lg-8 col-md-12">
                            @if (session('send') )
                            <div class="alert alert-success alert-dismissible fade show" style="font-size: 14px;">
                                <ul class="m-0">
                                    @if (session('send'))
                                    <li>{{ session('send') }}</li>
                                    @endif
                                    <button type="button" class="close pt-0 pb-0" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true" style="font-size: 15px;">&times;</span>
                                    </button>
                                </ul>
                            </div>
                            @endif
                            @if ($errors->any() || session('fail') )
                            <div class="alert alert-danger alert-dismissible fade show" style="font-size: 14px;">
                                <ul class="m-0">
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                    @if (session('fail'))
                                    <li>{{ session('fail') }}</li>
                                    @endif
                                    <button type="button" class="close pt-0 pb-0" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true" style="font-size: 15px;">&times;</span>
                                    </button>
                                </ul>
                            </div>
                            @endif
                            <div class="login-form mt-0">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <p>
                                            <label> {{ __('E-Mail Address') }} <span class="required">*</span></label>
                                            <input type="email" value="{{ old('email') }}" name="email"
                                                class="is-invalid input" required>
                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </p>
                                    </div>
                                    <div class="col-lg-6">
                                        <p>
                                            <label> {{ __('Phone') }} <span class="required">*</span></label>
                                            <input type="text" value="{{ old('phone') }}" name="phone"
                                                class="is-invalid input" required>
                                            @error('phone')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </p>
                                    </div>
                                </div>
                                <p>
                                    <label>{{ __('Image') }} ({{ __('Optional') }})</label>
                                    <input type="file" value="{{ old('picture_url') }}" name="picture_url"
                                        class="is-invalid input" required>
                                    @error('picture_url')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </p>
                                <p>
                                    <label> {{ __('Media Stream') }} (<small>Ex: MP3, WAVES, AAC</small>)
                                        <span class="required">*</span>
                                    </label>
                                    <input type="file" value="{{ old('media_stream') }}" name="media_stream"
                                        class="is-invalid input" required>
                                    @error('media_stream')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </p>
                                <p>
                                    <label>
                                        {{ __('Description') }}
                                    </label>
                                    <textarea name="description" class="is-invalid input" cols="25"
                                        rows="5">{{ old('description') ?: Auth::user()->description }}</textarea>
                                    @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </p>
                                <p>
                                    <button type="submit" class="button button-primary">{{ __('Send') }}</button>
                                    <small class="ml-4"><b>({{ $countRequest }}/4)</b></small>
                                </p>
                            </div>
                        </div>
                    </div>
                </form>
                @else
                <h1 class="display text-primary text-center"> ...</h1>
                <p class="has-medium-font-size text-center">
                    {{ __('Your requests are being processed') }}
                </p>
                @endif
            @else
            <h1 class="display text-primary text-center">{{ __('Propose your music') }}</h1>
            <div class="text-center">
                <small><em>{{ __('You have an artist status') }}</em></small>
            </div>
            <p class="has-medium-font-size text-center">
                {{ __('Please fill in all the requested fields and we will contact you as soon as possible') }}</p>
            <form autocomplete="off" enctype="multipart/form-data" action="{{ route('user.upload.music-request', [
                        'id' => Auth::user()->id, 
                        'name' => Str::slug(Auth::user()->name),
                        'url' => request()->fullUrl()
                    ]) }}" method="POST">
                @csrf
                <div class="row justify-content-center">
                    <div class="col-lg-8 col-md-12">
                        @if (session('send'))
                        <div class="alert alert-success alert-dismissible fade show" style="font-size: 14px;">
                            <ul class="m-0">
                                @if (session('send'))
                                <li>{{ session('send') }}</li>
                                @endif
                                <button type="button" class="close pt-0 pb-0" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true" style="font-size: 15px;">&times;</span>
                                </button>
                            </ul>
                        </div>
                        @endif
                        @if ($errors->any() || session('fail') )
                        <div class="alert alert-danger alert-dismissible fade show" style="font-size: 14px;">
                            <ul class="m-0">
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                                @if (session('fail'))
                                <li>{{ session('fail') }}</li>
                                @endif
                                <button type="button" class="close pt-0 pb-0" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true" style="font-size: 15px;">&times;</span>
                                </button>
                            </ul>
                        </div>
                        @endif
                        <div class="login-form mt-0">
                            <p>
                                <label> {{ __('E-Mail Address') }} <span class="required">*</span></label>
                                <input type="email" value="{{ old('email') ?: Auth::user()->email }}" name="email"
                                    class="is-invalid input" required>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </p>
                            <p>
                                <label> {{ __('Media Stream') }} (<small>Ex: MP3, WAVES, AAC</small>)
                                    <span class="required">*</span>
                                </label>
                                <input type="file" value="{{ old('media_stream') }}" name="media_stream"
                                    class="is-invalid input" required>
                                @error('media_stream')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </p>
                            <p>
                                <label>
                                    {{ __('Description') }}
                                </label>
                                <textarea name="description" class="is-invalid input" cols="25"
                                    rows="5">{{ old('description') ?: Auth::user()->description }}</textarea>
                                @error('description')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </p>
                            <p>
                                <button type="submit" class="button button-primary">{{ __('Propose') }}</button>
                            </p>
                        </div>
                    </div>
                </div>
            </form>
            @endif
        </div>
    </div>
</div>
@endsection