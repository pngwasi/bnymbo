@extends('user')

@section('user-content')
<div data-controller="user--Profile">
    <form data-swup-form enctype="multipart/form-data"
        action="{{ route('user.profileModify', ['id' => Auth::user()->id, 'name' => Str::slug(Auth::user()->name)]) }}"
        method="POST" class="form">
        @csrf
        @method('PUT')
        <div class="row offset-lg-1">
            <div class="col-lg-2 col-md-3 col-sm-6 mb-2">
                <div class="upload--img-u">
                    <label>{{ __('Poster') }} *</label>
                    <div class="upload-image">
                        <div>
                            <img {{ Auth::user()->image !== 'avatar.jpg' ? "src=".asset(Auth::user()->_image()) : '' }}
                                data-target="user--Profile.imageDisplay" style="width: 100%; height: 100%;" />
                        </div>
                        <input type="file" data-action="user--Profile#onChangeImage" id="file" accept="image/*"
                            class="is-invalid file-upload-m" name="image">
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-9 offset-lg-1">
                @if (session('success') )
                <div class="alert alert-success alert-dismissible fade show" style="font-size: 14px;">
                    <ul class="m-0">
                        @if (session('success'))
                        <li>{{ session('success') }} !</li>
                        @endif
                        <button type="button" class="close pt-0 pb-0" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true" style="font-size: 15px;">&times;</span>
                        </button>
                    </ul>
                </div>
                @endif
                @if ($errors->any())
                <div class="alert alert-danger alert-dismissible fade show" style="font-size: 14px;">
                    <ul class="m-0">
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                        <button type="button" class="close pt-0 pb-0" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true" style="font-size: 15px;">&times;</span>
                        </button>
                    </ul>
                </div>
                @endif
                <div class="login-form mt-0">
                    <div class="row">
                        <div class="col-lg-6">
                            <p>
                                <label> {{ __('First Name') }} <span class="required">*</span></label>
                                <input type="text" value="{{ old('firstname') ?: Auth::user()->first_name }}"
                                    name="firstname" class="is-invalid input" required>
                                @error('firstname')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </p>
                        </div>
                        <div class="col-lg-6">
                            <p>
                                <label> {{ __('Last Name') }} <span class="required">*</span></label>
                                <input type="text" value="{{ old('lastname') ?: Auth::user()->last_name }}"
                                    name="lastname" class="is-invalid input" required>
                                @error('lastname')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </p>
                        </div>
                    </div>
                    <p>
                        <label>{{ __('Display Name') }}<span class="required">*</span></label>
                        <input type="text" value="{{ old('name') ?: Auth::user()->name }}" name="name"
                            class="is-invalid input" required>
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </p>
                    <p>
                        <label> {{ __('E-Mail Address') }} <span class="required">*</span> </label>
                        <input type="email" value="{{ old('email') ?: Auth::user()->email }}" name="email"
                            class="is-invalid input" required>
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </p>
                    <p>
                        <label> {{ __('Phone Number') }} <span class="required">*</span> </label>
                        <input type="tel" value="{{ old('phone') ?: Auth::user()->phone }}" name="phone"
                            class="is-invalid input" required>
                        @error('phone')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </p>
                    @if(Auth::user()->artiste == true)
                    <p>
                        <label>
                            {{ __('Description') }} ({{ __('Optional') }})
                        </label>
                        <textarea name="description" class="is-invalid input" cols="25"
                            rows="5">{{ old('description') ?: Auth::user()->description }}</textarea>
                        @error('description')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </p>
                    <p>
                        <label>
                            {{ __('Website') }} ({{ __('Optional') }})
                        </label>
                        <input type="text" value="{{ old('website') ?: Auth::user()->website }}" name="website"
                            class="is-invalid input">
                        @error('website')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </p>
                    <p>
                        <label>
                            {{ __('Facebook') }} ({{ __('Optional') }})
                        </label>
                        <input type="text" value="{{ old('facebook') ?: Auth::user()->facebook }}" name="facebook"
                            class="is-invalid input">
                        @error('facebook')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </p>
                    <p>
                        <label>
                            {{ __('Twitter') }} ({{ __('Optional') }})
                        </label>
                        <input type="text" value="{{ old('twitter') ?: Auth::user()->twitter }}" name="twitter"
                            class="is-invalid input">
                        @error('twitter')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </p>
                    <p>
                        <label>
                            {{ __('YouTube') }} ({{ __('Optional') }})
                        </label>
                        <input type="text" value="{{ old('youtube') ?: Auth::user()->youtube }}" name="youtube"
                            class="is-invalid input">
                        @error('youtube')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </p>
                    <p>
                        <label>
                            {{ __('Instagram') }} ({{ __('Optional') }})
                        </label>
                        <input type="text" value="{{ old('instagram') ?: Auth::user()->instragram }}" name="instagram"
                            class="is-invalid input">
                        @error('instagram')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </p>
                    @endif
                    <p>
                        <label>
                            {{ __('Current password') }}
                        </label>
                        <input type="password" name="password" class="is-invalid input" required>
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </p>
                    <p>
                        <label>
                            {{ __('New password') }} ({{ __('Optional') }})
                        </label>
                        <input type="password" name="new_password" class="is-invalid input">
                        @error('new_password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </p>
                    <p>
                        <label>
                            {{ __('Comfirm new password') }} ({{ __('Optional') }})
                        </label>
                        <input type="password" name="new_password_confirmation" class="is-invalid input">
                        @error('password_confirmation')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </p>
                    <p>
                        <button type="submit" class="button button-primary">{{ __('Save') }}</button>
                    </p>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection