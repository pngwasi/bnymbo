@extends('user')

@section('user-content')
<div id="rowElement" class="row">
    @include('layouts.items-followers', ['items' => $user_items])
</div>
@endsection
