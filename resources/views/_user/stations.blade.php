@extends('user')

@section('user-content')
<div id="rowElement" class="row">
    @include('layouts.items', ['items' => $user_items])
</div>
@endsection
