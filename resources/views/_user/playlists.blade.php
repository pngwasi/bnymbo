@extends('user')

@section('user-content')
<div id="rowElement" class="row">
    @include('layouts.items-playlists', ['items' => $user_items])
</div>
@endsection
