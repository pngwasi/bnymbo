@extends('layouts.app')

@section('title', $music->_title() . ' | '. __('Station'))

@section('meta-tags')
<meta property="og:audio" content="{{ $music->adminPlayer() }}" />
<meta property="og:audio:type" content="audio/mpeg" />
<meta property="og:locale" content="{{ app()->getLocale() }}">
<meta property="og:type" content="station">
<meta property="og:title" content="{{ $music->_title() }}">
<meta property="og:description" content="{{ __('A Congolese and other music sales resource') }} !">
<meta property="og:url" content="{{ request()->fullUrl() }}">
<meta property="og:site_name" content="{{ $app_name }}">
<meta property="og:image" content="{{ asset($music->_image()) }}">
<meta property="og:image:type" content="image/jpeg" />
@endsection

@section('content')
@include('layouts.navbar', ['dark' => true])
<div class="mb-5" data-controller="Station">
    <div class="station--user">
        <div id="carousel-example-1z" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                    <div class="view" style="background-image: url({{ asset($music->_image()) }});">
                        <div class="mask rgba-black-light d-flex align-items-center">
                            <div class="container">
                                <header class="entry-header typewriter">
                                    <h1 class="entry-title" data-controller="Title">{{ $music->_title() }}</h1>
                                    <div class="entry-meta">
                                        <span>
                                            <button class="btn-play"
                                                data-url-playings="{{ route('playingCount', ['id' => $music->id]) }}"
                                                data-action="Station#playOrPause" data-target="Station.playBtn">
                                                play
                                            </button>
                                            {{ $playingsCount }}
                                        </span>

                                        <button data-action="Station#like" data-target="Station.LikeEl"
                                            data-url-action="{{ route('likeMusic', ['id' => $music->id]) }}"
                                            class="btn-like {{ $liked ? 'active': '' }}">
                                            @include('templates.svg.like')
                                            <span class="count">{{ $likesCount }}</span>
                                        </button>

                                        <button class="btn-like">
                                            @include('templates.svg.download')
                                            <span class="count">{{ $downloadsCount }}</span>
                                        </button>

                                        <button class="btn-like">
                                            <span class="svg-icon">
                                                <img alt="{{ $music->user->_name() }}"
                                                    src="{{ asset($music->user->_image()) }}"
                                                    class="avatar avatar-48 photo img-cover-full" />
                                            </span>
                                            <span class="author vcard">
                                                <a class="url fn n author--name" href="{{ $music->user->route() }}">
                                                    {{ $music->user->_name() }}
                                                </a>
                                            </span>
                                        </button>
                                    </div>

                                    <div class="entry-term">
                                        <span class="entry-tag">
                                            <span class="tag">
                                                @include('layouts.add-to-cart')
                                            </span>
                                        </span>
                                        <span class="entry-cat">
                                            <span class="genre">
                                                <a href="{{ route('genre', ['name' => Str::slug($music->genre)]) }}"
                                                    rel="tag">{{ $music->genre }}</a>
                                            </span>
                                            @if ($music->album)
                                            <span class="album ml-3">
                                                <a href="{{ $music->album->route() }}"
                                                    rel="tag">{{ $music->album->_name() }}</a>
                                            </span>
                                            @endif
                                        </span>
                                    </div>
                                </header>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script id="music-id" data-swup-reload-script data-mi="{{ $music->id }}">
        window.$music = [{
            "name": "{{ $music->_title() }}"
            , "artist": "{{ $music->user->_name() }}"
            , "album": "{{ $music->album ? $music->album->_name() : '' }}"
            , "url": "{{ $music->adminPlayer() }}"
            , "cover_art_url": "{{ asset($music->_image()) }}"
        }];

    </script>
    <div class="container mt-3">
        <div class="row">
            <div class="col-12 col-lg-4 player--station--user">
                @include('layouts.amplitudes', ['url' => request()->fullUrl(), 'title' => $music->_title()])
            </div>
            <div class="col-12 col-lg-1"></div>
            <div class="col-12 col-lg-7 mt-5">
                <section>
                    <div class="release-container">
                        <h5>{{ __('More from') }} {{ $music->user->_name() }}</h5>
                        <div class="row mt-4">
                            @include('layouts.items', ['items' => $moreMusics, 'col' => 'col-6 col-lg-3'])
                        </div>
                    </div>
                </section>
                <section class="similar-station">
                    <div class="row mt-5">
                        <div class="col-12">
                            <h5>{{ __('Similar') }}</h5>
                            <div class="last-top block-loop-index mt-4">
                                @include('layouts.items-sm', ['items' => $similars])
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
@endsection