<div id="comments" class="comments-area">
    <div id="respond" class="comment-respond">
        <h4 id="reply-title" class="comment-reply-title">
            <small><b>{{ __('Leave a Reply') }}</b></small>
        </h4>
        <form onsubmit="return false" action="" method="post">
            @csrf
            <div class="login-form mt-0">
                <p>
                    <label>
                        {{ __('Comment') }}
                    </label>
                    <textarea name="comment" disabled class="is-invalid input" cols="45" rows="8">{{ old('comment') }}</textarea>
                    @error('comment')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </p>
                @auth
                    <div class="text-muted mb-4">{{ __('Reply as') }} <b>{{ Auth::user()->email }}</b></div>
                @endauth
                @guest
                <div class="row">
                    <div class="col-lg-4 col-sm-12">
                        <p>
                            <label> {{ __('Name') }} <span class="required">*</span></label>
                            <input type="text" disabled value="{{ old('name') }}" name="name" class="is-invalid input" required>
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </p>
                    </div>
                    <div class="col-lg-4 col-sm-12">
                        <p>
                            <label> {{ __('E-Mail Address') }} <span class="required">*</span></label>
                            <input type="email" disabled value="{{ old('email') }}" name="email" class="is-invalid input"
                                required>
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </p>
                    </div>
                    <div class="col-lg-4 col-sm-12">
                        <p>
                            <label> {{ __('Website') }} ({{ __('Optional') }})</label>
                            <input type="url" disabled value="{{ old('website') }}" name="website" class="is-invalid input"
                                required>
                            @error('Website')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </p>
                    </div>
                </div>
                @endguest
                <p>
                    <button type="submit" disabled class="button btn-sm mt-1">{{ __('Post Comment') }}</button>
                </p>
            </div>
        </form>
    </div>
</div>