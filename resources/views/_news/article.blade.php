@extends('layouts.app')

@section('title', $article->_title() . ' | '. __('Article'))

@section('meta-tags')
<meta property="og:locale" content="{{ app()->getLocale() }}">
<meta property="og:type" content="article">
<meta property="og:title" content="{{ $article->_title() }}">
<meta property="og:description" content="{{  $article->description ?: __('A Congolese and other music sales resource') }} !">
<meta property="og:url" content="{{ request()->fullUrl() }}">
<meta property="og:site_name" content="{{ $app_name }}">
<meta property="og:image" content="{{ asset($article->_image()) }}">
<meta property="og:image:type" content="image/jpeg" />
@endsection

@section('content')
@include('layouts.navbar', ['dark' => true])
<div class="mb-5" data-controller="Article">
    <div class="station--user">
        <div id="carousel-example-1z" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                    <div class="view" style="background-image: url({{ asset($article->_image()) }});">
                        <div class="mask rgba-black-light d-flex align-items-center">
                            <div class="container">
                                <header class="entry-header typewriter">
                                    <h1 class="entry-title" data-controller="Title">
                                        {{ $article->_title() }}
                                    </h1>
                                    <div class="entry-meta">
                                        <span class="byline">
                                            @if ($article->user)
                                            <span class="svg-icon">
                                                <img alt="{{ $article->user->_name() }}"
                                                    src="{{ asset($article->user->_image()) }}" class="img-cover-full"
                                                    height="48" width="48">
                                            </span>
                                            @endif
                                            <span class="author vcard">
                                                @if ($article->user)
                                                <a class="url fn n" href="{{ $article->user->rout() }}">
                                                    {{ $article->user->_name() }}
                                                </a>
                                                @else
                                                <span>{{ $app_name }}</span>
                                                @endif
                                            </span>
                                        </span>
                                        <span class="posted-on">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                viewBox="0 0 24 24" class="svg-icon" fill="none" stroke="currentColor"
                                                stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                aria-hidden="true" role="img" focusable="false">
                                                <circle cx="12" cy="12" r="10"></circle>
                                                <polyline points="12 6 12 12 16 14"></polyline>
                                            </svg>
                                            <time class="entry-date published" datetime="{{ $article->created_at }}">
                                                {{ $article->created_at->toFormattedDateString() }}
                                            </time>
                                            <span class="cat-links">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                    viewBox="0 0 24 24" class="svg-icon" fill="none"
                                                    stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                                    stroke-linejoin="round" aria-hidden="true" role="img"
                                                    focusable="false">
                                                    <polyline points="21 8 21 21 3 21 3 8"></polyline>
                                                    <rect x="1" y="3" width="22" height="5"></rect>
                                                    <line x1="10" y1="12" x2="14" y2="12"></line>
                                                </svg>
                                                @if ($article->category)
                                                <span class="clickable">{{ $article->category->name }}</span>
                                                @else
                                                <span class="clickable">{{ __('Uncategorized') }}</span>
                                                @endif
                                            </span>
                                            <span class="comments-link">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                    viewBox="0 0 24 24" class="svg-icon" fill="none"
                                                    stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                                    stroke-linejoin="round" aria-hidden="true" role="img"
                                                    focusable="false">
                                                    <path
                                                        d="M21 15a2 2 0 0 1-2 2H7l-4 4V5a2 2 0 0 1 2-2h14a2 2 0 0 1 2 2z">
                                                    </path>
                                                </svg>
                                                <a href="#comments">{{ __('Leave a comment') }}</a>
                                            </span>
                                    </div>
                                </header>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script data-swup-reload-script>
        window.$article = @json(json_decode($article->json, true))
    </script>
    <div class="container mt-3 article--profile">
        <div class="row">
            <div class="col-12 col-lg-10 article-content" data-target="Article.content">
                <div class="d-flex justify-content-center">
                    <div class="spinner-border" role="status">
                        <span class="sr-only">Loading...</span>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-10 mt-5">
                @include('_news._comment')
            </div>
        </div>
    </div>
</div>
@endsection