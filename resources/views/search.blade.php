@extends('layouts.app')

@section('title', __("Search Results for"). " “".request('s')."”")

@section('content')
@include('layouts.navbar')
<div class="container mb-5 content-top" data-controller="Search">
    <div class="site-content">
        <div id="primary" class="content-area">
            <main id="main" class="site-main">
                @if ($result->empty > 0)
                @include('_search.result')
                @else
                @include('_search.empty')
                @endif
            </main>
        </div>
    </div>
</div>
@endsection