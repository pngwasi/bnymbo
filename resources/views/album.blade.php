@extends('layouts.app')

@section('title', $album->_name() . ' | '. __('Album'))

@section('meta-tags')
<meta property="og:locale" content="{{ app()->getLocale() }}">
<meta property="og:type" content="album">
<meta property="og:title" content="{{ $album->_name() }}">
<meta property="og:description"
    content="{{  $album->description ?: __('A Congolese and other music sales resource') }} !">
<meta property="og:url" content="{{ request()->fullUrl() }}">
<meta property="og:site_name" content="{{ $app_name }}">
<meta property="og:image" content="{{ asset($album->_image()) }}">
<meta property="og:image:type" content="image/jpeg" />
@endsection

@section('content')
@include('layouts.navbar', ['dark' => true])
<div class="mb-5" data-controller="Album">
    <div class="station--user">
        <div id="carousel-example-1z" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                    <div class="view" style="background-image: url({{ asset($album->_image()) }});">
                        <div class="mask rgba-black-light d-flex align-items-center">
                            <div class="container">
                                <header class="entry-header typewriter">
                                    <h1 class="entry-title" data-controller="Title">
                                        {{ $album->_name() }}</h1>
                                    <div class="entry-description">
                                        {{ $album->description }}
                                    </div>
                                    <div class="entry-meta">
                                        <span>
                                            <button class="btn-play" data-action="Album#playOrPause"
                                                data-target="Album.playBtn">
                                                play
                                            </button>
                                        </span>

                                        <button data-action="Album#like" data-target="Album.LikeEl"
                                            data-url-action="{{ route('likeAlbum', ['id' => $album->id]) }}"
                                            class="btn-like {{ $liked ? 'active': '' }}">
                                            @include('templates.svg.like')
                                            <span class="count">{{ $likesCount }}</span>
                                        </button>

                                        <button class="btn-like">
                                            <span class="svg-icon">
                                                <img alt="{{ $album->user->_name() }}"
                                                    src="{{ asset($album->user->_image()) }}"
                                                    class="avatar avatar-48 photo img-cover-full" />
                                            </span>
                                            <span class="author vcard">
                                                <a class="url fn n author--name" href="{{ $album->user->route() }}">
                                                    {{ $album->user->_name() }}
                                                </a>
                                            </span>
                                        </button>
                                    </div>

                                    <div class="entry-term">
                                        <span class="entry-tag">
                                            <span class="tag">
                                                @include('layouts.add-to-cart')
                                            </span>
                                        </span>
                                        <span class="entry-cat"><span class="albums"><a
                                                    href="{{ route('browse.albums') }}"
                                                    rel="tag">{{ __('Albums') }}</a></span></span>
                                        <span class="entry-cat">
                                            <span data-action="Album#like" data-target="Album.LikeEl" class="btn-like">
                                                @include('templates.svg.music-svg')
                                                <span class="count">{{ $album->musics->count() }}</span>
                                            </span>
                                        </span>
                                    </div>
                                </header>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script id="music-id" data-swup-reload-script data-ai="{{ $album->id }}">
        window.$music = [
            @foreach($musics as $music) {
                "name": "{{ $music->_title() }}"
                , "artist": "{{ $album->user->_name() }}"
                , "album": "{{ $music->album ? $music->album->_name() : '' }}"
                , "url": "{{ $music->adminPlayer() }}"
                , "cover_art_url": "{{ asset($music->_image()) }}"
            }
            , @endforeach
        ];

    </script>
    <div class="container mt-3">
        <div class="row">
            <div class="col-12 col-lg-4 player--station--user">
                @include('layouts.amplitudes', ['url' => request()->fullUrl(), 'title' => $album->_name()])
            </div>
            <div class="col-12 col-lg-1"></div>
            <div class="col-12 col-lg-7 mt-5">
                <section>
                    <div class="row">
                        <div class="col-12">
                            <h5>{{ __('Stations') }}</h5>
                            <div class="last-top block-loop-index mt-4" style="overflow-y: auto; max-height: 640px">
                                @include('layouts.items-sm', ['items' => $musics])
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <div class="mt-5">
            <h5>{{ __('More') }}</h5>
            <div class="row mt-4">
                @include('layouts.items-albums', ['items' => $othersAlbum])
            </div>
        </div>
    </div>
</div>
@endsection