@extends('browse')

@section('title', __('New Release'))

@section('browse')
<div class="mb-5">
    <div>@include('layouts.categories')</div>
    <div class="row mt-4">
        @include('layouts.items', ['items' => $release])
    </div>
</div>
@endsection
