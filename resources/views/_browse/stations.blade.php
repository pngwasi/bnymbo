@extends('browse')

@section('title', __('Stations'))

@section('browse')
<div class="mb-5">
    <div>@include('layouts.categories')</div>
    @if (empty($musics->items()))
            <div class="user-placeholder">
                @include('templates.svg.empty-svg')
                <p> </p>
            </div>
    @endif
    <div class="row mt-4">
        @include('layouts.items', ['items' => $musics])
    </div>
    <div class="text-center">
        <div class="paginate--stations">
            {{ $musics->links() }}
        </div>
    </div>
</div>
@endsection