@extends('browse')

@section('title', __('Playlist'))

@section('browse')
<div class="mb-5">
    @if (empty($playlists->items()))
            <div class="user-placeholder">
                @include('templates.svg.empty-svg')
                <p> </p>
            </div>
    @endif
    <div class="row mt-4">
        @include('layouts.items-playlists', ['items' => $playlists])
    </div>
    <div class="text-center">
        <div class="paginate--stations">
            {{ $playlists->links() }}
        </div>
    </div>
</div>
@endsection