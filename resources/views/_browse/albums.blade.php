@extends('browse')

@section('title', __('Album'))


@section('browse')
<div class="mb-5">
    <div>@include('layouts.categories')</div>
    @if (empty($albums->items()))
            <div class="user-placeholder">
                @include('templates.svg.empty-svg')
                <p> </p>
            </div>
    @endif
    <div class="row mt-4">
        @include('layouts.items-albums', ['items' => $albums])
    </div>
    <div class="text-center">
        <div class="paginate--stations">
            {{ $albums->links() }}
        </div>
    </div>
</div>
@endsection