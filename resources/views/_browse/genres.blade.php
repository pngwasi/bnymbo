@extends('browse')

@section('title', __('Genres'))

@section('browse')
<div class="music--genres">
    <h4 class="h4 mb-4">{{ __('Music genre') }}</h4>
    <div class="row mb-4">
        @foreach ($genres->genres_short as $genre)
            <div class="col-6 col-sm-4 col-md-3 col-lg-2-4 col-xl-2">
                <div class="card text-white bg-secondary mb-3" style="max-width: 100%;background: linear-gradient(180deg,transparent,rgba(0,0,0,.5));">
                    <div class="card-body text-center py-5">
                        <h6> <a href="{{ route('genre', ['name' => Str::slug($genre)]) }}">{{ $genre }}</a></h6>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
@endsection
