@extends('browse')

@section('title', __('Browse'))

@section('browse')
<section class="browse--news">
    <div class="wp-block-cover has-background-dim gd-primary rounded">
        <div class="wp-block-cover__inner-container">
            <p style="text-align:center" class="has-large-font-size display"><strong>
                    {{ __('Discover songs you’ll love') }}
                </strong></p>
            <p style="text-align:center" class="w-50--n">
                {{ __('Find the most popular artists and listen their music on BnB') }}. {{ __('Create your own playlist when listening to them') }}
            </p>
            <p></p>
            <div class="wp-block-button aligncenter text-primary">
                <a class="wp-block-button__link has-text-color has-vivid-green-cyan-color has-background" href="{{ route('browse.release') }}" style="background-color:#ffffff">
                    {{ __('Explore the new music') }}
                </a>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="release-container mt-4">
        <h5>{{ __('Featured Tracks') }}</h5>
        <div class="row mt-4">
            @include('layouts.items', ['items' => $populars])
        </div>
    </div>
</section>
@endsection