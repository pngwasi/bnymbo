@extends('browse')

@section('title', __('Popular'))

@section('browse')
<div class="mb-5">
    <div>@include('layouts.categories')</div>
    <div class="row mt-4">
        @include('layouts.items', ['items' => $popular])
    </div>
</div>
@endsection