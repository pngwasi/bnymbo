@extends('layouts.app')

@section('title', __('Checkout'))

@section('content')

@include('layouts.navbar')
<div class="container mb-5 content-top">
    <div class="adding--card">
        <header class="mb-4">
            <h1>{{ __('Checkout') }}</h1>
        </header>
        <script data-swup-reload-script>
            window.authUser = @json(Auth::user())
        </script>
        <div data-controller="Checkout--Checkout" data-Checkout--Checkout-cart-items="{{ route('cart.items') }}">
            <div class="text-center" style="color: #999999">
                <h5>{{ __('Loading') }}...</h5>
            </div>
        </div>
    </div>
</div>
</div>
@endsection