<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMusicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('musics', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->double('specific_price')->default(0);
            $table->date('release_date');
            $table->string('category')->nullable();
            $table->string('genre')->nullable();

            $table->unsignedBigInteger('album_id')->nullable();
            $table->foreign('album_id')
                ->references('id')->on('albums')
                ->onDelete('cascade');
            
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            
            $table->boolean('active')->default(true);
            $table->string('image')->default('music.png');
            $table->string('private_file')->nullable();
            $table->string('public_file')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('musics');
    }
}
