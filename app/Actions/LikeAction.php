<?php

namespace App\Actions;

use App\Statics\CurrencyFormat;
use Illuminate\Http\Request;

trait LikeAction
{

    /**
     * @param Request $request
     * @param \Illuminate\Database\Eloquent\Model|static $model
     * @param CurrencyFormat $format
     */
    protected function Like(Request $request, $model, CurrencyFormat $format)
    {
        $model->with('likes');
        $auth = $request->user();
        if (!$auth) {
            return response(['redirect_url' => route('login', [
                'url' => $request->query('url'), 'nonce' => $request->query('nonce'),
            ])], 302);
        }
        $likes = $model->likes();
        $liked = $likes->where('user_id', $auth->id)->first();
        $action = null;
        if ($liked) {
            $liked->delete();
            $action = 'unlike';
        } else {
            $likes->create(['user_id' => $auth->id]);
            $action = 'like';
        }
        $likes = $model->likes();
        return ['action' => $action, 'count' => $format->format($likes->count())];
    }
}
