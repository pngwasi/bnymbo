<?php

namespace App\Statics;

use App\Repository\AlbumRepository;
use App\Repository\MusicRepository;
use App\Repository\PlaylistRepository;
use Illuminate\Support\Facades\Auth;

class CartTable
{

    /**
     * Undocumented variable
     *
     * @var array
     */
    public $table = ['Playlist', 'Album', 'Station'];


    /**
     * Undocumented variable
     *
     * @var string
     */
    public $cartSession = 'bnb-cart';

    /**
     * Undocumented function
     *
     * @return string
     */
    public function playlist()
    {
        return $this->table[0];
    }

    /**
     * Undocumented function
     *
     * @return string
     */
    public function album()
    {
        return $this->table[1];
    }

    /**
     * Undocumented function
     *
     * @return string
     */
    public function station()
    {
        return $this->table[2];
    }

    /**
     * Undocumented function
     *
     * @param int $id
     * @return array
     */
    public function itemAlbum($itemKey, $id, $price, AlbumRepository $albumRp)
    {
        $album = $albumRp->findAlbumOrFail($id);
        return [
            'itemKey' => $itemKey,
            'id' => $id,
            'price' => $price,
            'section' => $this->album(),
            'image' => asset($album->_image()),
            'product' => $album->_name()
        ];
    }

    /**
     * Undocumented function
     *
     * @param int $id
     * @return array
     */
    public function itemPlaylist($itemKey, $id, $price, PlaylistRepository $playlistRp)
    {
        $playlist = $playlistRp->findPlaylistOrFail($id);
        return [
            'itemKey' => $itemKey,
            'id' => $id,
            'price' => $price,
            'section' => $this->playlist(),
            'image' => '',
            'product' => $playlist->_name()
        ];
    }

    /**
     * Undocumented function
     *
     * @param int $id
     * @return array
     */
    public function itemStation($itemKey, $id, $price, MusicRepository $stationRp)
    {
        $station = $stationRp->findMusicOrFail($id);
        return [
            'itemKey' => $itemKey,
            'id' => $id,
            'price' => $price,
            'section' => $this->station(),
            'image' => asset($station->_image()),
            'product' => $station->_title()
        ];
    }

    /**
     * Undocumented function
     *
     * @param array $item
     * @param string $key
     * @return void
     */
    public function getItemBySection(array $item, string $itemKey)
    {
        $params = ['itemKey' => $itemKey, 'id' => $item['id'], 'price' => $item['price']];
        switch ($item['section']) {
            case $this->table[0]:
                return app()->call(self::class . '@itemPlaylist', $params);
                break;
            case $this->table[1]:
                return app()->call(self::class . '@itemAlbum', $params);
                break;
            case $this->table[2]:
                return app()->call(self::class . '@itemStation', $params);
                break;
            default:
                return null;
                break;
        }
    }
}
