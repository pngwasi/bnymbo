<?php

namespace App\Statics;

class Statics
{
    /**
     * @param mixed $morth
     * @param mixed $morthable
     * 
     * @return array
     */
    public function morthable($morth, $morthable): array
    {
        $date = $morth->created_at->toDateTimeString();
        $id = $morth->id;

        switch ($morthable->getMorphClass()) {
            case 'App\Models\Music':
                $type = [
                    'id' => $id,
                    'morthable' => trans('Music'),
                    'name' => $morthable->_title(),
                    'image' => asset($morthable->_image()),
                    'route' => $morthable->adminRoute(),
                    'created_at' => $date
                ];
                break;
            case 'App\Models\Album':
                $type = [
                    'id' => $id,
                    'morthable' => trans('Album'),
                    'name' => $morthable->_name(),
                    'image' => asset($morthable->_image()),
                    'route' => $morthable->adminRoute(),
                    'created_at' => $date
                ];
                break;
            case 'App\Models\Playlist':
                $type = [
                    'id' => $id,
                    'morthable' => trans('Playlist'),
                    'name' => $morthable->_name(),
                    'image' => null,
                    'route' => $morthable->adminRoute(),
                    'created_at' => $date
                ];
                break;
            default:
                break;
        }
        return $type ?? [];
    }
}
