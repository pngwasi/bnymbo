<?php

namespace App\Statics;

class File
{
    /**
     * @var string ARTIST_PATH
     */
    public const ARTIST_IMG_PATH = 'artistes/profiles';

    /**
     * @var string ALBUM_IMG_PATH
     */
    public const ALBUM_IMG_PATH = 'artistes/albums';

    /**
     * @var string MUSIC_IMG_PATH
     */
    public const MUSIC_IMG_PATH = 'artistes/musics';

    /**
     * @var string MUSIC_IMG_PATH
     */
    public const BLOG_IMG_PATH = 'blogs/articles';

    /**
     * @var string MUSIC_PATH
     */
    public const MUSICS_MSC_PATH = 'musics';

    /**
     * @var string SHORT_MUSIC_FILENAME
     */
    public const SHORT_MUSIC_FILENAME = 'short_cut.mp3';

    /**
     * @var int SIZE
     */
    public const SIZE = 1024;

    /**
     * @var array IMAGE_RULES
     */
    public const IMAGE_RULES = ['required', 'file', "max:" . (self::SIZE * 5) . "", 'dimensions:min_width=400,min_height=400', 'mimes:jpeg,jpg,png,gif'];

    /**
     * Undocumented function
     *
     * @param int|string $uniq
     * @return string
     */
    public static function uniqidPathArtist($uniq)
    {
        return self::ARTIST_IMG_PATH . '/' . $uniq;
    }
}
