<?php

namespace App\Statics;


class Category {

    public array $type = [
        'mundane' => 'mundane',
        'christian' => 'christian'
    ];

    public function types()
    {
        return [
            [
                'name' => trans('Religious'),
                'type' => 'christian'
            ],
            [
                'name' => trans('Mundane'),
                'type' => 'mundane'
            ]
        ];
    }

}
