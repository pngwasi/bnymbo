<?php

namespace App\Statics;


class Genres
{

    public $genres = [
        'Autre', 'Afro', 'Afrobeat', 'Afro-congo', 'Benga-music', 'Blues', 'Classique', 'country',
        'Coupé-Decalé', 'Electro', 'Gospel', 'jazz', 'K-pop', 'Hip-hop', 'Ndombolo', 'Rap', 'Reggae', 'Rumba', 'Rock&roll',
        'R&B', 'Samba', 'Slam', 'Soul', 'Techno', 'Tradutional', 'Zouk', 'Salsa'
    ];

    public $genres_short = [
        'Autre', 'Afro', 'Afrobeat', 'Afro-congo', 'Benga-music', 'Blues', 'Classique', 'country',
        'Coupé-Decalé', 'Electro', 'Gospel', 'jazz', 'K-pop', 'Hip-hop', 'Ndombolo', 'Rap', 'Reggae', 'Rumba', 'Rock&roll',
        'R&B', 'Samba', 'Slam', 'Soul', 'Techno', 'Tradutional', 'Zouk', 'Salsa'
    ];
}
