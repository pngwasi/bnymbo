<?php

namespace App;

use Illuminate\Auth\Passwords\CanResetPassword as PasswordsCanResetPassword;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;


class User extends Authenticatable implements /*MustVerifyEmail */ CanResetPassword
{
    use HasApiTokens, /*Notifiable, */ PasswordsCanResetPassword;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
        'first_name', 'last_name', 'artiste', 'description',
        'website', 'facebook', 'twitter', 'youtube', 'instragram', 'phone', 'locale'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     *
     * @return string
     */
    public function _image()
    {
        // return 'storage/' . $this->image;
        return $this->storage()->url($this->image);
    }

    /**
     * Undocumented function
     *
     * @return mixed
     */
    private function storage()
    {
        return Storage::disk();
    }

    /**
     * @return string
     */
    public function _name()
    {
        return ucwords(Str::lower($this->name));
    }
    /**
     * @return string
     */
    public function nameSlug()
    {
        return Str::slug($this->name);
    }

    /**
     * @return string
     */
    public function route()
    {
        return route('user', ['id' => $this->id, 'name' => $this->nameSlug()]);
    }
    /**
     * Undocumented function
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function albums()
    {
        return $this->hasMany('App\Models\Album');
    }

    /**
     * Undocumented function
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function musics()
    {
        return $this->hasMany('App\Models\Music');
    }

    /**
     * Undocumented function
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function playlists()
    {
        return $this->hasMany('App\Models\Playlist');
    }

    /**
     * @return string
     */
    public function adminRoute()
    {
        return route('admin.artist.profile', ['id' => $this->id], false);
    }

    /**
     * Undocumented function
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function followers()
    {
        return $this->hasMany('App\Models\Follower');
    }

    /**
     * Undocumented function
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function following()
    {
        return $this->hasMany('App\Models\Follower', 'follower_id');
    }

    /**
     * Undocumented function
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function likes()
    {
        return $this->hasMany('App\Models\Like');
    }

    /**
     * Undocumented function
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function downloads()
    {
        return $this->hasMany('App\Models\Download');
    }

    /**
     * Undocumented function
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function artisteRequests()
    {
        return $this->hasMany('App\Models\ArtisteRequest');
    }

    /**
     * Undocumented function
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function musicRequests()
    {
        return $this->hasMany('App\Models\MusicRequest');
    }

    /**
     * Undocumented function
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany('App\Models\Order', 'owner_id');
    }

    /**
     * Undocumented function
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function purchase()
    {
        return $this->hasMany('App\Models\Order', 'customer_id');
    }
}
