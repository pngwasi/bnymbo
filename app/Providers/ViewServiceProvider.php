<?php

namespace App\Providers;

use App\Http\View\Composers\ArtistProfileComposer;
use App\Http\View\Composers\UserCountChildsComposer;
use App\Repository\GeneralPriceRepository;
use App\View\Components\CartIcon;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;


class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::component('cart-icon', CartIcon::class);
        View::composer('user', UserCountChildsComposer::class);
        View::composer(['admin.artist.profile', 'admin.artist.profile.*'], ArtistProfileComposer::class);
        View::composer(['admin.musics.playlists', 'admin.musics.albums', 'admin.music_album'], function ($view) {
            $view->with('price', app()->call(GeneralPriceRepository::class . "@getActivePrice"));
        });
        View::composer('*', function ($view) {
            $view->with('app_name', 'B-Nyimbo');
            $view->with('langs', [
                [
                    'name' => trans('French'),
                    'key' => 'fr'
                ],
                [
                    'name' => trans('English'),
                    'key' => 'en'
                ],
            ]);
        });
    }
}
