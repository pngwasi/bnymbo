<?php

namespace App\Providers;

use Illuminate\Support\Facades\Storage;
use TusPhp\Tus\Server as TusServer;
use Illuminate\Support\ServiceProvider;

class TusServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('tus-server', function ($app, array $attributes) {
            $path = storage_path("app/musics/{$attributes['id']}");
            if (!file_exists($path)) {
                Storage::makeDirectory("musics/{$attributes['id']}");
            }
            $server = new TusServer('redis');
            $server
                ->setApiPath("/api/tus")
                ->setUploadDir($path);

            return $server;
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
