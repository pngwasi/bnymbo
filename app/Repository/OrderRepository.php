<?php


namespace App\Repository;

use App\Models\Order;
use App\User;

class OrderRepository
{

    /**
     * @var Order
     */
    public Order $order;

    /**
     * @var UserRepository
     */
    public UserRepository $user;

    /**
     * @param Order $order
     * @param UserRepository $user
     */
    public function __construct(Order $order, UserRepository $user)
    {
        $this->order = $order;
        $this->user = $user;
    }

    /**
     * Undocumented function
     *
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Model[]|static[]
     */
    public function getOrderActivityArtist($id) {
        $user =  $this->user->findArtistOrFail($id);
        return $user->orders;
    }

}
