<?php


namespace App\Repository;

use App\Models\Follower;

class FollowerRepository
{

    public Follower $follower;

    public function __construct(Follower $follower)
    {
        $this->follower = $follower;
    }

    /**
     * Undocumented function
     *
     * @param int $id
     * @return int
     */
    public function queryUserFollowingCount($id)
    {
        return $this->follower->newQuery()
            ->where('follower_id', $id)->count();
    }
    
    /**
     * Undocumented function
     *
     * @param int $id
     * @return mixed
     */
    public function queryUserFollowing($id)
    {
        return $this->follower->newQuery()
            ->with('user')
            ->where('follower_id', $id);
    }
}
