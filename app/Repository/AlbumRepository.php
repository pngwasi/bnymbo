<?php

namespace App\Repository;

use App\Models\Album;

class AlbumRepository
{


    public Album $album;

    /**
     *
     * @param Album $album
     */
    public function __construct(Album $album)
    {
        $this->album = $album;
    }

    /**
     * Undocumented function
     *
     * @param int $id
     * @return static
     */
    public function createAlbum(string $name, string $description, string $category, string $release_date, string $specific_price)
    {
        return $this->album->newInstance([
            'name' => $name,
            'description' => $description,
            'category' => $category,
            'release_date' => $release_date,
            'specific_price' => $specific_price,
        ]);
    }

    /**
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Model|static
     */
    public function findAlbumOrFail($id)
    {
        return $this->album->newQuery()
            ->where(['id' => $id])
            ->firstOrFail();
    }

    /**
     * @param integer $id
     * @param string $category
     * @param integer $limit
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function queryLatestExceptPassed(int $id, string $category, int $limit)
    {
        return $this->album->newQuery()
            ->whereKeyNot($id)
            ->where('category', $category)
            ->latest()
            ->limit(50)
            ->get()->shuffle()->take($limit);
    }

    /**
     * @param string $category
     * @param int $limit
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function queryAlbumsByPaginate(string $category, int $limit = 16)
    {
        return $this->album->newQuery()
            ->where('category', $category)
            ->with('user')
            ->latest()
            ->paginate($limit);
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $builder
     * @param bool|string $status
     * 
     * @return \Illuminate\Database\Eloquent\Builder
     */
    private function status($builder, $status)
    {
        if (is_bool($status)) {
            $builder = $builder->where('active', $status);
        }
        return $builder;
    }
    /**
     * @param \Illuminate\Database\Eloquent\Builder $builder
     * @param string $search
     * 
     * @return mixed
     */
    private function searchable($builder, string $search)
    {
        if ($search) {
            $builder = $builder->search($search);
        }
        return $builder;
    }
    /**
     * Undocumented function
     *
     * @param integer $limit
     * @param bool|string $status
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getAlbumsByFilter($limit = 12, string $value = '', $status)
    {
        $builder = $this->status($this->album->newQuery(), $status);
        return $this->searchable($builder, $value)->latest()->paginate($limit);
    }

}
