<?php

namespace App\Repository;

use App\Statics\CurrencyFormat;
use App\User;

class UserRepository
{

    /**
     * The  user Model.
     * @var User
     */
    public User $user;

    /**
     * The  format currencies.
     * @var CurrencyFormat
     */
    public CurrencyFormat $format;


    public function __construct(User $user, CurrencyFormat $format)
    {
        $this->user = $user;
        $this->format = $format;
    }

    /**
     * Undocumented function
     *
     * @param string $fistname
     * @param string $lastname
     * @param string $email
     * @param string $username
     * @param string $image
     * @return \Illuminate\Database\Eloquent\Model|$this
     */
    public function createArtist(string $email, string $username)
    {
        return $this->user->newQuery()
            ->create([
                'artiste' => true,
                'email' => $email,
                'name' => $username
            ]);
    }

    /**
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Model|static
     */
    public function findArtistOrFail($id)
    {
        return $this->user->newQuery()
            ->where(['id' => $id, 'artiste' => true])
            ->firstOrFail();
    }


    /**
     * @param int $limit
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|object
     */
    public function all($limit = 12)
    {
        return $this->user->newQuery()->simplePaginate($limit);
    }

    /**
     * Undocumented function
     *
     * @param int $id
     * @param \Illuminate\Support\Facades\Auth $auth
     * @param FollowerRepository $followRp
     * @return array
     */
    public function countingChilds($id, $auth, FollowerRepository $followRp)
    {
        $result = [
            'playlistsCount' => 0,
            'musicsCount' => 0,
            'followersCount' => 0,
            'albumsCount' => 0,
            'followingCount' => 0,
            'followed' => false
        ];
        $user = $this->user->newQuery()
            ->with('musics')
            ->with('albums')
            ->with('playlists')
            ->with('followers')
            ->find($id);
        if (!$user) {
            return $result;
        }
        if ($auth) {
            $result['followingCount'] = $followRp->queryUserFollowingCount($auth->id);
            $follow = $user->followers()->where('follower_id', $auth->id)->first();
            $result['followed'] = !!$follow;
        }
        $result['playlistsCount'] = $this->format->format($user->playlists->count());;
        $result['musicsCount'] = $this->format->format($user->musics->count());
        $result['followersCount'] = $this->format->format($user->followers->count());
        $result['albumsCount'] = $this->format->format($user->albums->count());
        return $result;
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $builder
     * 
     * @return \Illuminate\Database\Eloquent\Builder
     */
    private function artistQuery($all = false)
    {
        $query = $this->user->newQuery()
            ->with('followers')
            ->with('musics');
        if (!$all) {
            $query = $query->where('artiste', true);
        }
        return $query;
    }

    /**
     * Undocumented function
     *
     * @param integer $limit
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getAllArtistes($limit = 12)
    {
        return $this->artistQuery()->latest()->paginate($limit);
    }

    /**
     * Undocumented function
     *
     * @param integer $limit
     * @param string $value
     * @param boolean $all
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|object
     */
    public function getArtistesFilter(int $limit = 12, string $value = '', bool $all = false)
    {
        return $this->artistQuery($all)
            ->where(function ($query) use ($value) {
                $query->where('email', 'like', "%{$value}%")
                    ->orWhere('name', 'like', "%{$value}%");
            })
            ->latest()->paginate($limit);
    }

    /**
     * Undocumented function
     *
     * @param integer $limit
     * @param string $value
     * @param boolean $status
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|object
     */
    public function getActiveArtistesFilter(int $limit = 12, string $value = '', bool $status = true)
    {
        return $this->artistQuery(false)
            ->where('active', $status)
            ->where(function ($query) use ($value) {
                $query->where('email', 'like', "%{$value}%")
                    ->orWhere('name', 'like', "%{$value}%");
            })
            ->latest()->paginate($limit);
    }


    /**
     * Undocumented function
     *
     * @param integer $limit
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getDisabledArtistes($limit = 12)
    {
        return $this->artistQuery()->where('active', false)->latest()->paginate($limit);
    }
}
