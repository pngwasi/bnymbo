<?php


namespace App\Repository;

use App\Models\Playlist;
use App\User;

class PlaylistRepository
{

    /**
     * @var Playlist
     */
    public Playlist $playlist;

    /**
     * @param Playlist $playlist
     */
    public function __construct(Playlist $playlist)
    {
        $this->playlist = $playlist;
    }

    /**
     * Undocumented function
     *
     * @param string $name
     * @return static
     */
    public function createInstanceNewPlayList(string $name)
    {
        return $this->playlist->newInstance([
            'name' => $name,
        ]);
    }

    /**
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Model|static
     */
    public function findPlaylistOrFail($id)
    {
        return $this->playlist->newQuery()
            ->whereKey($id)
            ->firstOrFail();
    }

    /**
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Model|static
     */
    public function queryLatestExceptPassed($id, $limit)
    {
        return $this->playlist->newQuery()
            ->whereKeyNot($id)
            ->latest()
            ->limit(50)
            ->get()->shuffle()->take($limit);
    }

    /**
     * @param string $category
     * @param integer $limit
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function queryPlaylistsByPaginate(?string $category = null, int $limit = 12)
    {
        return $this->playlist->newQuery()
            ->with('user')
            ->latest()
            ->paginate($limit);
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $builder
     * @param bool|string $status
     * 
     * @return \Illuminate\Database\Eloquent\Builder
     */
    private function status($builder, $status)
    {
        if (is_bool($status)) {
            $builder = $builder->where('active', $status);
        }
        return $builder;
    }
    /**
     * @param \Illuminate\Database\Eloquent\Builder $builder
     * @param string $search
     * 
     * @return mixed
     */
    private function searchable($builder, string $search)
    {
        if ($search) {
            $builder = $builder->search($search);
        }
        return $builder;
    }
    /**
     * Undocumented function
     *
     * @param integer $limit
     * @param bool|string $status
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getPlaylistsByFilter($limit = 12, string $value = '', $status)
    {
        $builder =  $this->status($this->playlist->newQuery(), $status);
        return $this->searchable($builder, $value)->latest()->paginate($limit);
    }
}
