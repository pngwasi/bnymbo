<?php


namespace App\Repository;

use App\Models\Blog\Blog;
use App\Models\Blog\BlogCategory;

class NewsRepository
{

    /**
     * @var Blog
     */
    public Blog $news;

    /**
     * @var BlogCategory
     */
    public BlogCategory $category;

    /**
     * @param Blog $news
     * @param BlogCategory $blogCategory
     */
    public function __construct(Blog $news, BlogCategory $blogCategory)
    {
        $this->news = $news;
        $this->category = $blogCategory;
    }

    /**
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Model|static
     */
    public function findAnyArticleOrFail($id)
    {
        return $this->news->newQuery()
            ->whereKey($id)
            ->firstOrFail();
    }

    /**
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Model|static
     */
    public function findArticleOrFail($id, $slug)
    {
        return $this->active()
            ->whereKey($id)
            ->where('slug', $slug)
            ->firstOrFail();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function active($active = true)
    {
        return $this->news->newQuery()->where('active', $active);
    }

    /**
     * @return static[]
     */
    public function categories()
    {
        return $this->category->all();
    }

    /**
     * @param int $limit
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getLastArticles($limit = 3)
    {
        return $this->active()->latest()->limit($limit)->get();
    }

    /**
     * @param int $limit
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getArticles($limit = 3)
    {
        return $this->active()->latest()->paginate($limit);
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $builder
     * @param bool|string $status
     * 
     * @return \Illuminate\Database\Eloquent\Builder
     */
    private function status($builder, $status)
    {
        if (is_bool($status)) {
            $builder = $builder->where('active', $status);
        }
        return $builder;
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $builder
     * @param string $search
     * 
     * @return mixed
     */
    private function searchable($builder, string $search)
    {
        if ($search) {
            $builder = $builder->search($search);
        }
        return $builder;
    }

    /**
     * Undocumented function
     *
     * @param integer $limit
     * @param bool|string $status
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getArticlesByFilter($limit = 12, string $value = '', $status)
    {
        $builder = $this->status($this->news->newQuery(), $status);
        return $this->searchable($builder, $value)->latest()->paginate($limit);
    }
}
