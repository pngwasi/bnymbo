<?php

namespace App\Repository;

use App\Models\Music;
use App\Statics\Category;
use App\User;

class MusicRepository
{


    /**
     * @var Music
     */
    private Music $music;

    /**
     * @var Category
     */
    private Category $category;

    /**
     * @param Music $music
     * @param Category $category
     */
    public function __construct(Music $music, Category $category)
    {
        $this->music = $music;
        $this->category = $category;
    }

    /**
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Model|static
     */
    public function findMusicOrFail($id)
    {
        return $this->music->newQuery()
            ->where(['id' => $id])
            ->firstOrFail();
    }

    /**
     * @param string $title
     * @param string $specific_price
     * @param string $release_date
     * @param string $category
     * @param string $genre
     * @return static
     */
    public function createInstanceNewMusic(string $title, $specific_price, string $release_date, $category, $genre, $user_id = null)
    {
        $datas = [
            'title' => $title,
            'specific_price' => $specific_price ?: 0,
            'release_date' => $release_date,
            'category' => $category,
            'genre' => $genre
        ];
        if (!is_null($user_id)) {
            $datas['user_id'] = $user_id;
        }
        return $this->music->newInstance($datas);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    private function with($query)
    {
        return $query->with('user')->with('album');
    }

    /**
     * @param string $type
     * @return \Illuminate\Database\Eloquent\Collection
     */
    private function getMusicByType(string $type)
    {
        return $this->with($this->music->newQuery())
            ->where('category', '=', $type)
            ->latest()
            ->limit(100)
            ->get()->shuffle()->take(6);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function queryLastMundaneMUsic()
    {
        return $this->getMusicByType($this->category->type['mundane']);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function queryLastChristianMUsic()
    {
        return $this->getMusicByType($this->category->type['christian']);
    }

    /**
     * @param integer $take
     * @param string|null $category
     * @return \Illuminate\Database\Eloquent\Collection
     */
    private function _queryTopLastPopular(int $take, ?string $category = null)
    {
        $musics = ($category ?
            $this->with($this->music->newQuery())->where('category', $category) :
            $this->with($this->music->newQuery()))
            ->with('likes')
            ->latest()
            ->limit(200)
            ->get();
        $sorted = $musics->sortByDesc(function ($music) {
            return $music->playings->count();
        });
        return $sorted->values()->take($take);
    }

    /**
     * @param integer $take
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function queryTopLastPopular($take = 5)
    {
        return $this->_queryTopLastPopular($take);
    }

    /**
     * * @param \Illuminate\Database\Eloquent\Collection|static[] $music
     */
    public function getMoreMusicArtist($music)
    {
        $musics = $this->with($music->user->musics())
            ->where('id', '!=', $music->id)
            ->latest()
            ->limit((4 * 3))
            ->get()->shuffle()->take(4);
        return $musics;
    }


    /**
     * @param \Illuminate\Database\Eloquent\Collection|static[] $music
     */
    private function _getSimilarMusics($music)
    {
        return $this->with($this->music->newQuery())
            ->where('category', '=', $music->category)
            ->where('user_id', '!=', $music->user->id);
    }

    /**
     * @param \Illuminate\Database\Eloquent\Collection|static[] $music
     */
    public function getSimilarMusics($music)
    {
        $musics =  $this->_getSimilarMusics($music)
            ->where('genre', $music->genre)
            ->latest()
            ->limit(100)
            ->get();
        if ($musics->isEmpty()) {
            $musics =  $this->_getSimilarMusics($music)
                ->latest()
                ->limit(100)
                ->get();
        }
        return $musics->shuffle()->take(4);
    }

    /**
     * @param string $category
     * @param integer $limit
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function release(string $category, int $limit = 18)
    {
        $musics = $this->with($this->music->newQuery())
            ->where('category', $category)
            ->latest()
            ->limit($limit)
            ->get();
        return $musics;
    }

    /**
     * @param string $category
     * @param integer $limit
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function popular(string $category, int $limit = 18)
    {
        return $this->_queryTopLastPopular($limit, $category);
    }

    /**
     * @param string $category
     * @param integer $limit
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function queryMusicsByPaginate(string $category, ?String $genre = null, int $limit = 12)
    {
        $musics =  $this->with($this->music->newQuery())
            ->where('category', $category);
        if (!is_null($genre)) {
            $musics = $musics->where('genre', $genre);
        }
        $musics = $musics->latest()
            ->paginate($limit);
        return $musics;
    }


    /**
     * @param \Illuminate\Database\Eloquent\Builder $builder
     * @param bool|string $status
     * 
     * @return \Illuminate\Database\Eloquent\Builder
     */
    private function status($builder, $status)
    {
        if (is_bool($status)) {
            $builder = $builder->where('active', $status);
        }
        return $builder;
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $builder
     * @param string $search
     * 
     * @return mixed
     */
    private function searchable($builder, string $search)
    {
        if ($search) {
            $builder = $builder->search($search);
        }
        return $builder;
    }

    /**
     * Undocumented function
     *
     * @param integer $limit
     * @param bool|string $status
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getMusicsByFilter($limit = 12, string $value = '', $status)
    {
        $builder = $this->with($this->status($this->music->newQuery(), $status));
        return $this->searchable($builder, $value)->latest()->paginate($limit);
    }
}
