<?php


namespace App\Repository;

use App\Models\GeneralPrice;

class GeneralPriceRepository
{

    /**
     * @var GeneralPrice
     */
    public GeneralPrice $price;

    /**
     * @param GeneralPrice $price
     */
    public function __construct(GeneralPrice $price)
    {
        $this->price = $price;
    }

    /**
     * @param boolean $static
     * @return object|int
     */
    public function getActivePrice($static = false)
    {
        $price = $this->price->newQuery()->latest()->first();
        return $static ? $price : ($price ? $price->price : 0);
    }

    /**
     * Undocumented function
     *
     * @return bool
     */
    public function setActivePrice($price)
    {
        $pr = $this->getActivePrice(true);
        if ($pr) {
            $pr->price = $price;
            $pr->save();
        } else {
            $this->price->newQuery()->create(['price' => $price]);
        }
        return true;
    }
}
