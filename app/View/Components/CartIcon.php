<?php

namespace App\View\Components;

use App\Statics\CartTable;
use Illuminate\Http\Request;
use Illuminate\View\Component;

class CartIcon extends Component
{

    /**
     * The class prop.
     *
     * @var string
     */
    public $class;

    /**
     * The counter notification.
     *
     * @var string
     */
    public $counter;


    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($class, CartTable $cartTable, Request $request)
    {
        $this->class = $class;
        $this->counter = count($request->session()->get($cartTable->cartSession, []));
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return <<<'blade'
            <li class="{{ $counter < 1 ? 'cart--shopping': '' }} nav-item {{ $class }}" data-target="NavBar.cartIcon">
                <a class="nav-link" href="{{ route('cart') }}">
                    @include('templates.svg.cart-svg')
                    <span class="notification">{{ $counter }}</span>
                </a>
            </li>
        blade;
    }
}
