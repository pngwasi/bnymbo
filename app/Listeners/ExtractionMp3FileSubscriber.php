<?php

namespace App\Listeners;

use App\Events\ExtractionMp3File;
use App\Statics\File;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class ExtractionMp3FileSubscriber
{
    /**
     * @var \falahati\PHPMP3\MpegAudio
     */
    private $mpg;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(\falahati\PHPMP3\MpegAudio $mpg)
    {
        $this->mpg = $mpg;
    }

    /**
     * Handle the event.
     *
     * @param  ExtractionMp3File  $event
     * @return void
     */
    public function handle(ExtractionMp3File $event)
    {
        // $filename = storage_path('app/' . $event->filename);
        // $this->mpg::fromFile($filename)
        //     ->trim($event->from, $event->starting)
        //     ->saveFile(storage_path('app/' . $event->path . '/' . File::SHORT_MUSIC_FILENAME));
    }
}
