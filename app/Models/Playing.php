<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Playing extends Model
{
    protected $table = 'playings';

    protected $fillable = ['played'];

    /**
     * Undocumented function
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function playable()
    {
        return $this->morphTo();
    }
}
