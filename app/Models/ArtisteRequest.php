<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ArtisteRequest extends Model
{
    /**
     * @var string
     */
    protected $table = 'artiste_requests';

    /**
     * @var array
     */
    protected $fillable = [
        'email', 'phone', 'picture_url', 'media_stream', 'description', 'done'
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
