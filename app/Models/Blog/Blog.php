<?php

namespace App\Models\Blog;

use App\Searchable\FullTextSearch;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class Blog extends Model
{
    use FullTextSearch;

    /**
     * @var string
     */
    protected $table = 'blogs';

    /**
     * The columns of the full text index
     */
    protected $searchable = ['title'];

    /**
     * @var array
     */
    protected $fillable = ['title', 'slug', 'image', 'json', 'decription', 'user_id', 'blog_category_id'];

    /**
     *
     * @return string
     */
    public function _image()
    {
        // return 'storage/' . $this->image;
        return $this->storage()->url($this->image);
    }

    /**
     * Undocumented function
     *
     * @return mixed
     */
    private function storage()
    {
        return Storage::disk();
    }


    /**
     * @return string
     */
    public function _title()
    {
        return ucwords(Str::lower($this->title));
    }

    /**
     * @return string
     */
    public function route()
    {
        return route('news.article', ['id' => $this->id, 'slug' => $this->slug]);
    }

    /**
     * Undocumented function
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('App\Models\Blog\BlogCategory', 'blog_category_id');
    }
}
