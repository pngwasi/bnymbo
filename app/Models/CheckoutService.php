<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CheckoutService extends Model
{
    protected $table = 'checkout_services';

    /**
     * Undocumented function
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
    public function orders() {
        return $this->hasMany('App\Models\Order', 'service_id');
    }

}
