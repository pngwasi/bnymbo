<?php

namespace App\Models;

use App\Searchable\FullTextSearch;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class Album extends Model
{
    use FullTextSearch;

    /**
     * @var string
     */
    protected $table = 'albums';

    /**
     * The columns of the full text index
     */
    protected $searchable = ['name'];

    /**
     * @var array
     */
    protected $fillable = [
        'name', 'image', 'description', 'category', 'release_date',
        'specific_price'
    ];

    /**
     *
     * @return string
     */
    public function _image()
    {
        // return 'storage/' . $this->image;
        return $this->storage()->url($this->image);
    }

    /**
     * Undocumented function
     *
     * @return mixed
     */
    private function storage()
    {
        return Storage::disk();
    }

    /**
     * @return string
     */
    public function _name()
    {
        return ucwords(Str::lower($this->name));
    }
    /**
     * @return string
     */
    public function nameSlug()
    {
        return Str::slug($this->name);
    }

    /**
     * @return string
     */
    public function adminRoute()
    {
        return route('admin.musics.album.profile.home', ['id' => $this->id]);
    }

    /**
     * @return string
     */
    public function route()
    {
        return route('album', ['id' => $this->id, 'name' => $this->nameSlug()]);
    }

    /**
     * @return integer
     */
    public function _price()
    {
        $price = 0;
        if ($this->specific_price) {
            $price = $this->specific_price;
        } else {
            foreach ($this->musics as $music) {
                $price += $music->_price();
            }
        }
        return $price;
    }

    /**
     * Undocumented function
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Undocumented function
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function musics()
    {
        return $this->hasMany('App\Models\Music');
    }

    /**
     * Undocumented function
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function downloads()
    {
        return $this->morphMany('App\Models\Download', 'downloadable');
    }

    /**
     * Undocumented function
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function likes()
    {
        return $this->morphMany('App\Models\Like', 'likeable');
    }

    /**
     * Undocumented function
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function playings()
    {
        return $this->morphMany('App\Models\Playing', 'playable');
    }

    /**
     * Undocumented function
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function orders()
    {
        return $this->morphMany('App\Models\Order', 'orderable');
    }
}
