<?php

namespace App\Models;

use App\Repository\GeneralPriceRepository;
use App\Searchable\FullTextSearch;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

// use Laravel\Scout\Searchable;
// use Devloops\LaravelTypesense\Interfaces\TypesenseSearch;

// implements TypesenseSearch

class Music extends Model
{

    // use Searchable;
    use FullTextSearch;

    /**
     * @var string
     */
    protected $table = 'musics';

    /**
     * The columns of the full text index
     */
    protected $searchable = [
        'title'
    ];
    /**
     * Undocumented variable
     *
     * @var array
     */
    protected $fillable = [
        'title', 'specific_price', 'release_date',
        'category', 'image', 'genre', 'private_file', 'public_file', 'user_id'
    ];

    /**
     *
     * @return string
     */
    public function _image()
    {
        if ($this->image === 'music.png' && $this->album) {
            $this->image = $this->album->image;
        }

        return ($this->image !== 'music.png' ? $this->storage()->url($this->image) : 'images/default/' . $this->image);
    }

    /**
     * Undocumented function
     *
     * @return mixed
     */
    private function storage()
    {
        return Storage::disk();
    }

    /**
     * @return string
     */
    public function _title()
    {
        return ucwords(Str::lower($this->title));
    }

    /**
     * @return string
     */
    public function titleSlug()
    {
        return Str::slug($this->title);
    }


    /**
     * @return string
     */
    public function adminRoute()
    {
        return route('admin.musics.music.profile.home', ['id' => $this->id]);
    }

    /**
     * @return string
     */
    public function adminPlayer()
    {
        // return route('admin.artist.play-artist-music', ['music_id' => $this->id]);
        return $this->storage()->url($this->public_file);
    }

    /**
     * @return string
     */
    public function route()
    {
        return route('station', ['id' => $this->id, 'title' => $this->titleSlug()]);
    }

    /**
     * @return integer
     */
    public function _price()
    {
        $price = 0;
        if ($this->specific_price) {
            $price = $this->specific_price;
        } else {
            $price = app()->call(GeneralPriceRepository::class . '@getActivePrice');
        }
        return $price;
    }

    /**
     * Undocumented function
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function album()
    {
        return $this->belongsTo('App\Models\Album');
    }
    /**
     * Undocumented function
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Undocumented function
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function playlists()
    {
        return $this->belongsTo('App\Models\Playlist');
    }

    /**
     * Undocumented function
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function orders()
    {
        return $this->morphMany('App\Models\Order', 'orderable');
    }

    /**
     * Undocumented function
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function downloads()
    {
        return $this->morphMany('App\Models\Download', 'downloadable');
    }

    /**
     * Undocumented function
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function playings()
    {
        return $this->morphMany('App\Models\Playing', 'playable');
    }

    /**
     * Undocumented function
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function likes()
    {
        return $this->morphMany('App\Models\Like', 'likeable');
    }
}
