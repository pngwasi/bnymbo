<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GeneralPrice extends Model
{
    /**
     * @var string
     */
    protected $table = 'general_price';

    /**
     * @var array
     */
    protected $fillable = ['price'];
}
