<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlaylistItem extends Model
{
    /**
     * @var string
     */
    protected $table = 'playlist_items';

    /**
     * @var array
     */
    protected $fillable = ['music_id'];

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function music()
    {
        return $this->belongsTo('App\Models\Music');
    }
}
