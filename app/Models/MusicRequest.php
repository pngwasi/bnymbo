<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MusicRequest extends Model
{
    /**
     * @var string
     */
    protected $table = 'music_requests';

    /**
     * fillable variable
     *
     * @var array
     */
    protected $fillable = ['email', 'media_stream', 'description'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
