<?php

namespace App\Models;

use App\Searchable\FullTextSearch;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;


class Playlist extends Model
{

    use FullTextSearch;

    /**
     * @var string
     */
    protected string $tables = 'playlists';
    /**
     * The columns of the full text index
     */
    protected $searchable = ['name'];

    /**
     * @var array
     */
    protected $fillable = ['name'];


    /**
     * @return string
     */
    public function _name()
    {
        return ucwords(Str::lower($this->name));
    }
    /**
     * @return string
     */
    public function nameSlug()
    {
        return Str::slug($this->name);
    }

    /**
     * @return string
     */
    public function adminRoute()
    {
        return route('admin.musics.playlist.profile.home', ['id' => $this->id]);
    }

    /**
     * @return string
     */
    public function route()
    {
        return route('playlist', ['id' => $this->id, 'name' => $this->nameSlug()]);
    }

    /**
     * @return string
     */
    public function _price()
    {
        $price = 0;
        foreach ($this->items as $music) {
            $price += $music->music->_price();
        }
        return $price;
    }
    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Undocumented function
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany('App\Models\PlaylistItem');
    }

    /**
     * Undocumented function
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function downloads()
    {
        return $this->morphMany('App\Models\Download', 'downloadable');
    }

    /**
     * Undocumented function
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function orders()
    {
        return $this->morphMany('App\Models\Order', 'orderable');
    }

    /**
     * Undocumented function
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function likes()
    {
        return $this->morphMany('App\Models\Like', 'likeable');
    }
}
