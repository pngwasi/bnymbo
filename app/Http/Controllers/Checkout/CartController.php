<?php

namespace App\Http\Controllers\Checkout;

use App\Http\Controllers\Controller;
use App\Statics\CartTable;
use Illuminate\Http\Request;

class CartController extends Controller
{

    /**
     * Undocumented variable
     *
     * @var CartTable
     */
    private $cartTable;

    /**
     * Undocumented function
     *
     * @param CartTable $cartTable
     */
    public function __construct(CartTable $cartTable)
    {
        $this->cartTable = $cartTable;
    }

    /**
     * Undocumented function
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('cart');
    }

    /**
     * Undocumented function
     *
     * @param mixed $item
     * @return string
     */
    private function itemKey($item): string
    {
        return "{$item->id}-{$item->section}";
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return mixed
     */
    private function items(Request $request)
    {
        return $request->session()->get($this->cartTable->cartSession, []);
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return array
     */
    public function addToCart(Request $req)
    {
        $req->validate([
            'id' => ['required'],
            'section' => ['required'],
            'price' =>  ['required', 'numeric', 'min:0.1']
        ]);
        $datas = $this->items($req);
        abort_if((count($datas) > 19), 400, trans('you get limite'));
        $key = $this->itemKey($req);
        if (!array_key_exists($key, $datas)) {
            $datas[$key] = $req->all();
            $req->session()->put($this->cartTable->cartSession, $datas);
        }
        return [
            'length' => count($datas),
            'item' => $req->all()
        ];
    }


    /**
     * Undocumented function
     *
     * @param Request $request
     * @return array
     */
    public function cartItems(Request $request)
    {
        $items = [];
        foreach ($this->items($request) as $k => $item) {
            $items[] = $this->cartTable->getItemBySection($item, $k);
        };
        return [
            'items' => $items,
            'coupon' => [
                'off' => 0,
                'amount' => 0,
                'code' => ''
            ]
        ];
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return array
     */
    public function deleteItemCart(Request $request)
    {
        $itemKey = $request->query('itemKey');
        abort_if(!$itemKey, 400);
        $items = $this->items($request);
        $datas = collect($items)->filter(fn ($v, $k) => $itemKey !== $k)->all();
        $request->session()->put($this->cartTable->cartSession, $datas);

        return [
            'length' => count($datas),
            'datas' => $datas,
            'removed' => $itemKey
        ];
    }
}
