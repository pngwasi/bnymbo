<?php

namespace App\Http\Controllers;

use App\Actions\LikeAction;
use App\Repository\GeneralPriceRepository;
use App\Repository\MusicRepository;
use App\Repository\PlaylistRepository;
use App\Statics\CartTable;
use App\Statics\CurrencyFormat;
use Illuminate\Http\Request;

class PlaylistController extends Controller
{

    use LikeAction;

    private PlaylistRepository $playlist;

    private MusicRepository $musics;

    /**
     * Undocumented function
     *
     * @param PlaylistRepository $playlist
     * @param MusicRepository $musics
     * @param GeneralPriceRepository $price
     */
    public function __construct(PlaylistRepository $playlist, MusicRepository $musics)
    {
        $this->playlist = $playlist;
        $this->musics = $musics;
    }

    /**
     * Undocumented function
     *
     * @param int $id
     * @param Request $request
     * @param CurrencyFormat $format
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($id, Request $request, CurrencyFormat $format, CartTable $cartTable)
    {
        $playlist = $this->playlist->findPlaylistOrFail($id);
        $playlist->with('user');
        $playlist->with('items');
        $musics = $playlist->items->map(fn ($item) => $item->music);

        $likesCount = $format->format($playlist->likes->count());
        $othersPlaylist = $this->playlist->queryLatestExceptPassed($playlist->id, 12);
        $auth = $request->user();
        $liked = false;
        if ($auth) {
            $like = $playlist->likes()->where('user_id', $auth->id)->first();
            $liked = !!$like;
        }
        $price = $playlist->_price();
        $cartDetails = [
            'id' => $id,
            'section' => $cartTable->playlist(),
            'price' => $price
        ];
        return view('playlist', compact(
            'playlist',
            'musics',
            'likesCount',
            'othersPlaylist',
            'price',
            'liked',
            'cartDetails'
        ));
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @param int $id
     * @param CurrencyFormat $format
     * @return mixed
     */
    public function likeAction(Request $request, $id, CurrencyFormat $format)
    {
        $playlist = $this->playlist->findPlaylistOrFail($id);
        return $this->Like($request, $playlist, $format);
    }
}
