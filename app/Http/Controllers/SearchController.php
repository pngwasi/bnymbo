<?php

namespace App\Http\Controllers;

use App\Repository\AlbumRepository;
use App\Repository\MusicRepository;
use App\Repository\NewsRepository;
use App\Repository\PlaylistRepository;
use App\Repository\UserRepository;
use Illuminate\Http\Request;

class SearchController extends Controller
{

    /**
     * @var UserRepository
     */
    private UserRepository $artiste;

    /**
     * @var MusicRepository
     */
    private MusicRepository $station;

    /**
     * @var AlbumRepository
     */
    private AlbumRepository $album;

    /**
     * @var PlaylistRepository
     */
    private PlaylistRepository $playlist;

    /**
     * @var NewsRepository
     */
    private NewsRepository $news;

    /**
     * @param UserRepository $artiste
     * @param MusicRepository $station
     * @param AlbumRepository $album
     * @param PlaylistRepository $playlist
     */
    public function __construct(NewsRepository $news, UserRepository $artiste, MusicRepository $station, AlbumRepository $album, PlaylistRepository $playlist)
    {
        $this->playlist = $playlist;
        $this->artiste = $artiste;
        $this->station = $station;
        $this->album = $album;
        $this->news = $news;
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $s = request('s', '~');
        $section = request('section');
        $album = $this->album->getAlbumsByFilter(12, $s, true);
        $station = $this->station->getMusicsByFilter(12, $s, true);
        $artiste = $this->artiste->getActiveArtistesFilter(12, $s, true);
        $playlist = $this->playlist->getPlaylistsByFilter(12, $s, true);
        $news = $this->news->getArticlesByFilter(12, $s, true);
        $total = [
            'station' => $station->total(),
            'album' => $album->total(),
            'artiste' => $artiste->total(),
            'playlist' => $playlist->total(),
            'news' => $news->total()
        ];
        $vars = compact('album', 'station', 'artiste', 'playlist', 'news');
        $result = key_exists($section, $vars) ?
            ['type' => $section, 'items' => $vars[$section]] :
            ['type' => 'station', 'items' => $vars['station']];
        return view('search', [
            'result' => (object) [
                'total' => (object) $total,
                'empty' => array_sum($total),
                'data' => (object) $result
            ]
        ]);
    }
}
