<?php

namespace App\Http\Controllers;

use App\Actions\LikeAction;
use App\Repository\MusicRepository;
use App\Statics\CartTable;
use App\Statics\CurrencyFormat;
use Illuminate\Http\Request;

class StationController extends Controller
{
    use LikeAction;

    private MusicRepository $musics;

    public function __construct(MusicRepository $musics)
    {
        $this->musics = $musics;
    }


    /**
     * Show the application dashboard.
     * @param int $id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request, $id, CurrencyFormat $format, CartTable $cartTable)
    {
        $music = $this->musics->findMusicOrFail($id);
        $moreMusics = $this->musics->getMoreMusicArtist($music);
        $similars = $this->musics->getSimilarMusics($music);
        $playingsCount = $format->format($music->playings->count());
        $likesCount = $format->format($music->likes->count());
        $downloadsCount = $format->format($music->downloads->count());
        $auth = $request->user();

        $liked = false;
        if ($auth) {
            $like = $music->likes()->where('user_id', $auth->id)->first();
            $liked = !!$like;
        }
        $price = $music->_price();
        $cartDetails = [
            'id' => $id,
            'section' =>  $cartTable->station(),
            'price' => $price
        ];
        return view('station', compact(
            'music',
            'moreMusics',
            'similars',
            'playingsCount',
            'likesCount',
            'price',
            'liked',
            'cartDetails',
            'downloadsCount'
        ));
    }

    /**
     * @param int $id
     */
    public function playingMusic($id)
    {
        $music = $this->musics->findMusicOrFail($id);
        $music->playings()->create(['played' => true]);
        return [true];
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @param int $id
     * @param CurrencyFormat $format
     * @return mixed
     */
    public function likeAction(Request $request, $id, CurrencyFormat $format)
    {
        $music = $this->musics->findMusicOrFail($id);
        return $this->Like($request, $music, $format);
    }
}
