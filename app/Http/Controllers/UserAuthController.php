<?php

namespace App\Http\Controllers;

use App\Actions\ImageCompression;
use App\Http\Requests\UserValidation;
use App\Repository\FollowerRepository;
use App\Statics\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;


class UserAuthController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'UserOnAuthPages', 'optimizeImages']);
    }

    private function response($datas = [])
    {
        $datas['user'] = Auth::user();
        return $datas;
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function profile()
    {
        return view('_user.auth.profile', $this->response());
    }

    /**
     * Undocumented function
     *
     * @param Request $user
     * @return mixed
     */
    private function _purchase(Request $request)
    {
        $user = $request->user();
        $purshases = $user->purchase()->latest()->paginate(12);
        return $purshases;
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function purchase(Request $request)
    {
        return view('_user.auth.purchase', $this->response(['user_items' => $this->_purchase($request)]));
    }

    /**
     * @param Request $request
     * @return  \Illuminate\Http\JsonResponse
     */
    public function purchaseTemplates(Request $request)
    {
        $instances = $this->_purchase($request);
        return response()->json([
            'nextPageUrl' => $instances->nextPageUrl(),
            'html' => view('layouts.items-purchase', ['items' => $instances])->toHtml()
        ]);
    }

    /**
     * @param Request $request
     * @param FollowerRepository $followRp
     * @return mixed
     */
    private function _following(Request $request, FollowerRepository $followRp)
    {
        $user = $request->user();
        $following = $followRp->queryUserFollowing($user->id)->latest()->paginate(12);
        $following->withPath(route('user.followingTemplates', ['id' => $user->id, 'name' => Str::slug($user->name)]));
        return $following;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function following(Request $request, FollowerRepository $followRp)
    {
        return view('_user.auth.following', $this->response(['user_items' => $this->_following($request, $followRp)]));
    }

    /**
     * @param Request $request
     * @return  \Illuminate\Http\JsonResponse
     */
    public function followingTemplates(Request $request, FollowerRepository $followRp)
    {
        $instances = $this->_following($request, $followRp);
        return response()->json([
            'nextPageUrl' => $instances->nextPageUrl(),
            'html' => view('layouts.items-following', ['items' => $instances])->toHtml()
        ]);
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function upload()
    {
        $response = $this->response();
        $response['countRequest'] = $response['user']->artisteRequests->count();
        return view('_user.auth.upload', $response);
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function artisteRequest(Request $request)
    {
        $request->validate([
            'email' => ['required', 'string', 'email', 'max:255'],
            'phone' => ['required', 'regex:/^[0-9\-\(\)\/\+\s]*$/'],
            'picture_url' => ['nullable', 'string', 'max:255', 'min:2', 'url', 'string'],
            'media_stream' => ['required', 'string', 'max:255', 'min:2', 'url', 'string'],
            'description' => ['required', 'string', 'max:255', 'min:10'],
        ]);
        $user = $request->user();
        if ($user->artisteRequests->count() > 3) {
            return redirect(request('url'))
                ->with('fail', trans('You have reached the maximum of your requests'));
        }
        $user->artisteRequests()->create([
            'email' => $request->email,
            'phone' => $request->phone,
            'picture_url' => $request->picture_url,
            'media_stream' => $request->media_stream,
            'description' => $request->description
        ]);
        return redirect(request('url'))->with('send', trans('Your request has been sent successfully'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function musicRequest(Request $request)
    {
        $request->validate([
            'email' => ['required', 'string', 'email', 'max:255'],
            'media_stream' => ['required', 'string', 'max:255', 'min:2', 'url', 'string'],
            'description' => ['required', 'string', 'max:255', 'min:10'],
        ]);
        $user = $request->user();
        $user->musicRequests()->create([
            'email' => $request->email,
            'media_stream' => $request->media_stream,
            'description' => $request->description
        ]);
        return redirect(request('url'))->with('send', trans('Your request has been sent successfully'));
    }

    /**
     * Undocumented function
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function profileModify(UserValidation $request, ImageCompression $compress)
    {
        $user = $request->user();
        $user->fill([
            'first_name' => $request->firstname,
            'last_name' => $request->lastname,
            'phone' => $request->phone,
            'name' => $request->name,
            'email' => $request->email,
            'description' => ($request->description ?: null),
            'website' => ($request->website ?: null),
            'facebook' => ($request->facebook ?: null),
            'twitter' => ($request->twitter ?: null),
            'youtube' => ($request->youtube ?: null),
            'instagram' => ($request->instagram ?: null)
        ]);

        if ($request->filled('new_password')) {
            $user->fill(['password' => Hash::make($request->new_password)]);
        }
        if ($request->hasFile('image')) {
            $path = File::uniqidPathArtist($user->id);
            $public = Storage::disk();
            if ($public->exists($user->image)) {
                $public->delete($user->image);
            }
            $file = $request->file('image');
            $user->image = $file->storePublicly($path);
            $compress->imgToSmall($file, $path, $user->id);
        }
        $user->save();
        return redirect()->intended($request->fullUrl())->with('success', trans('Saved'));
    }
}
