<?php

namespace App\Http\Controllers;

use App\Repository\MusicRepository;
use App\Repository\NewsRepository;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    /**
     * @var MusicRepository
     */
    private MusicRepository $musics;

    /**
     * @var NewsRepository
     */
    private NewsRepository $news;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(MusicRepository $musics, NewsRepository $news)
    {
        $this->musics = $musics;
        $this->news = $news;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $mundane = $this->musics->queryLastMundaneMUsic();
        $christian = $this->musics->queryLastChristianMUsic();
        $popular = $this->musics->queryTopLastPopular(3);
        $news = $this->news->getLastArticles();
        $user = $request->user();
        return view('home', [
            'musics' => compact('mundane', 'christian', 'popular'),
            'news' => $news,
            'artiste_url' => $request->user() ? route('user.upload', [
                'id' => $user->id,
                'name' => $user->nameSlug()
            ]) : route('register')
        ]);
    }
}
