<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repository\NewsRepository;
use App\Services\Og\OpenGraph;
use App\Statics\File;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class NewsController extends Controller
{
    /**
     * @var NewsRepository
     */
    private NewsRepository $news;

    /**
     * @var array
     */
    private array $status = [
        'actif' => true,
        'inactif' => false
    ];

    /**
     * @param NewsRepository $news
     */
    public function __construct(NewsRepository $news)
    {
        $this->news = $news;
        $this->middleware(['optimizeImages']);
    }

    /**
     * @return bool|string
     */
    private function status()
    {
        return array_key_exists(request('f'), $this->status) ? $this->status[request('f')] : '';
    }

    /**
     * Show the application article.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function newArticle()
    {
        return view('admin.news.new-article', [
            'categories' => $this->news->categories()
        ]);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function addArticle(Request $request)
    {
        $request->validate([
            'title' => ['required', 'string', 'min:5', 'max:255', 'unique:blogs'],
            'category' => ['nullable', 'numeric', 'min:1'],
            'image' => File::IMAGE_RULES,
            'json' => ['required', 'json']
        ]);
        $article = $this->news->news->newQuery()->create([
            'title' => $request->get('title'),
            'slug' => Str::slug($request->get('title')),
            'json' => $request->get('json'),
            'blog_category_id' => $request->category
        ]);
        if ($request->hasFile('image')) {
            $path = $request->file('image')->storePublicly(File::BLOG_IMG_PATH . "/{$article->id}");
            $article->image = $path;
            $article->save();
        }
        return response()->json([
            'title' => $request->get('title'),
            'json' => json_decode($request->get('json'), true)
        ], 201);
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function articles()
    {
        $articles = $this->news->getArticlesByFilter(12, request('q', ''), $this->status());
        return view('admin.news.articles', compact('articles'));
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function modifyArticle()
    {
        $redirect = request('redirect');
        $id = request('id');
        $article = $this->news->findAnyArticleOrFail($id);
        $article->active = !$article->active;
        $article->save();
        return redirect($redirect);
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function categories()
    {
        return view('admin.news.categories', [
            'categories' => $this->news->categories()
        ]);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function addCategory(Request $request)
    {
        $request->validate([
            'name' => 'required|min:2'
        ]);
        $this->news->category->newQuery()->create([
            'name' => $request->name
        ]);
        return [$request->name];
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delCategory()
    {
        $this->news->category->newQuery()
            ->findOrFail(request('id'))
            ->delete();
        return redirect(request('redirect'));
    }

    /**
     * @param OpenGraph $og
     * @return array
     */
    public function editorLinkData(OpenGraph $og)
    {
        $graph = $og::fetch(urldecode(request('url')));
        if (!$graph) {
            return [
                'success' => 0,
                'message' => trans('Invalid Link')
            ];
        }
        return [
            'success' => 1,
            'meta' =>  [
                "title" => $graph->title ?: '',
                "description" => $graph->description ?: '',
                "image" => [
                    "url" => $graph->image ?: ''
                ]
            ]
        ];
    }
}
