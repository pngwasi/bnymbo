<?php

namespace App\Http\Controllers\Admin\Client;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Repository\UserRepository;
use App\Statics\File;
use App\Statics\Statics;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ClientController extends Controller
{
    /**
     * @var UserRepository
     */
    private UserRepository $client;

    /**
     * @param UserRepository $client
     */
    public function __construct(UserRepository $client)
    {
        $this->client = $client;
    }


    /**
     * @param object|null $client
     * @param array $merge
     * 
     * @return array
     */
    private function apiArrayResultClient(?object $client, array $merge = [])
    {
        return array_merge([
            'id' => $client->id,
            'artiste' => boolval($client->artiste),
            'name' => $client->_name(),
            'email' => $client->email,
            'image' => asset($client->_image()),
            'route' => $client->adminRoute()
        ], $merge);
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.client');
    }

    /**
     * @param mixed $datas
     * @param mixed $route
     * 
     * @return array
     */
    private function paginator($datas, $route)
    {
        $datas->withPath(route($route, [], false));
        $result = [];
        foreach ($datas as $client) {
            $result[] = $this->apiArrayResultClient($client);
        }
        $datas['data'] = $result;
        return $datas;
    }

    /**
     * @return array
     */
    public function all()
    {
        $clients = $this->client->all(4);
        return $this->paginator($clients, 'admin.client.all');
    }

    /**
     * @return array
     */
    public function search(Request $request)
    {
        $clients = $this->client->getArtistesFilter(12, $request->query('q'), true);
        return $this->paginator($clients, 'admin.client.search');
    }

    /**
     * @return object|null
     */
    public function getClient()
    {
        $query = request('client');
        return $this->client->user->newQuery()->findOrFail($query);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function details()
    {
        $client = $this->getClient();
        return [
            'details' => [
                'likes' => $client->likes->count(),
                'following' => $client->following->count(),
                'downloads' => $client->downloads->count(),
                'purchase' => $client->purchase->sum('total'),
            ],
            'profile' => new UserResource($client)
        ];
    }

    private function paginate($relation)
    {
        return $this->getClient()->$relation()->latest()->simplePaginate(12);
    }

    /**
     * @return array
     */
    public function following()
    {
        $datas = $this->paginate('following');
        $datas->withPath(route('admin.client.following', [], false));
        $result = [];
        foreach ($datas as $client) {
            $result[] = $this->apiArrayResultClient($client->user, [
                'created_at' => $client->created_at->toDateTimeString()
            ]);
        }
        $datas['data'] = $result;
        return $datas;
    }

    /**
     * @return array
     */
    public function followers()
    {
        $datas = $this->paginate('followers');
        $datas->withPath(route('admin.client.followers', [], false));
        $result = [];
        foreach ($datas as $client) {
            $result[] = $this->apiArrayResultClient($client->follower, [
                'created_at' => $client->created_at->toDateTimeString()
            ]);
        }
        $datas['data'] = $result;
        return $datas;
    }

    /**
     * @return array
     */
    public function likes(Statics $statics)
    {
        $datas = $this->paginate('likes');
        $datas->withPath(route('admin.client.likes', [], false));
        $result = [];
        foreach ($datas as $like) {
            $result[] = $statics->morthable($like, $like->likeable);
        }
        $datas['data'] = $result;
        return $datas;
    }

    /**
     * Undocumented function
     *
     * @return array
     */
    public function purchase(Statics $statics)
    {
        $datas =  $this->paginate('purchase');
        $result = [];
        foreach ($datas as $order) {
            $result[] = [
                'product' => array_merge($statics->morthable($order, $order->orderable), [
                    'price' => $order->orderable->_price()
                ]),
                'id' => $order->id,
                'service_name' => $order->service_name,
                'total_paid' => $order->total_paid,
                'discounts' => $order->discounts,
                'total' => $order->total,
                'created_at' => $order->created_at->toDateTimeString()
            ];
        }
        $datas['data'] = $result;
        return $datas;
    }

    /**
     * @return array
     */
    public function toArtiste(Request $request)
    {
        $request->validate([
            'image' => File::IMAGE_RULES,
        ]);
        $client = $this->getClient();
        $path = File::uniqidPathArtist($client->id);
        $public = Storage::disk('public');
        if ($public->exists($client->image)) {
            $public->delete($client->image);
        }
        $client->image = $request->file('image')->storePublicly($path);
        $client->artiste = true;
        $client->save();
        $client->refresh();
        return $this->apiArrayResultClient($client);
    }
}
