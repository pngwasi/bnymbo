<?php

namespace App\Http\Controllers\Admin\Album;

use App\Events\ExtractionMp3File;
use App\Http\Controllers\Controller;
use App\Repository\AlbumRepository;
use App\Repository\MusicRepository;
use App\Statics\File;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class AlbumController extends Controller
{
    /**
     * @var AlbumRepository
     */
    private AlbumRepository $album;

    /**
     * @var MusicRepository
     */
    private MusicRepository $musics;

    /**
     * @param AlbumRepository $album
     * @param MusicRepository $music
     */
    public function __construct(AlbumRepository $album, MusicRepository $music)
    {
        $this->album = $album;
        $this->musics = $music;
        $this->middleware(['optimizeImages']);
    }

    /**
     * @param object $music
     * @param array $merge
     * 
     * @return array
     */
    private function apiArrayResultMusic(object $music, array $merge = [])
    {
        return array_merge([
            'id' => $music->id,
            'title' => $music->_title(),
            'url' => $music->adminPlayer(),
            'url_profile' => $music->adminRoute(),
            'image' => asset($music->_image())
        ], $merge);
    }

    /**
     * Undocumented function
     * @param int $id
     * @return array
     */
    public function getAlbumMusicsList($id)
    {
        $album = $this->album->findAlbumOrFail($id);
        $musics = $album->musics()->latest()->get();
        $result = [];
        foreach ($musics as $music) {
            $result[] = $this->apiArrayResultMusic($music, [
                'likes' => $album->musics->count(),
                'downloads' => $album->downloads->count(),
            ]);
        }
        return $result;
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function newAlbumMusics(Request $request, $id)
    {
        $album = $this->album->findAlbumOrFail($id);
        $user = $album->user;

        $disk = config('filesystems.default');

        $request->validate([
            'music_file' => 'required|max:20000|file',
            'title' => 'required|max:255|min:1',
            'music_img' => ['sometimes', ...File::IMAGE_RULES],
            'unitPrice' => 'nullable|numeric',
            'genres' => 'nullable|max:255|min:2',
            'category' => 'required|max:255'
        ]);

        $title = ucwords(Str::lower($request->get('title')));

        $existName = $album->musics()->where('title', $title)->first();

        if ($existName) {
            return abort(409, trans('Title input already exist'));
        }

        $musicInstance = $this->musics->createInstanceNewMusic(
            $title,
            $request->get('unitPrice'),
            now(),
            $request->get('category'),
            $request->get('genres'),
            $user->id
        );

        $music = $album->musics()->save($musicInstance);

        $userMusicPath = File::MUSICS_MSC_PATH . '/' . $user->id . "/" . $music->id;

        $local = in_array($disk, ['public', 'local']);

        $path = $request->file('music_file')->storePubliclyAs($userMusicPath, Str::random(40) . '.mp3', $local ? 'local' : []);

        $music->public_file = $path;
        $music->private_file = $userMusicPath . '/' . File::SHORT_MUSIC_FILENAME;
        // dispache event for extration mp3
        try {
            event(new ExtractionMp3File($path, $userMusicPath));
        } catch (\Throwable $th) {
            //throw $th;
        }
        //----------------
        if ($request->hasFile('music_img')) {
            $path = File::MUSIC_IMG_PATH . '/' . $user->id . "/" . $music->id;
            $img = $request->file('music_img')->storePublicly($path);
            $music->image = $img;
        }

        $music->save();
        $music->refresh();

        return [
            $this->apiArrayResultMusic($music, [
                'likes' => 0,
                'downloads' => 0,
            ])
        ];
    }
}
