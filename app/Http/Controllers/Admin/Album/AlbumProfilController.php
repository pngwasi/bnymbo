<?php

namespace App\Http\Controllers\Admin\Album;

use App\Http\Controllers\Controller;
use App\Repository\AlbumRepository;
use App\Statics\Category;
use App\Statics\Genres;
use Illuminate\Http\Request;

class AlbumProfilController extends Controller
{
    /**
     * @var AlbumRepository
     */
    private  AlbumRepository $album;

    /**
     * @param AlbumRepository $album
     */
    public function __construct(AlbumRepository $album)
    {
        $this->album = $album;
    }

    /**
     * @param string|null $view
     * @param array|null $datas
     * 
     * @return \Illuminate\Contracts\Support\Renderable
     */
    private function view(?string $view, ?array $datas = [])
    {
        $album = $this->album->findAlbumOrFail(request()->id);
        return view("admin.music-album.profile-album." . $view, array_merge($datas, compact('album')));
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function home(Category $category, Genres $genres)
    {
        return $this->view('music', [
            'genres' => $genres->genres,
            'categorys' => $category->types()
        ]);
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function report()
    {
        return $this->view('report');
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function settings()
    {
        return $this->view('settings');
    }
}
