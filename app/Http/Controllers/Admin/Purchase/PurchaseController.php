<?php

namespace App\Http\Controllers\Admin\Purchase;

use App\Http\Controllers\Controller;
use App\Repository\OrderRepository;
use App\Statics\Statics;
use Illuminate\Http\Request;

class PurchaseController extends Controller
{
    /**
     * @var OrderRepository
     */
    private OrderRepository $order;

    /**
     * @param OrderRepository $order
     */
    public function __construct(OrderRepository $order)
    {
        $this->order = $order;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Statics $statics)
    {
        $datas =  $this->order->order->newQuery()->latest()->paginate(12);
        $result = [];
        foreach ($datas as $order) {
            $result[] = (object) [
                'product' => (object) array_merge($statics->morthable($order, $order->orderable), [
                    'price' => $order->orderable->_price()
                ]),
                'id' => $order->id,
                'service_name' => $order->service_name,
                'total_paid' => $order->total_paid,
                'discounts' => $order->discounts,
                'total' => $order->total,
                'created_at' => $order->created_at->toDateTimeString()
            ];
        }
        return view('admin.purchase.purchases', [
            'datas' => $datas,
            'result' => (object) $result
        ]);
    }

}
