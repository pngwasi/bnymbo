<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Auth\LoginController;
use Illuminate\Support\Facades\Auth;

class AdminLoginController extends LoginController
{
    /**
     *  @var string
     */
    protected string $validateTable = 'admins';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $home = route('admin.home', [], false);
        $this->redirectTo = $home;
        $this->middleware('guest:admin,'.$home)->except('logout');
    }

    /**
     * Undocumented function
     *
     * @return mixed
     */
    protected function guard()
    {
        return Auth::guard('admin');
    }

    /**
     * @return \Illuminate\View\View
     */
    public function showLoginForm()
    {
        return view('admin.login');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function loggedOut(\Illuminate\Http\Request $request)
    {
        return redirect(route('admin.login'));
    }

}
