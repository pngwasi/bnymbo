<?php

namespace App\Http\Controllers\Admin\Playlist;

use App\Http\Controllers\Controller;
use App\Repository\PlaylistRepository;
use Illuminate\Http\Request;

class PlaylistProfilController extends Controller
{
    /**
     * @var PlaylistRepository
     */
    private PlaylistRepository $playlist;

    /**
     * @param PlaylistRepository $playlist
     */
    public function __construct(PlaylistRepository $playlist)
    {
        $this->playlist = $playlist;
    }
    /**
     * @param string|null $view
     * @param array|null $datas
     * 
     * @return \Illuminate\Contracts\Support\Renderable
     */
    private function view(?string $view, ?array $datas = [])
    {
        $playlist = $this->playlist->findPlaylistOrFail(request()->id);
        return view("admin.music-album.profile-playlist." . $view, array_merge($datas, compact('playlist')));
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function home()
    {
        return $this->view('home');
    }

    /**
     * @param object $music
     * @param array $merge
     * 
     * @return array
     */
    private function apiArrayResultMusic(object $music, array $merge = [])
    {
        return array_merge([
            'id' => $music->id,
            'title' => $music->_title(),
            'url' => $music->adminPlayer(),
            'url_profile' => $music->adminRoute(),
            'image' => asset($music->_image())
        ], $merge);
    }

    /**
     * @param int $id
     * 
     * @return array
     */
    public function getPlaylistItems($id)
    {
        $playlist = $this->playlist->findPlaylistOrFail($id);
        return $playlist->items->map(function ($item) {
            $music = $item->music;
            return $this->apiArrayResultMusic($music);
        })->all();
    }

    /**
     * @param int $id
     * @param Request $request
     * 
     * @return array
     */
    public function addToPlaylist($id, Request $request)
    {
        $music_id = $request->query('music_id');
        abort_if(!$music_id, 400, trans('Invalid datas'));
        $playlist = $this->playlist->findPlaylistOrFail($id);
        $music = $playlist->items()->firstOrCreate(['music_id' => $music_id]);
        return $music->wasRecentlyCreated ? [$this->apiArrayResultMusic($music->music)] : [];
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function report()
    {
        return $this->view('report');
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function settings()
    {
        return $this->view('settings');
    }
}
