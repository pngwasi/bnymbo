<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Album;
use App\Models\Download;
use App\Models\Music;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class DashboardController extends Controller
{
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.dash', [
            'musics' => Music::count(),
            'artistes' => User::query()->where('artiste', true)->count(),
            'clients' => User::query()->where('artiste', false)->count(),
            'downloads' => Download::count()
        ]);
    }

    /**
     * @param Request $request
     */
    public function adminProfile(Request $request)
    {
        $request->validate([
            'current_password' => ['required', 'string', 'max:255', 'password:api-admin'],
            'password' => ['required', 'string', 'max:255', 'min:6'],
        ]);
        $admin = $request->user();
        $admin->password = Hash::make($request->password);
        $admin->save();
        return [
            'message' => trans('Saved')
        ];
    }
}
