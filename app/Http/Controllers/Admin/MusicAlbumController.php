<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repository\AlbumRepository;
use App\Repository\GeneralPriceRepository;
use App\Repository\MusicRepository;
use App\Repository\PlaylistRepository;
use App\Statics\Category;
use App\Statics\File;
use App\Statics\Genres;
use Illuminate\Http\Request;

class MusicAlbumController extends Controller
{

    /**
     * @var AlbumRepository
     */
    private AlbumRepository $album;

    /**
     * @var MusicRepository
     */
    private MusicRepository $music;

    /**
     * @var PlaylistRepository
     */
    private PlaylistRepository $playlist;

    /**
     * @var array
     */
    private array $status = [
        'actif' => true,
        'inactif' => false
    ];


    /**
     * @return bool|string
     */
    private function status()
    {
        return array_key_exists(request('f'), $this->status) ? $this->status[request('f')] : '';
    }

    /**
     * @param AlbumRepository $album
     * @param MusicRepository $music
     * @param PlaylistRepository $playlist
     */
    public function __construct(AlbumRepository $album, MusicRepository $music, PlaylistRepository $playlist)
    {
        $this->album = $album;
        $this->music = $music;
        $this->playlist = $playlist;
    }

    /**
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function musics()
    {
        $musics = $this->music->getMusicsByFilter(12, request('q', ''), $this->status());
        return view('admin.music-album.musics', compact('musics'));
    }

    /**
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function albums()
    {
        $albums = $this->album->getAlbumsByFilter(12, request('q', ''), $this->status());
        return view('admin.music-album.albums', compact('albums'));
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function playlists()
    {
        $playlists = $this->playlist->getPlaylistsByFilter(12, request('q', ''), $this->status());
        return view('admin.music-album.playlists', compact('playlists'));
    }
    /**
     * Show the application.
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function musicRequest()
    {
        return view('admin.music-album.music-request');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function modifyMusic()
    {
        $redirect = request('redirect');
        $id = request('id');
        $music = $this->music->findMusicOrFail($id);
        $music->active = !$music->active;
        $music->save();
        return redirect($redirect);
    }

    /**
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function modifyAlbum()
    {
        $redirect = request('redirect');
        $id = request('id');
        $album = $this->album->findAlbumOrFail($id);
        $album->active = !$album->active;
        $album->save();
        return redirect($redirect);
    }

    /**
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function modifyPlaylist()
    {
        $redirect = request('redirect');
        $id = request('id');
        $playlist = $this->playlist->findPlaylistOrFail($id);
        $playlist->active = !$playlist->active;
        $playlist->save();
        return redirect($redirect);
    }

    /**
     * @param Request $request
     * @param GeneralPriceRepository $rp
     * 
     * @return array
     */
    public function putGeneralPrice(Request $request, GeneralPriceRepository $rp)
    {
        $request->validate([
            'price' => ['required', 'numeric', 'min:0.1']
        ]);
        $price = $request->get('price');
        $rp->setActivePrice($price);
        return ['message' => trans('Saved'), 'price' => $price];
    }
}
