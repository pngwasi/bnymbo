<?php

namespace App\Http\Controllers\Admin\Artist;

use App\Http\Controllers\Controller;
use App\Mail\PasswordGenerate;
use App\Repository\AlbumRepository;
use App\Repository\PlaylistRepository;
use App\Repository\UserRepository;
use App\Statics\File;
use Illuminate\Hashing\HashManager;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailer;

class ArtistController extends Controller
{
    /**
     * Undocumented variable
     *
     * @var UserRepository $users
     */
    private UserRepository $users;

    /**
     * Undocumented variable
     *
     * @var AlbumRepository
     */
    public AlbumRepository $album;

    /**
     * @var PlaylistRepository
     */
    public PlaylistRepository $playlist;

    /**
     * @param UserRepository $user
     * @param AlbumRepository $album
     * @param PlaylistRepository $playlist
     */
    public function __construct(UserRepository $user, AlbumRepository $album, PlaylistRepository $playlist)
    {
        $this->users = $user;
        $this->album = $album;
        $this->playlist = $playlist;
        $this->middleware(['optimizeImages']);
    }


    /**
     * @param object $music
     * @param array $merge
     * 
     * @return array
     */
    private function apiArrayResultAlbum(object $album, array $merge = [])
    {
        return array_merge([
            'album_name' => $album->_name(),
            'album_id' => $album->id,
            'album_image' => asset($album->_image()),
            'url' => $album->adminRoute(),
            'musics' => $album->musics->count(),
            'downloads' => $album->downloads->count(),
        ], $merge);
    }

    /**
     * newAstist function
     *
     * @param Request $request
     * @return array
     */
    public function newArtist(Request $request)
    {
        $request->validate([
            'image' => File::IMAGE_RULES,
            'email' => 'required|email:rfc|unique:users|max:255',
            'username' => 'required|max:255'
        ]);
        $artist = $this->users->createArtist(
            $request->email,
            $request->username
        );
        $path = $request->file('image')->storePublicly(File::uniqidPathArtist($artist->id));
        if ($path) $artist->image = $path;
        $artist->save();
        return response()->json([
            'redirect' => route('admin.artist.profile', ['id' => $artist->id], false)
        ], 201);
    }



    /**
     * Undocumented function
     * @param int $id
     * @return array
     */
    public function getAlbumArtist($id)
    {
        $result = [];
        $user = $this->users->findArtistOrFail($id);
        foreach ($user->albums()->latest()->cursor() as $album) {
            $result[] = $this->apiArrayResultAlbum($album);
        }
        return $result;
    }

    /**
     * Undocumented function
     *
     * @param [type] $id
     * @param Request $request
     * @return array
     */
    public function createAlbumArtist($id, Request $request)
    {
        $user = $this->users->findArtistOrFail($id);
        $request->validate([
            'title' => 'required|max:255',
            'category' => 'required|max:255',
            'image' => File::IMAGE_RULES,
            'unit_price' => 'nullable|numeric|min:0.1',
            'description' => 'required|max:255',
            'release_date' => 'required|date|max:255'
        ]);
        $albumModel = $this->album->createAlbum(
            $request->title,
            $request->description,
            $request->category,
            $request->release_date,
            $request->unit_price ?: 0.00
        );
        $album = $user->albums()->save($albumModel);
        $path = $request->file('image')->storePublicly(File::ALBUM_IMG_PATH . '/' . $user->id);
        $album->image = $path;
        $album->save();
        $album->refresh();

        return [$this->apiArrayResultAlbum($album)];
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function newPlaylist(Request $request, $id)
    {
        $user = $this->users->findArtistOrFail($id);
        $request->validate([
            'name' => 'required|max:255|min:2',
        ]);
        $playlist = $this->playlist->createInstanceNewPlayList($request->name);
        $pl = $user->playlists()->save($playlist);

        return [[
            'url' => $pl->adminRoute(),
            'musics' => 0,
            'name' => ucfirst($pl->name)
        ]];
    }

    /**
     * Undocumented function
     *
     * @param int $id
     * @return array
     */
    public function getPlaylists($id)
    {
        $user = $this->users->findArtistOrFail($id);
        $result = [];
        foreach ($user->playlists()->latest()->cursor() as $playlist) {
            $result[] = [
                'name' => $playlist->_name(),
                'url' => $playlist->adminRoute(),
                'musics' => $playlist->items->count(),
            ];
        }
        return $result;
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function changeProfilPrimary(Request $request, $id)
    {
        $user = $this->users->findArtistOrFail($id);
        $datas = $request->validate([
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255',
            'email' => 'required|email:rfc|max:255',
            'username' => 'required|max:255',
            'description' => 'nullable|max:255|min:2'
        ]);
        $user->update([
            'name' => $datas['username'],
            'first_name' => $datas['firstname'],
            'email' => $datas['email'],
            'last_name' => $datas['lastname'],
            'description' => $datas['description']
        ]);
        return ['message' => trans('Saved')];
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function changeProfilSecondary(Request $request, $id)
    {
        $user = $this->users->findArtistOrFail($id);
        $datas = $request->validate([
            'website' => 'nullable|max:255|min:2|url',
            'facebook' => 'nullable|max:255|min:2|url',
            'twitter' => 'nullable|max:255|min:2|url',
            'youtube' => 'nullable|max:255|min:2|url',
            'instagram' => 'nullable|max:255|min:2|url'
        ]);
        $user->fill([
            'website' => $datas['website'],
            'facebook' => $datas['facebook'],
            'twitter' => $datas['twitter'],
            'youtube' => $datas['youtube'],
            'instagram' => $datas['instagram']
        ])->save();
        return ['message' => trans('Saved')];
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function generatePassword($id, Mailer $mail, HashManager $hash)
    {
        $user = $this->users->findArtistOrFail($id);
        $password = substr(str_shuffle(str_repeat('1234567890', 6)), 0, 6);
        $when = now()->addSeconds(5);
        $mail->to($user)->later($when, new PasswordGenerate($user, $password));
        $user->fill([
            'password' => $hash->make($password)
        ])->save();
        return ['message' => trans('Sent')];
    }
}
