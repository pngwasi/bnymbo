<?php

namespace App\Http\Controllers\Admin\Artist;

use App\Http\Controllers\Controller;
use App\Repository\UserRepository;
use Illuminate\Http\Request;

class HomeArtistController extends Controller
{
    /**
     * @var UserRepository
     */
    private UserRepository $user;

    /**
     * @param UserRepository $user
     */
    public function __construct(UserRepository $user)
    {
        $this->user = $user;
    }

    /**
     * Show the application.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function recent()
    {
        $artistes = $this->user->getArtistesFilter(12, request('q', ''));
        return view('admin.artist.recent', compact('artistes'));
    }


    /**
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    public function modifyArtist()
    {
        $redirect = request('redirect');
        $id = request('artiste');
        $artiste = $this->user->findArtistOrFail($id);
        $artiste->active = !$artiste->active;
        $artiste->save();
        return redirect($redirect);
    }

    /**
     * Show the application.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function status()
    {
        $artistes = $this->user->getDisabledArtistes();
        return view('admin.artist.status', compact('artistes'));
    }

    /**
     * Show the application.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function request()
    {
        return view('admin.artist.request');
    }
}
