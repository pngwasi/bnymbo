<?php

namespace App\Http\Controllers\Admin\Artist;

use App\Http\Controllers\Controller;
use App\Models\Music;
use App\Repository\UserRepository;
use App\Statics\Category;
use App\Statics\Genres;
use App\User;
use Closure;
use Illuminate\Http\Request;

class ArtistProfilController extends Controller
{

    /**
     * Undocumented variable
     *
     * @var UserRepository $users
     */
    private UserRepository $users;


    /**
     * @param UserRepository $users
     */
    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function traffic($id)
    {
        $user = User::query()->find($id);
        $musics = $user->musics->count();
        $albums = $user->albums->count();
        $playlists = $user->playlists->count();
        $orders = $user->orders->sum('total');
        return view('admin.artist.profile.traffic', compact('musics', 'albums', 'playlists', 'orders'));
    }

    /**
     * @param Category $category
     * @param Genres $genres
     * 
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function music(Category $category, Genres $genres)
    {
        return view('admin.artist.profile.music', [
            'genres' => $genres->genres,
            'categorys' => $category->types()
        ]);
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function profile()
    {
        return view('admin.artist.profile.profile',);
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function report()
    {
        return view('admin.artist.profile.report');
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function orders()
    {
        return view('admin.artist.profile.orders');
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function settings()
    {
        return view('admin.artist.profile.settings');
    }
}
