<?php

namespace App\Http\Controllers\Admin\Artist;

use App\Repository\AlbumRepository;
use App\Repository\PlaylistRepository;
use App\Repository\UserRepository;

use App\Http\Controllers\Controller;
use App\Repository\OrderRepository;
use Illuminate\Http\Request;

class ArtistRepportController extends Controller
{
    /**
     * Undocumented variable
     *
     * @var UserRepository $users
     */
    private UserRepository $users;

    /**
     * Undocumented variable
     *
     * @var AlbumRepository
     */
    public AlbumRepository $album;

    /**
     * @var PlaylistRepository
     */
    public PlaylistRepository $playlist;

    public function __construct(UserRepository $user, AlbumRepository $album, PlaylistRepository $playlist)
    {
        $this->users = $user;
        $this->album = $album;
        $this->playlist = $playlist;
    }

    /**
     * Undocumented function
     * @param int $id
     * @return array
     */
    public function activities($id, OrderRepository $rp)
    {
        $result = [];
        $orders = $rp->getOrderActivityArtist($id);
        foreach ($orders as $order) {
            $customer = $order->customer;
            $orderable = null;
            switch ($order->orderable->getMorphClass()) {
                case 'App\Models\Music':
                    $orderable = trans('Music');
                    break;
                case 'App\Models\Album':
                    $orderable = trans('Album');
                    break;
                case 'App\Models\Playlist':
                    $orderable = trans('Playlist');
                    break;
                default:
                    break;
            }
            if (!is_null($orderable)) {
                $result[] = [
                    'name' => $customer->name,
                    'email' => $customer->email,
                    'orderable' => $orderable,
                    'service' => $order->service->name,
                    'date' => $order->created_at
                ];
            }
        }
        return $result;
    }

    /**
     * Undocumented function
     *
     * @param int $id
     * @return array
     */
    public function followers($id)
    {
        $user = $this->users->findArtistOrFail($id);
        $result = [];
        foreach ($user->followers as $follower) {
            $result[] = [
                'name' => $follower->name,
                'email' => $follower->email,
                'url' => route('admin.' . $follower->id ? 'artist' : 'client' . '.profile', ['id' => $follower->id], false),
                'date' => $follower->created_at
            ];
        }
        return $result;
    }
}
