<?php

namespace App\Http\Controllers\Admin\Musics;

use App\Http\Controllers\Controller;
use App\Repository\MusicRepository;
use Illuminate\Http\Request;

class MusicProfilController extends Controller
{

    /**
     * @var MusicRepository
     */
    private MusicRepository $music;

    /**
     * @param MusicRepository $music
     */
    public function __construct(MusicRepository $music)
    {
        $this->music = $music;
    }

    /**
     * @param string|null $view
     * @param array|null $datas
     * 
     * @return \Illuminate\Contracts\Support\Renderable
     */
    private function view(?string $view, ?array $datas = [])
    {
        $music = $this->music->findMusicOrFail(request()->id);
        return view("admin.music-album.profile-music." . $view, array_merge($datas, compact('music')));
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function home()
    {
        return $this->view('home');
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function settings()
    {
        return $this->view('settings');
    }
}
