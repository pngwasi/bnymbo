<?php

namespace App\Http\Controllers\Admin\Musics;

use App\Events\ExtractionMp3File;
use App\Http\Controllers\Controller;
use App\Repository\MusicRepository;
use App\Repository\UserRepository;
use App\Statics\File;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class MusicController extends Controller
{
    /**
     * @var MusicRepository
     */
    private MusicRepository $musics;

    /**
     * @var UserRepository
     */
    private UserRepository $users;

    /**
     * @param MusicRepository $musics
     * @param UserRepository $users
     */
    public function __construct(MusicRepository $musics, UserRepository $users)
    {
        $this->musics = $musics;
        $this->users = $users;
        $this->middleware(['optimizeImages']);
    }

    /**
     * @param object $music
     * @param array $merge
     * 
     * @return array
     */
    private function apiArrayResultMusic(object $music, array $merge = [])
    {
        return array_merge([
            'id' => $music->id,
            'title' => $music->_title(),
            'url' => $music->adminPlayer(),
            'url_profile' => $music->adminRoute(),
            'image' => asset($music->_image())
        ], $merge);
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @param [type] $id
     * @return array
     */
    public function newArtistMusics(Request $request, $id)
    {
        $user = $this->users->findArtistOrFail($id);
        $disk = config('filesystems.default');

        $request->validate([
            'music_file' => 'required|max:20000|file',
            'title' => 'required|max:255|min:1',
            'music_img' => ['sometimes',  ...File::IMAGE_RULES],
            'unitPrice' => 'nullable|numeric',
            'genres' => 'nullable|max:255|min:2',
            'category' => 'required|max:255'
        ]);

        $title = ucwords(Str::lower($request->get('title')));

        $existName = $user->musics()->where('title', $title)->first();

        if ($existName) {
            return abort(409, trans('Title input already exist'));
        }

        $musicInstance = $this->musics->createInstanceNewMusic(
            $title,
            $request->get('unitPrice'),
            now(),
            $request->get('category'),
            $request->get('genres')
        );

        $music = $user->musics()->save($musicInstance);

        $userMusicPath = File::MUSICS_MSC_PATH . '/' . $user->id . "/" . $music->id;

        $local = in_array($disk, ['public', 'local']);

        $path = $request->file('music_file')->storePubliclyAs($userMusicPath, Str::random(40) . '.mp3', $local ? 'local' : []);

        $music->public_file = $path;
        $music->private_file = $userMusicPath . '/' . File::SHORT_MUSIC_FILENAME;
        // dispache event for extration mp3
        try {
            event(new ExtractionMp3File($path, $userMusicPath));
        } catch (\Throwable $th) {
            //throw $th;
        }
        //----------------
        if ($request->hasFile('music_img')) {
            $img = $request->file('music_img')->storePublicly(File::MUSIC_IMG_PATH . '/' . $user->id . "/" . $music->id);
            $music->image = $img;
        }

        $music->save();
        $music->refresh();

        return [$this->apiArrayResultMusic($music)];
    }

    /**
     * Undocumented function
     * @param int $id
     * @return array
     */
    public function getArtistMusics($id)
    {
        $user = $this->users->findArtistOrFail($id);
        $musics = $user->musics()->latest()->limit(10)->cursor()->map(function ($music) use ($id) {
            return $this->apiArrayResultMusic($music);
        });
        $result = [];
        foreach ($musics as $music) {
            $result[] = $music;
        }
        return $result;
    }

    /**
     * Undocumented function
     *
     * @param int $id
     * @return array
     */
    public function getArtistMusicsList($id)
    {
        $user = $this->users->findArtistOrFail($id);
        $musics = $user->musics()->latest()->paginate(18);
        $musics->withPath(route('admin.artist.get-artist-musics-list', ['id' => $id], false));
        $result = [];
        foreach ($musics as $music) {
            $result[] = $this->apiArrayResultMusic($music, [
                'likes' => $music->likes->count(),
                'downloads' => $music->downloads->count()
            ]);
        }
        $musics['data'] = $result;
        return $musics;
    }


    /**
     * @param Request $request
     * @param mixed $id
     * 
     * @return array
     */
    public function searchArtistMusic(Request $request, $id)
    {
        $request->validate([
            'title' => 'required|max:255|min:2',
        ]);
        $user = $this->users->findArtistOrFail($id);
        $musics = $user->musics()->search($request->title)->get();
        if (!count($musics)) {
            abort(404, trans('No Result found'));
        }
        $result = [];
        foreach ($musics as $music) {
            $result[] = $this->apiArrayResultMusic($music, [
                'likes' => $music->likes->count(),
                'downloads' => $music->downloads->count()
            ]);
        }
        return $result;
    }

    /**
     * @param int $id
     * @param int $music_id
     * @return mixed
     */
    public function playArtistMusic($music_id, Request $request)
    {
        // $user = $request->user('admin');
        // abort_if(!$user, 401, trans('Unauthorized !'));
        $name = $this->musics->findMusicOrFail($music_id);
        $filepath = storage_path('app/' . $name->public_file);
        $total  = filesize($filepath);
        // $blocksize = (2 << 20);
        // $sent      = 0;
        // $handle    = fopen($filepath, "r");
        header('Content-type: audio/mpeg, audio/x-mpeg, audio/x-mpeg-3, audio/mpeg3');
        header('Content-Disposition: filename=' . last(explode('/', $name->public_file)));
        header('Content-length: ' . $total);
        header('X-Pad: avoid browser bug');
        header('Accept-Ranges: bytes');
        header("Content-Transfer-Encoding: chunked");
        readfile($filepath);
        // while ($sent < $total) {
        //     echo fread($handle, $blocksize);
        //     $sent += $blocksize;
        // }
        exit(0);
    }
}
