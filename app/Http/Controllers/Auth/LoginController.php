<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     *  @var string
     */
    protected string $validateTable = 'users';

    /**
     *  @var string
     */
    protected string $validatColumn = 'email';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    // protected function validateLogin(\Illuminate\Http\Request $request)
    // {
    //     $request->validate([
    //         $this->username() => ['required', 'string', Rule::exists($this->validateTable, $this->validatColumn)->where('active', true)],
    //         'password' => ['required', 'string'],
    //     ]);
    // }

    /**
     * Get the login username to be used by the controller.
     * 
     * @return string
     */
    protected function username()
    {
        return 'email';
    }

    /**
     * @param Request $request
     * @param mixed $user
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function authenticated(Request $request, $user)
    {
        $url = $request->query('url');
        return $url ? redirect()->intended((string) $url) : null;
    }

    /**
     * @return mixed
     */
    protected function guard()
    {
        return Auth::guard('web');
    }
}
