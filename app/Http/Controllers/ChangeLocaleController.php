<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ChangeLocaleController extends Controller
{

    /**
     * @param Request $request
     * 
     * @return array
     */
    public function change(Request $request)
    {
        $datas = ['locale' => $request->get('locale')];
        $user = $request->user();
        if ($user) {
            $user->locale = $request->get('locale');
            $user->save();
        }else {
            session($datas);
        }
        return $datas;
    }
}
