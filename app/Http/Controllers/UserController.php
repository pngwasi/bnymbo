<?php

namespace App\Http\Controllers;

use App\Repository\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class UserController extends Controller
{
    /**
     * The user repository implementation.
     * @var UserRepository
     */
    private UserRepository $user;

    /**
     * @param UserRepository $user
     */
    public function __construct(UserRepository $user)
    {
        $this->user = $user;
    }

    /**
     * Undocumented function
     *
     * @param int $id
     * @return static
     */
    public function authOrUserArtist($id)
    {
        if (Auth::check() && Auth::user()->id === intval($id)) {
            $user = Auth::user();
        } else {
            return $this->user->findArtistOrFail($id);
        }
        return $user;
    }
    
    /**
     * Undocumented function
     *
     * @param int $id
     * @param string $relation
     * @param string $withPath
     * @param integer $perPage
     * @return array
     */
    protected function paginate($id, string $relation, string $withPath, $perPage = 12)
    {
        $user = $this->authOrUserArtist($id);
        $user->with($relation);
        $items = $user->$relation()->latest()->paginate($perPage);
        $items->withPath(route($withPath, ['id' => $id, 'name' => Str::slug($user->name)]));
        return [
            'user' => $user,
            'user_items' => $items
        ];
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function followAction(Request $request, $id)
    {
        $user = $this->user->findArtistOrFail($id);
        $user->with('followers');
        $auth = $request->user();
        if (!$auth) {
            return response(['redirect_url' => route('login', [
                'url' => $request->query('url'), 'nonce' => $request->query('nonce'),
            ])], 302);
        }
        abort_if(($auth->id == $user->id), 401, 'Unauthorized action !');
        $followers = $user->followers();
        $followed = $followers->where('follower_id', $auth->id)->first();
        $action = null;
        if ($followed) {
            $followed->delete();
            $action = 'unfollow';
        } else {
            $followers->create(['follower_id' => $auth->id]);
            $action = 'follow';
        }
        return ['action' => $action];
    }

    /**
     *@param int $id
     *@return \Illuminate\Contracts\Support\Renderable
     */
    public function stations($id)
    {
        $instances = $this->paginate($id, 'musics', 'user.stationsTemplates');
        extract($instances);
        return view('_user.stations', [
            'user' => $user,
            'user_items' => $user_items
        ]);
    }

    /**
     *@param int $id
     *@return \Illuminate\Http\JsonResponse
     */
    public function stationsTemplates($id)
    {
        $instances = $this->paginate($id, 'musics', 'user.stationsTemplates');
        extract($instances);
        return response()->json([
            'nextPageUrl' => $user_items->nextPageUrl(),
            'html' => view('layouts.items', ['items' => $user_items])->toHtml()
        ]);
    }


    /**
     *@param int $id
     *@return \Illuminate\Contracts\Support\Renderable
     */
    public function albums($id)
    {
        $instances = $this->paginate($id, 'albums', 'user.albumsTemplates', 6);
        extract($instances);
        return view('_user.albums', compact('user', 'user_items'));
    }


    /**
     *@param int $id
     *@return \Illuminate\Http\JsonResponse
     */
    public function albumsTemplates($id)
    {
        $instances = $this->paginate($id, 'albums', 'user.albumsTemplates', 6);
        extract($instances);
        return response()->json([
            'nextPageUrl' => $user_items->nextPageUrl(),
            'html' => view('layouts.items-albums', ['items' => $user_items])->toHtml()
        ]);
    }

    /**
     *@param int $id
     *@return \Illuminate\Contracts\Support\Renderable
     */
    public function playlists($id)
    {
        $instances = $this->paginate($id, 'playlists', 'user.playlistsTemplates', 6);
        extract($instances);
        return view('_user.playlists', compact('user', 'user_items'));
    }
    /**
     *@param int $id
     *@return \Illuminate\Http\JsonResponse
     */
    public function playlistsTemplates($id)
    {
        $instances = $this->paginate($id, 'playlists', 'user.playlistsTemplates', 6);
        extract($instances);
        return response()->json([
            'nextPageUrl' => $user_items->nextPageUrl(),
            'html' => view('layouts.items-playlists', ['items' => $user_items])->toHtml()
        ]);
    }

    /**
     *  @param  \Illuminate\Database\Eloquent\Builder[] $user_items
     *  @return  \Illuminate\Database\Eloquent\Builder|static[]
     */
    private function followed($user_items, Request $request)
    {
        if ($auth = $request->user()) {
            foreach ($user_items as $key => $follower) {
                $followers = $follower->follower->followers();
                $followed = $followers->firstWhere('follower_id', $auth->id);
                $user_items[$key]->follower->followed = !!$followed;
            }
        }
        return $user_items;
    }

    /**
     *@param int $id
     *@return \Illuminate\Contracts\Support\Renderable
     */
    public function followers(Request $request, $id)
    {
        $instances = $this->paginate($id, 'followers', 'user.followersTemplates', 12);
        extract($instances);
        $user_items = $this->followed($user_items, $request);
        return view('_user.followers', compact('user', 'user_items'));
    }

    /**
     *@param int $id
     *@return \Illuminate\Http\JsonResponse
     */
    public function followersTemplates(Request $request, $id)
    {
        $instances = $this->paginate($id, 'followers', 'user.followersTemplates', 12);
        extract($instances);
        return response()->json([
            'nextPageUrl' => $user_items->nextPageUrl(),
            'html' => view('layouts.items-followers', ['items' => $this->followed($user_items, $request)])->toHtml()
        ]);
    }
}
