<?php

namespace App\Http\Controllers;

use App\Repository\NewsRepository;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    /**
     * @var NewsRepository
     */
    private NewsRepository $news;

    /**
     * @param NewsRepository $news
     */
    public function __construct(NewsRepository $news)
    {
        $this->news = $news;
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $latest = $this->news->getLastArticles(2);
        $articles = $this->news->getArticles();
        return view('news', compact('latest', 'articles'));
    }

    /**
     * @param int $id
     * @param string $slug
     * 
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function article($id, $slug)
    {
        $article = $this->news->findArticleOrFail($id, $slug);
        $article->views += 1;
        $article->save();
        return view('_news.article', compact('article'));
    }
}
