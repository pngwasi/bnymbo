<?php

namespace App\Http\Controllers;

use App\Repository\UserRepository;
use Illuminate\Http\Request;

class ArtistesController extends Controller
{
    /**
     * Undocumented variable
     *
     * @var UserRepository
     */
    private UserRepository $artistes;

    /**
     * Undocumented function
     *
     * @param UserRepository $artist
     */
    public function __construct(UserRepository $artist)
    {
        $this->artistes = $artist;
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function index(Request $request)
    {
        $artistes = $this->followed($this->artistes->getAllArtistes(), $request);
        return view('artistes', compact('artistes'));
    }

    /**
     * Undocumented function
     *
     * @param mixed $user_items
     * @param Request $request
     * @return \Illuminate\Database\Eloquent\Builder|static[]
     */
    private function followed($user_items, Request $request)
    {
        if ($auth = $request->user()) {
            foreach ($user_items as $key => $follower) {
                $followers = $follower->followers();
                $followed = $followers->firstWhere('follower_id', $auth->id);
                $user_items[$key]->followed = !!$followed;
            }
        }
        return $user_items;
    }
}
