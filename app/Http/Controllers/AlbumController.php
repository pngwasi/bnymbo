<?php

namespace App\Http\Controllers;

use App\Actions\LikeAction;
use App\Repository\AlbumRepository;
use App\Repository\GeneralPriceRepository;
use App\Repository\MusicRepository;
use App\Statics\CartTable;
use App\Statics\CurrencyFormat;
use Illuminate\Http\Request;

class AlbumController extends Controller
{
    use LikeAction;

    private AlbumRepository $album;

    private MusicRepository $musics;

    /**
     * Undocumented function
     *
     * @param AlbumRepository $album
     * @param MusicRepository $musics
     */
    public function __construct(AlbumRepository $album, MusicRepository $musics)
    {
        $this->album = $album;
        $this->musics = $musics;
    }

    /**
     * Undocumented function
     *
     * @param int $id
     * @param Request $request
     * @param CurrencyFormat $format
     * @return  \Illuminate\Contracts\Support\Renderable
     */
    public function index($id, Request $request, CurrencyFormat $format, CartTable $cartTable)
    {
        $album = $this->album->findAlbumOrFail($id);
        $album->with('user');
        $album->with('musics');
        $musics = $album->musics;
        $likesCount = $format->format($album->likes->count());
        $othersAlbum = $this->album->queryLatestExceptPassed($album->id, $album->category, 6);
        $auth = $request->user();
        $liked = false;
        if ($auth) {
            $like = $album->likes()->where('user_id', $auth->id)->first();
            $liked = !!$like;
        }
        $price = $album->_price();
        $cartDetails = [
            'id' => $id,
            'section' =>  $cartTable->album(),
            'price' => $price
        ];
        return view('album', compact(
            'album',
            'liked',
            'likesCount',
            'musics',
            'price',
            'othersAlbum',
            'cartDetails'
        ));
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @param int $id
     * @param CurrencyFormat $format
     * @return mixed
     */
    public function likeAction(Request $request, $id, CurrencyFormat $format)
    {
        $album = $this->album->findAlbumOrFail($id);
        return $this->Like($request, $album, $format);;
    }
}
