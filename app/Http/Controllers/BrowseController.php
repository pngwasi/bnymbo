<?php

namespace App\Http\Controllers;

use App\Repository\AlbumRepository;
use App\Repository\MusicRepository;
use App\Repository\PlaylistRepository;
use App\Statics\Category;
use App\Statics\Genres;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class BrowseController extends Controller
{

    /**
     * @var MusicRepository
     */
    private MusicRepository $musics;

    /**
     * @var Category
     */
    private Category $categories;

    /**
     * @var AlbumRepository
     */
    private AlbumRepository $albums;

    /**
     * @var PlaylistRepository
     */
    private PlaylistRepository $playlists;

    /**
     * @param MusicRepository $musics
     * @param Category $categories
     * @param AlbumRepository $albums
     * @param PlaylistRepository $playlists
     */
    public function __construct(MusicRepository $musics, Category $categories, AlbumRepository $albums, PlaylistRepository $playlists)
    {
        $this->musics = $musics;
        $this->categories = $categories;
        $this->albums = $albums;
        $this->playlists = $playlists;
    }

    /**
     * @param array  $array
     * @return array
     */
    public function response($array = [])
    {
        $array['categories'] = $this->categories->types();
        $array['category_type'] = session('category-type', $this->categories->types()[1]['type']);
        return $array;
    }

    /**
     * @param Request $request
     * @return array
     */
    public function setCategory(Request $request)
    {
        $datas = ['category-type' => $request->get('category')];
        session($datas);
        return $datas;
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $populars = $this->musics->queryTopLastPopular(6);
        return view('_browse.browse', compact('populars'));
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function release()
    {
        $array = $this->response();
        $array['release'] = $this->musics->release($array['category_type']);
        return view('_browse.releases', $array);
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function popular()
    {
        $response = $this->response();
        $response['popular'] = $this->musics->popular($response['category_type']);
        return view('_browse.popular', $response);
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function stations()
    {
        $response = $this->response();
        $response['musics'] = $this->musics->queryMusicsByPaginate($response['category_type']);
        return view('_browse.stations', $response);
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function albums()
    {
        $response = $this->response();
        $response['albums'] = $this->albums->queryAlbumsByPaginate($response['category_type']);
        return view('_browse.albums', $response);
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function playlists()
    {
        $response = $this->response();
        $response['playlists'] = $this->playlists->queryPlaylistsByPaginate();
        return view('_browse.playlists', $response);
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function genres(Genres $genres)
    {
        return view('_browse.genres', compact('genres'));
    }

    /**
     *  @return  \Illuminate\Contracts\Support\Renderable
     */
    public function genre(String $genre, Genres $genres)
    {
        $response = $this->response();
        $response['genres'] = array_slice($genres->genres_short,0, floor((count($genres->genres_short) - 1)  / 2));
        $response['route_genre'] = $this->getPassedGenre($genres->genres_short, $genre);
        $response['musics'] = $this->musics->queryMusicsByPaginate($response['category_type'], $response['route_genre']);
        return view('genre', $response);
    }

    /**
     * Undocumented function
     *
     * @param Genres $genres
     * @param string $genre
     * @return string|null
     */
    private function getPassedGenre(array $genres, string $genre): ?string
    {
        foreach ($genres as $value) {
            if (Str::slug($value) == $genre) {
                return $value;
            }
        }
        return null;
    }
}
