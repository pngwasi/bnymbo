<?php

namespace App\Http\Middleware;

use Closure;

class UserOnAuthPages
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user() && (int)$request->id != $request->user()->id) {
            return redirect('/');
        }
        return $next($request);
    }
}
