<?php

namespace App\Http\View\Composers;

use App\Repository\UserRepository;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ArtistProfileComposer
{
    /**
     * The user repository implementation.
     * @var UserRepository
     */
    protected $users;


    /**
     * @var Request
     */
    protected $request;


    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct(UserRepository $users, Request $request)
    {
        // Dependencies automatically resolved by service container...
        $this->users = $users;
        $this->request = $request;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('user', $this->users->findArtistOrFail($this->request->id));
    }
}
