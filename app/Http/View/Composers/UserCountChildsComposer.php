<?php

namespace App\Http\View\Composers;

use App\Repository\FollowerRepository;
use App\Repository\UserRepository;
use Illuminate\Http\Request;
use Illuminate\View\View;

class UserCountChildsComposer
{
    /**
     * The user repository implementation.
     * @var UserRepository
     */
    protected $users;


    /**
     * @var Request
     */
    protected $request;

    /**
     * @var FollowerRepository
     */
    protected $followRp;



    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct(UserRepository $users, Request $request, FollowerRepository $followRp)
    {
        // Dependencies automatically resolved by service container...
        $this->users = $users;
        $this->request = $request;
        $this->followRp = $followRp;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $counts = $this->users->countingChilds($this->request->id, $this->request->user(), $this->followRp);
        foreach ($counts as $key => $value) {
            $view->with($key, $value);
        }
    }
}
