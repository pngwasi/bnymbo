<?php

namespace App\Http\Requests;

use App\Statics\File;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'max:255', 'min:2', Rule::unique('users')->ignore($this->user()->id)],
            'email' => ['required', 'string', 'email', 'max:255', Rule::unique('users')->ignore($this->user()->id)],
            'phone' => ['required', 'regex:/^[0-9\-\(\)\/\+\s]*$/', Rule::unique('users')->ignore($this->user()->id)],
            'firstname' => ['required', 'string', 'max:255', 'min:2'],
            'lastname' => ['required', 'string', 'max:255', 'min:2'],
            'password' => ['required', 'string', 'max:255', 'password:web'],
            'description' => ['nullable', 'string', 'max:255', 'min:2'],

            'website' => ['nullable', 'string', 'max:255', 'min:2', 'url'],
            'facebook' => ['nullable', 'string', 'max:255', 'min:2', 'url'],
            'twitter' => ['nullable', 'string', 'max:255', 'min:2', 'url'],
            'youtube' => ['nullable', 'string', 'max:255', 'min:2', 'url'],
            'instagram' => ['nullable', 'string', 'max:255', 'min:2', 'url'],
            'image' => ['nullable', ...array_slice(File::IMAGE_RULES, 1)],

            'new_password' => ['nullable', 'string', 'max:255', 'min:6', 'confirmed'],
        ];
    }
}
