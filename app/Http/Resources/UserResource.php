<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->_name(),
            'firstname' => $this->first_name,
            'lastname' => $this->last_name,
            'artiste' => boolval($this->artiste),
            'image' => asset($this->_image()),
            'email' => $this->email,
            'website' => $this->website,
            'created_at' => $this->created_at->toDateTimeString(),
        ];
    }
}
