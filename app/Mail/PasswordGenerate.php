<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PasswordGenerate extends Mailable implements ShouldQueue
{
    use SerializesModels, Queueable;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public User $user;

    public string $password;

    public function __construct(User $user, string $password)
    {
        $this->password = $password;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.users.password.generate');
    }
}
