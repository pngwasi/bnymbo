<?php

namespace App\Events;

use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ExtractionMp3File
{
    use Dispatchable, SerializesModels;

    public string $filename;
    
    public int $from;

    public int $starting = 20;

    public string $path;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(string $filename = '', string $path ,int $from  = 10, int $starting = 20)
    {
        $this->filename = $filename;
        $this->from = $from;
        $this->starting = $starting;
        $this->path = $path;
    }

}
