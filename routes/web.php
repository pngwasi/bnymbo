<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true, 'reset' => true, 'confirm' => true]);

Route::where(['id' => '[0-9]+'])
    ->group(function () {
        Route::get('/', 'HomeController@index')->name('home');
        Route::get('search/', 'SearchController@index')->name('search');
        Route::namespace('Checkout')
            ->prefix('cart')
            ->group(function () {
                Route::get('', 'CartController@index')->name('cart');
                Route::get('items/', 'CartController@cartItems')->name('cart.items');
                Route::post('add/', 'CartController@addToCart')->name('cart.add');
                Route::delete('remove/', 'CartController@deleteItemCart')->name('cart.delete');
            });
        Route::namespace('Checkout')
            ->middleware('auth')
            ->prefix('checkout')
            ->group(function () {
                Route::get('', 'CheckoutController@index')->name('checkout');
            });
        Route::prefix('news')
            ->name('news')
            ->group(function () {
                Route::get('', 'NewsController@index');
                Route::get('/{id}/{slug}', 'NewsController@article')->name('.article');
            });
        Route::get('artistes/', 'ArtistesController@index')->name('artistes');

        Route::post('brow-se/category', "BrowseController@setCategory")->name('browse-set-category');
        Route::get('brow-se/', "BrowseController@index")->name('browse-index');
        Route::prefix('brow-se')
            ->name('browse.')
            ->group(function () {
                Route::get('release', "BrowseController@release")->name('release');
                Route::get('popular', "BrowseController@popular")->name('popular');
                Route::get('stations', "BrowseController@stations")->name('stations');
                Route::get('albums', "BrowseController@albums")->name('albums');
                Route::get('playlists', "BrowseController@playlists")->name('playlists');
                Route::get('genres', "BrowseController@genres")->name('genres');
            });
        Route::get('genre/{name}', "BrowseController@genre")->name('genre');

        Route::get('station/{id}/{title}', "StationController@index")
            ->name('station');
        Route::get('album/{id}/{name}', 'AlbumController@index')
            ->name('album');
        Route::get('playlist/{id}/{name}', "PlaylistController@index")
            ->name('playlist');

        Route::post('like/album/action/{id}/', "AlbumController@likeAction")->name('likeAlbum');
        Route::post('like/music/action/{id}/', "StationController@likeAction")->name('likeMusic');
        Route::post('like/playlist/action/{id}/', "PlaylistController@likeAction")->name('likePlaylist');

        Route::post('playings/{id}', "StationController@playingMusic")->name('playingCount');

        Route::post('follow/user/action/{id}', "UserController@followAction")->name('followUser');
        Route::prefix('user/{id}/{name}')
            ->name('user')
            ->group(function () {
                Route::get('stations', "UserController@stations");
                Route::get('stations/template/items', "UserController@stationsTemplates")->name('.stationsTemplates');

                Route::get('albums', "UserController@albums")->name('.albums');
                Route::get('albums/template/items', "UserController@albumsTemplates")->name('.albumsTemplates');

                Route::get('playlists', "UserController@playlists")->name('.playlists');
                Route::get('playlists/template/items', "UserController@playlistsTemplates")->name('.playlistsTemplates');


                Route::get('followers', "UserController@followers")->name('.followers');
                Route::get('followers/template/items', "UserController@followersTemplates")->name('.followersTemplates');

                Route::middleware(['auth', 'UserOnAuthPages'])
                    ->group(function () {
                        // 
                        Route::get('following', "UserAuthController@following")->name('.following');
                        Route::get('following/template/items', "UserAuthController@followingTemplates")->name('.followingTemplates');

                        Route::get('purchase', "UserAuthController@purchase")->name('.purchase');

                        Route::get('profile', "UserAuthController@profile")->name('.profile');
                        Route::put('profile', "UserAuthController@profileModify")->name('.profileModify');

                        Route::get('upload', "UserAuthController@upload")->name('.upload');
                        Route::post('upload/artiste-request', "UserAuthController@artisteRequest")->name('.upload.artiste-request');
                        Route::post('upload/music-request', "UserAuthController@musicRequest")->name('.upload.music-request');

                        Route::get('desconnect', "UserAuthController@desconnect")->name('.desconnect');
                    });
            });
    });
Route::post('locale/', "ChangeLocaleController@change")->name('locale');

// Admin routes
Route::get('dash/', fn () => redirect(route('admin.login')));
Route::namespace('Admin\Auth')
    ->prefix('dash')
    ->name('admin.login')
    ->group(function () {
        Route::get('login/', "AdminLoginController@showLoginForm");
        Route::post('login/', "AdminLoginController@login");
    });

Route::namespace('Admin')
    ->prefix('dash')
    ->name('admin.')
    ->where(['id' => '[0-9]+'])
    ->middleware('auth:admin')
    ->group(function () {
        Route::post('logout/', "Auth\AdminLoginController@logout")->name('logout');
        Route::get('home/', "DashboardController@index")->name('home');
        Route::get('artist/', "Artist\HomeArtistController@recent")->name('artist');
        Route::namespace('Artist')
            ->prefix('artist')
            ->name('artist.')
            ->group(function () {
                Route::get('status/', 'HomeArtistController@status')->name('status');
                Route::get('request/', "HomeArtistController@request")->name('request');
                Route::patch('modify/', "HomeArtistController@modifyArtist")->name('modify');

                Route::get('view/{id}/', "ArtistProfilController@traffic")->name('profile');
                Route::prefix('view/{id}/')
                    ->name('profile.')
                    ->group(function () {
                        Route::get('music/', "ArtistProfilController@music")->name('music');
                        Route::get('profile/', "ArtistProfilController@profile")->name('profile');
                        Route::get('report/', "ArtistProfilController@report")->name('report');
                        Route::get('orders/', "ArtistProfilController@orders")->name('orders');
                        Route::get('settings/', "ArtistProfilController@settings")->name('settings');
                    });
            });


        Route::get('music-album/', "MusicAlbumController@musics")->name("music_album");
        Route::prefix('music-album')
            ->name('musics.')
            ->group(function () {
                Route::get('playlists/',  "MusicAlbumController@playlists")->name('playlists');
                Route::get('albums/',  "MusicAlbumController@albums")->name('albums');
                Route::get('music-request/',  "MusicAlbumController@musicRequest")->name('music-request');
                Route::patch('playlist/', "MusicAlbumController@modifyPlaylist")->name('playlist.modify');;
                Route::patch('music/', "MusicAlbumController@modifyMusic")->name('music.modify');
                Route::patch('album/', "MusicAlbumController@modifyAlbum")->name('album.modify');

                Route::namespace('Musics')
                    ->name('music.profile')
                    ->prefix('music/profil/{id}')
                    ->group(function () {
                        Route::get('', "MusicProfilController@home")->name('.home');
                        Route::get('settings/', "MusicProfilController@settings")->name('.settings');
                    });
                Route::prefix('album/profil/{id}')
                    ->namespace('Album')
                    ->name('album.profile')
                    ->group(function () {
                        Route::get('', "AlbumProfilController@home")->name('.home');
                        Route::get('report/', "AlbumProfilController@report")->name('.report');
                        Route::get('settings/', "AlbumProfilController@settings")->name('.settings');
                    });
                Route::namespace('Playlist')
                    ->name('playlist.profile')
                    ->prefix('playlist/profil/{id}')
                    ->group(function () {
                        Route::get('', "PlaylistProfilController@home")->name('.home');
                        Route::get('report/', "PlaylistProfilController@report")->name('.report');
                        Route::get('settings/', "PlaylistProfilController@settings")->name('.settings');
                    });
            });


        Route::prefix('client')
            ->name('client')
            ->namespace('Client')
            ->group(function () {
                Route::get('', "ClientController@index");
            });

        Route::prefix('purchase')
            ->name('purchase')
            ->namespace('Purchase')
            ->group(function () {
                Route::get('', "PurchaseController@index");
            });

        Route::get('events/', "EventController@index")->name('event');
        Route::prefix('news')
            ->name('news')
            ->group(function () {
                Route::get('', "NewsController@newArticle");
                Route::get('articles/', "NewsController@articles")->name('.articles');
                Route::patch('article/', "NewsController@modifyArticle")->name('.article.modify');
                Route::get('categories/', "NewsController@categories")->name('.categories');
                Route::delete('category/', "NewsController@delCategory")->name('.category.delete');
            });
    });
