<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::namespace('Admin')
    ->name('admin.')
    ->prefix('dashboard')
    ->where(['id' => '[0-9]+'])
    ->middleware('auth:api-admin')
    ->group(function () {
        Route::prefix('artist')
            ->name('artist.')
            ->group(function () {
                Route::namespace('Artist')
                    ->group(function () {
                        Route::post('register/', "ArtistController@newArtist")->name('newArtist');
                        Route::get('{id}/albums', "ArtistController@getAlbumArtist")->name('get-album');
                        Route::post('register/{id}/album', "ArtistController@createAlbumArtist")->name('create-album');
                        Route::get('{id}/playlists', "ArtistController@getPlaylists")->name('get-playlists');
                        Route::post('register/{id}/playlist', "ArtistController@newPlaylist")->name('new-playlist');

                        Route::prefix('repport')
                            ->group(function () {
                                Route::get('{id}/activity', 'ArtistRepportController@activities')->name('activities');
                                Route::get('{id}/followers', 'ArtistRepportController@followers')->name('followers');
                                Route::get('{id}/print-repport', 'ArtistRepportController@printRepport')->name('print-repport');
                            });

                        Route::name('profile.')
                            ->prefix('profile')
                            ->group(function () {
                                Route::put('{id}/generate-password/', 'ArtistController@generatePassword')->name('password');
                                Route::put('{id}/primary/', 'ArtistController@changeProfilPrimary')->name('primary');
                                Route::put('{id}/secondary/', 'ArtistController@changeProfilSecondary')->name('secondary');
                            });
                    });

                Route::namespace('Musics')
                    ->group(function () {

                        // unsecure
                        Route::post('register/{id}/musics', 'MusicController@newArtistMusics')
                        ->withoutMiddleware(['auth:api-admin'])
                        ->name('new-artist-music');
                        // unsecure

                        Route::get('{id}/musics', 'MusicController@getArtistMusics')->name('get-artist-musics');
                        Route::get('{id}/musics-list', 'MusicController@getArtistMusicsList')->name('get-artist-musics-list');
                        Route::post('search/{id}/music', 'MusicController@searchArtistMusic')->name('search-artist-music');

                        Route::get('play/music/{music_id}', 'MusicController@playArtistMusic')
                            ->name('play-artist-music')
                            ->withoutMiddleware(['auth:api-admin', 'api'])
                            ->middleware(['web'])
                            ->where(['music_id' => '[0-9]+']);
                    });
            });

        Route::prefix('music-album')
            ->name('music-album.')
            ->group(function () {
                Route::post('genral-price/', "MusicAlbumController@putGeneralPrice")->name('general-price');
                Route::namespace('Album')
                    ->name('album.')
                    ->prefix('album')
                    ->group(function () {
                        //unsecure
                        Route::post('{id}/new/musics', 'AlbumController@newAlbumMusics')
                        ->withoutMiddleware(['auth:api-admin'])
                        ->name('new-musics');
                        //unsecure
                        
                        Route::get('{id}/musics', 'AlbumController@getAlbumMusicsList')->name('get-musics');
                    });
                Route::namespace('Playlist')
                    ->name('playlist.')
                    ->prefix('playlist')
                    ->group(function () {
                        Route::get('{id}/musics', 'PlaylistProfilController@getPlaylistItems')->name('musics');
                        Route::post('{id}/add-to-playlist/', 'PlaylistProfilController@addToPlaylist')->name('add-to-playlist');
                    });
            });
        Route::prefix('client')
            ->name('client.')
            ->namespace('Client')
            ->group(function () {
                Route::get('all/', "ClientController@all")->name('all');
                Route::get('search/', "ClientController@search")->name('search');
                Route::get('details/', "ClientController@details")->name('details');
                Route::get('following/', "ClientController@following")->name('following');
                Route::get('followers/', "ClientController@followers")->name('followers');
                Route::get('likes/', "ClientController@likes")->name('likes');
                Route::get('purchase/', "ClientController@purchase")->name('purchase');
                Route::post('client-artiste/', "ClientController@toArtiste")
                    ->middleware('optimizeImages')
                    ->name('clientToArtist');
            });
        Route::prefix('news')
            ->name('news')
            ->group(function () {
                Route::post('', "NewsController@addArticle")->name('.add-article');
                Route::get('get-link-meta-data', "NewsController@editorLinkData")
                    ->withoutMiddleware(['auth:api-admin'])
                    ->name('.editor-link');
                Route::post('category', "NewsController@addCategory")->name('.add-category');
            });
        Route::put('profile/', 'DashboardController@adminProfile')->name('profile');
    });
